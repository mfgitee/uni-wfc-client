# 野火实时音视频RTC插件使用说明

## 前置说明
本插件依赖于[野火IM插件](https://ext.dcloud.net.cn/plugin?id=7895)，需要同时集成该插件，才能正常使用。

## 集成说明
有2种插件的使用方法，一种是基于野火提供的Demo直接进行二次开发；另外一种是把野火插件放到现有项目中。

### 使用野火demo
野火UniApp平台的demo地址:[https://gitee.com/wfchat/uni-chat](https://gitee.com/wfchat/uni-chat)。按照说明配置地址就可以直接运行，再根据产品需求进行二次开发。

### 现有项目使用
#### 集成步骤：
1. 添加插件到项目中
2. 参考[野火IM插件](https://ext.dcloud.net.cn/plugin?id=7895)的集成说明，完成野火IM插件的集成
3. 调用接口类[avengineKit.js](https://gitee.com/wfchat/uni-chat/blob/main/wfc/av/engine/avengineKit.js)来使用野火插件

### 源码地址
源码在[https://gitee.com/wfchat/uni-wfc-client](https://gitee.com/wfchat/uni-wfc-client)。

### 技术支持
如果遇到问题，可以去[插件源码工程](https://gitee.com/wfchat/uni-wfc-client)或者[demo源码工程](https://gitee.com/wfchat/uni-chat)提issue，也可以去[野火论坛](https://bbs.wildfirechat.cn)发帖子问题，我们会尽快回复。谢谢大家的支持。
