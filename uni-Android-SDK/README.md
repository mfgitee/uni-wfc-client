# uni-Android-SDK


## Android IM Client 原生插件说明

0. 提供两个插件```uni-client-module```和```uni-uikit-module```，分别是 IM 能力层插件和 UI 层插件

    1. [x] ```uni-app```层提供对```uni-client-module``` 插件的封装，接口和```web/pc/wx```一致
    2. [ ] ```uni-app```层提供对```uni-uikit-module``` 插件的封装，支持```uni-app```直接打开原生界面

1. 支持推送

## 快速开始

1. 到uniapp [开发者后台](https://dev.dcloud.net.cn)创建应用，并申请```appkey```，可参考[这儿](https://nativesupport.dcloud.net.cn/AppDocs/usesdk/appkey)
2. 修改```app/AndroidManifest.xml```里面```dcloud_appkey```对应的```value```为上一步生成的```appkey```
3. 根据第一步生成的证书等，修改```app/build.gradle```里面```signingConfigs```部分
4. 将```app/src/main/assets/apps/```下面的目录名修改为第一步步生成的```appId```，同时，将```app/src/main/assets/data/dcloud_control.xml```里面的```appid```字段修改为第一步生成的```appId```
5. 跟随[野火文档](https://docs.wildfirechat.net/)在本地或在服务器上构建并配置好服务端
6. 运行```app module```！

## 更改示例项目并测试

用 hbuilderx 打开[uni-chat](https://github.com/wildfirechat/uni-chat)

- 更改示例后在 hbuilderx -> 发行 -> 本地打包 -> 生成本地打包资源，并将其放入`app/src/main/assets/apps`中，然后在 Android Studio 中运行
- 或者修改原生代码后[打自定义基座](https://ask.dcloud.net.cn/article/35482)并在 hbuilderx 中运行

具体选择哪种取决于当前工作重心，封装原生新功能使用本地打包，只需调试示例打自定义基座

## 注意事项

1. [uni-app离线打包Android平台注意事项](https://ask.dcloud.net.cn/article/35139)


## 效果图
![](./images/home.png))
![](./images/conversation.png))
![](./images/contact.png))
![](./images/user.png))

## 感谢

本项目参考了[wildfire-uniplugin-demo](https://github.com/PentaTea/wildfire-uniplugin-demo)
