package cn.wildfirechat.uni.client;

import com.taobao.weex.bridge.JSCallback;

import cn.wildfirechat.remote.GeneralCallback;

public class JSGeneralCallback implements GeneralCallback {

    private JSCallback successCB = null;
    private JSCallback failCB = null;

    public JSGeneralCallback(JSCallback successCB, JSCallback failCB) {
        this.successCB = successCB;
        this.failCB = failCB;
    }

    @Override
    public void onSuccess() {
        if (successCB != null) {
            successCB.invoke(null);
        }
    }

    @Override
    public void onFail(int errorCode) {
        if (failCB != null) {
            failCB.invoke(errorCode);
        }
    }
}
