package cn.wildfirechat.uni.client;

import com.taobao.weex.bridge.JSCallback;

import cn.wildfirechat.remote.GeneralCallback2;

public class JSGeneralCallback2 implements GeneralCallback2 {

    private JSCallback successCB = null;
    private JSCallback failCB = null;

    public JSGeneralCallback2(JSCallback successCB, JSCallback failCB) {
        this.successCB = successCB;
        this.failCB = failCB;
    }

    @Override
    public void onSuccess(String result) {
        if (successCB != null) {
            successCB.invoke(result);
        }
    }

    @Override
    public void onFail(int errorCode) {
        if (failCB != null) {
            failCB.invoke(errorCode);
        }
    }
}
