package cn.wildfirechat.uni.client.jsmodel;

import cn.wildfirechat.model.Conversation;
import cn.wildfirechat.model.ConversationSearchResult;

public class JSConversationSearchResult {
    public Conversation conversation;
    //only marchedCount == 1, load the message
    public JSMessage marchedMessage;
    public long timestamp;
    public int marchedCount;

    public JSConversationSearchResult() {
    }

    public static JSConversationSearchResult fromConversationSearch(ConversationSearchResult result){
        JSConversationSearchResult jsResult = new JSConversationSearchResult();
        jsResult.conversation = result.conversation;
        jsResult.marchedMessage = JSMessage.fromMessage(result.marchedMessage);
        jsResult.timestamp = result.timestamp;
        jsResult.marchedCount = result.marchedCount;;
        return jsResult;
    }
}
