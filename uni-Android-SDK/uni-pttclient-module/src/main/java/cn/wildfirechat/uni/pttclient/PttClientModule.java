package cn.wildfirechat.uni.pttclient;

import android.util.Log;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.taobao.weex.bridge.JSCallback;

import java.util.ArrayList;
import java.util.List;

import cn.wildfirechat.model.Conversation;
import cn.wildfirechat.ptt.PTTClient;
import cn.wildfirechat.ptt.PttCallback;
import cn.wildfirechat.ptt.TalkingCallback;
import cn.wildfirechat.remote.ChatManager;
import cn.wildfirechat.uni.client.ClientUniAppHookProxy;
import cn.wildfirechat.uni.client.JSGeneralCallback;
import io.dcloud.feature.uniapp.AbsSDKInstance;
import io.dcloud.feature.uniapp.annotation.UniJSMethod;
import io.dcloud.feature.uniapp.common.UniModule;

/**
 * 野火IM Android pttClient扩展
 */
public class PttClientModule extends UniModule implements PttCallback {

    private static final String TAG = "WfcPttClientModule";

    static AbsSDKInstance uniSDKInstance;

    @UniJSMethod(uiThread = false)
    public void init(String options) {
       initClient(options);
    }
    @UniJSMethod(uiThread = false)
    public void initClient(String options) {
        try {
            JSONObject obj = JSONObject.parseObject(options);
            if (obj.containsKey("enableFullDuplex")) {
                PTTClient.ENABLE_FULL_DUPLEX = obj.getBoolean("enableFullDuplex");
            }
            if (obj.containsKey("enableGlobalPtt")) {
                PTTClient.ENABLE_GLOBAL_PTT = obj.getBoolean("enableGlobalPtt");
            }
            if (obj.containsKey("enablePriorityMode")) {
                PTTClient.ENABLE_PRIORITY_MODE = obj.getBoolean("enablePriorityMode");
            }
            if (obj.containsKey("groupChatMaxSpeakTime")) {
                PTTClient.GROUP_CHAT_MAX_SPEAK_TIME = obj.getIntValue("groupChatMaxSpeakTime") * 1000;
            }
            if (obj.containsKey("groupChatMaxSpeakerCount")) {
                PTTClient.GROUP_CHAT_MAX_SPEAKER_COUNT = obj.getIntValue("groupChatMaxSpeakerCount");
            }
            if (obj.containsKey("groupChatSaveVoiceMessage")) {
                PTTClient.GROUP_CHAT_SAVE_VOICE_MESSAGE = obj.getBoolean("groupChatSaveVoiceMessage");
            }
            if (obj.containsKey("singleChatMaxSpeakTime")) {
                PTTClient.SINGLE_CHAT_MAX_SPEAK_TIME = obj.getIntValue("singleChatMaxSpeakTime") * 1000;
            }
            if (obj.containsKey("singleChatMaxSpeakerCount")) {
                PTTClient.SINGLE_CHAT_MAX_SPEAKER_COUNT = obj.getIntValue("singleChatMaxSpeakerCount");
            }
            if (obj.containsKey("singleChatSaveVoiceMessage")) {
                PTTClient.SINGLE_CHAT_SAVE_VOICE_MESSAGE = obj.getBoolean("singleChatSaveVoiceMessage");
            }

            //PTTClient.getInstance().init(); 在 PttClientUniAppHookProxy 里面调用

        } catch (Exception e) {
            e.printStackTrace();
        }

        PttClientModule.uniSDKInstance = mUniSDKInstance;
        // main thread
        ChatManager.Instance().getMainHandler().post(() -> {
            PTTClient.getInstance().setPttCallback(this);
        });
    }

    @Override
    public void onActivityCreate() {
        super.onActivityCreate();
    }

    /**
     * sdk后处理
     */
    @Override
    public void onActivityDestroy() {
        super.onActivityDestroy();
        // 删除监听器
        Log.i(TAG, "应用销毁后处理");
    }

    @UniJSMethod(uiThread = false)
    public int getMaxSpeakCount(String strConv) {
        Conversation conversation = parseObject(strConv, Conversation.class);
        if (conversation == null) {
            return 0;
        }
        return PTTClient.getInstance().getMaxSpeakCount(conversation);
    }

    @UniJSMethod(uiThread = false)
    public int getMaxSpeakTime(String strConv) {
        Conversation conversation = parseObject(strConv, Conversation.class);
        if (conversation == null) {
            return 0;
        }
        return PTTClient.getInstance().getMaxSpeakTime(conversation);
    }

    @UniJSMethod(uiThread = false)
    public int getTalkingMemberCount(String strConv) {
        Conversation conversation = parseObject(strConv, Conversation.class);
        if (conversation == null) {
            return 0;
        }
        return PTTClient.getInstance().getTalkingMemberCount(conversation);
    }

    @UniJSMethod(uiThread = false)
    public String getTalkingMembers(String strConv) {
        Conversation conversation = parseObject(strConv, Conversation.class);
        List<String> list = new ArrayList<>();
        if (conversation != null) {
            list = PTTClient.getInstance().getTalkingMembers(conversation);
        }
        return JSONObject.toJSONString(list, ClientUniAppHookProxy.serializeConfig);
    }

    @UniJSMethod(uiThread = false)
    public long getTalkingStartTime() {
        return PTTClient.getInstance().getTalkingStartTime();
    }

    @UniJSMethod(uiThread = false)
    public boolean isConversationPttSilent(String strConv) {
        Conversation conversation = parseObject(strConv, Conversation.class);
        if (conversation == null) {
            return true;
        }
        return PTTClient.getInstance().isConversationPttSilent(conversation);
    }

    @UniJSMethod(uiThread = false)
    public boolean isSaveVoiceMessage(String strConv) {
        Conversation conversation = parseObject(strConv, Conversation.class);
        if (conversation == null) {
            return true;
        }
        return PTTClient.getInstance().isSaveVoiceMessage(conversation);
    }

    @UniJSMethod(uiThread = false)
    public void releaseTalking(String strConv) {
        Conversation conversation = parseObject(strConv, Conversation.class);
        if (conversation == null) {
            return;
        }
        ChatManager.Instance().getMainHandler().post(() -> PTTClient.getInstance().releaseTalking(conversation));
    }


    @UniJSMethod(uiThread = true)
    public void requestTalk(String strConv, int talkingPriority, JSCallback onStartTalkingCB, JSCallback onTalkingEndCB, JSCallback onRequestFailCB, JSCallback onAmplitudeUpdateCB) {
        Conversation conversation = parseObject(strConv, Conversation.class);
        if (conversation == null) {
            if (onRequestFailCB != null) {
                JSONArray array = new JSONArray();
                array.add(strConv);
                array.add(-1);
                onRequestFailCB.invoke(array);
            }
        }
        PTTClient.getInstance().requestTalk(conversation, new TalkingCallback() {
            @Override
            public void onStartTalking(Conversation conversation) {
                if (onStartTalkingCB != null) {
                    JSONArray array = new JSONArray();
                    array.add(strConv);
                    onStartTalkingCB.invoke(array);
                }
            }

            @Override
            public void onTalkingEnd(Conversation conversation, int reason) {
                if (onTalkingEndCB != null) {
                    JSONArray array = new JSONArray();
                    array.add(strConv);
                    onTalkingEndCB.invoke(array);
                }
            }

            @Override
            public void onRequestFail(Conversation conversation, int errorCode) {
                if (onRequestFailCB != null) {
                    JSONArray array = new JSONArray();
                    array.add(strConv);
                    array.add(errorCode);
                    onRequestFailCB.invoke(array);
                }
            }

            @Override
            public int talkingPriority(Conversation conversation) {
                return talkingPriority;
            }

            @Override
            public void onAmplitudeUpdate(int averageAmplitude) {
                if (onAmplitudeUpdateCB != null) {
                    JSONArray array = new JSONArray();
                    array.add(averageAmplitude);
                    onAmplitudeUpdateCB.invoke(array);
                }
            }
        });
    }

    @UniJSMethod(uiThread = true)
    public void setConversationMaxSpeakerCount(String strConv, int count, JSCallback successCB, JSCallback failCB) {
        Conversation conversation = parseObject(strConv, Conversation.class);
        if (conversation == null) {
            return;
        }
        PTTClient.getInstance().setConversationMaxSpeakerCount(conversation, count, new JSGeneralCallback(successCB, failCB));
    }

    @UniJSMethod(uiThread = true)
    public void setConversationMaxSpeakTime(String strConv, int duration, JSCallback successCB, JSCallback failCB) {
        Conversation conversation = parseObject(strConv, Conversation.class);
        if (conversation == null) {
            return;
        }
        PTTClient.getInstance().setConversationMaxSpeakerCount(conversation, duration, new JSGeneralCallback(successCB, failCB));
    }

    @UniJSMethod(uiThread = true)
    public void setConversationPttSilent(String strConv, boolean silent, JSCallback successCB, JSCallback failCB) {
        Conversation conversation = parseObject(strConv, Conversation.class);
        if (conversation == null) {
            return;
        }
        PTTClient.getInstance().setConversationPttSilent(conversation, silent, new JSGeneralCallback(successCB, failCB));
    }


    @UniJSMethod(uiThread = false)
    public void setEnablePtt(String strConv, boolean enable) {
        Conversation conversation = parseObject(strConv, Conversation.class);
        if (conversation == null) {
            return;
        }
        PTTClient.getInstance().setEnablePtt(conversation, enable);
    }

    @UniJSMethod(uiThread = true)
    public void setSaveVoiceMessage(String strConv, boolean save, JSCallback successCB, JSCallback failCB) {
        Conversation conversation = parseObject(strConv, Conversation.class);
        if (conversation == null) {
            return;
        }
        PTTClient.getInstance().setSaveVoiceMessage(conversation, save, new JSGeneralCallback(successCB, failCB));
    }


    @Override
    public void didUserStartTalking(Conversation conversation, String userId) {
        fireToJS("userStartTalking", conversation, userId);
    }

    @Override
    public void didUserEndTalking(Conversation conversation, String userId) {
        fireToJS("userEndTalking", conversation, userId);
    }

    @Override
    public void didReceiveData(Conversation conversation, String userId, String data) {
        fireToJS("receiveData", conversation, userId, data);
    }

    @Override
    public void didUserAmplitudeUpdate(Conversation conversation, String userId, int averageAmplitude) {
        fireToJS("userAmplitudeUpdate", conversation, userId, averageAmplitude);
    }

    private void fireToJS(String event, Object... args) {
        JSONObject object = new JSONObject();
        JSONArray array = new JSONArray();
        array.add(event);
        for (Object arg : args) {
            array.add(JSONObject.toJSONString(arg, ClientUniAppHookProxy.serializeConfig));
        }
        object.put("args", array);
        object.put("timestamp", System.currentTimeMillis());
        mUniSDKInstance.fireGlobalEventCallback("wfc-ptt-event", object);
    }

    private static <T> T parseObject(String text, Class<T> clazz) {
        try {
            return JSONObject.parseObject(text, clazz);
        } catch (Exception e) {
            Log.e(TAG, "parseObject exception " + clazz.getName());
        }
        return null;
    }
}
