package cn.wildfirechat.uni.pttclient;

import android.app.Application;
import android.util.Log;

import cn.wildfirechat.ptt.PTTClient;
import cn.wildfirechat.remote.ChatManager;
import io.dcloud.feature.uniapp.UniAppHookProxy;

public class PttClientUniAppHookProxy implements UniAppHookProxy {

    private static final String TAG = "PttClientUniHookProxy";

    @Override
    public void onSubProcessCreate(Application application) {
        // TODO
    }

    @Override
    public void onCreate(Application application) {
        Log.e(TAG, "PttClientUniAppHook onCreate");
        // 可重复初始化，但是不能不初始化
        ChatManager.init(application, null);
        PTTClient.getInstance().init(application);
    }
}

