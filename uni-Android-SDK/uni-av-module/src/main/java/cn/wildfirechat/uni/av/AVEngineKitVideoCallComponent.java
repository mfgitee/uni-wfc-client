package cn.wildfirechat.uni.av;

import android.content.Context;
import android.widget.FrameLayout;


import io.dcloud.feature.uniapp.UniSDKInstance;
import io.dcloud.feature.uniapp.ui.action.AbsComponentData;
import io.dcloud.feature.uniapp.ui.component.AbsVContainer;
import io.dcloud.feature.uniapp.ui.component.UniComponent;

//@UniComponentProp(name = "tel")
public class AVEngineKitVideoCallComponent extends UniComponent<FrameLayout> {
    public AVEngineKitVideoCallComponent(UniSDKInstance instance, AbsVContainer parent, int type, AbsComponentData componentData) {
        super(instance, parent, type, componentData);
    }

    public AVEngineKitVideoCallComponent(UniSDKInstance instance, AbsVContainer parent, AbsComponentData componentData) {
        super(instance, parent, componentData);
    }

    @Override
    protected FrameLayout initComponentHostView(Context context) {
        return new FrameLayout(context);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
