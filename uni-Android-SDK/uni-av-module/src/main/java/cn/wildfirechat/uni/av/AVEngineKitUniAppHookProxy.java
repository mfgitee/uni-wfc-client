package cn.wildfirechat.uni.av;

import android.app.Application;
import android.util.Log;

import io.dcloud.feature.uniapp.UniAppHookProxy;

public class AVEngineKitUniAppHookProxy implements UniAppHookProxy {

    private static final String TAG = "UIKitUniAppHookProxy";

    @Override
    public void onSubProcessCreate(Application application) {
        // TODO
    }

    @Override
    public void onCreate(Application application) {
        Log.e(TAG, "AVEngineUniAppHook onCreate");
//        AVEngineKit.init(application, AVEngineCallbackWrapper.INSTANCE);
//        AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = true;
    }
}

