package cn.wildfirechat.uni.av;

import cn.wildfirechat.avenginekit.AVEngineKit;
import cn.wildfirechat.model.Conversation;

public class JSCallSession {
    public String callId;
    public String title;
    public String desc;
    public String initiator;
    public String inviter;
    public AVEngineKit.CallState state;
    public long startTime;
    public long connectedTime;
    public long endTime;
    public Conversation conversation;
    public boolean audioOnly;
    public AVEngineKit.CallEndReason endReason;
    public boolean conference;
    public boolean audience;
    public boolean advanced;
    public boolean audioMuted;
    public boolean videoMuted;

    public static JSCallSession fromCallSession(AVEngineKit.CallSession session) {
        JSCallSession jsCallSession = new JSCallSession();
        jsCallSession.callId = session.getCallId();
        jsCallSession.title = session.getTitle();
        jsCallSession.desc = session.getDesc();
        jsCallSession.initiator = session.initiator;
        jsCallSession.inviter = session.inviter;
        jsCallSession.state = session.getState();
        jsCallSession.startTime = session.getStartTime();
        jsCallSession.connectedTime = session.getConnectedTime();
        jsCallSession.endTime = session.getEndTime();
        jsCallSession.conversation = session.getConversation();
        jsCallSession.audioOnly = session.isAudioOnly();
        jsCallSession.endReason = session.getEndReason();
        jsCallSession.conference = session.isConference();
        jsCallSession.audience = session.isAudience();
        jsCallSession.advanced = session.isAdvanced();
        jsCallSession.videoMuted = session.videoMuted;
        jsCallSession.audioMuted = session.audioMuted;
        return jsCallSession;
    }

}
