package cn.wildfirechat.uni.av;

import org.webrtc.StatsReport;

import java.util.List;

import cn.wildfirechat.avenginekit.AVAudioManager;
import cn.wildfirechat.avenginekit.AVEngineKit;

public class CallSessionCallbackWrapper implements AVEngineKit.CallSessionCallback {
    static CallSessionCallbackWrapper INSTANCE = new CallSessionCallbackWrapper();

    private CallSessionCallbackWrapper() {
    }


    @Override
    public void didCallEndWithReason(AVEngineKit.CallEndReason callEndReason) {
        AVEngineKitModule.fireCallSessionEvent2js("didCallEndWithReason", callEndReason);
        VoipFloatingWindowManager.getInstance().hideFloatWindow();
    }

    @Override
    public void didChangeState(AVEngineKit.CallState callState) {
        AVEngineKitModule.fireCallSessionEvent2js("didChangeState", callState);
    }

    @Override
    public void didParticipantJoined(String userId, boolean screenSharing) {
        AVEngineKitModule.fireCallSessionEvent2js("didParticipantJoined", userId, screenSharing);
    }

    @Override
    public void didParticipantConnected(String userId, boolean screenSharing) {
        AVEngineKitModule.fireCallSessionEvent2js("didParticipantConnected", userId, screenSharing);
    }

    @Override
    public void didParticipantLeft(String userId, AVEngineKit.CallEndReason callEndReason, boolean screenSharing) {
        AVEngineKitModule.fireCallSessionEvent2js("didParticipantLeft", userId, callEndReason, screenSharing);
    }

    @Override
    public void didChangeMode(boolean audioOnly) {
        AVEngineKitModule.fireCallSessionEvent2js("didChangeMode", audioOnly);
    }

    @Override
    public void didChangeInitiator(String initiator) {
        AVEngineKitModule.fireCallSessionEvent2js("didChangeInitiator", initiator);
    }

    @Override
    public void didCreateLocalVideoTrack() {
        AVEngineKitModule.fireCallSessionEvent2js("didCreateLocalVideoTrack");
    }

    @Override
    public void didReceiveRemoteVideoTrack(String userId, boolean screenSharing) {
        AVEngineKitModule.fireCallSessionEvent2js("didReceiveRemoteVideoTrack", userId, screenSharing);
    }

    @Override
    public void didRemoveRemoteVideoTrack(String userId) {
        AVEngineKitModule.fireCallSessionEvent2js("didRemoveRemoteVideoTrack", userId);
    }

    @Override
    public void didError(String error) {
        // TODO
    }

    @Override
    public void didGetStats(StatsReport[] statsReports) {
        AVEngineKitModule.fireCallSessionEvent2js("didGetStats", (Object[]) statsReports);
    }

    @Override
    public void didVideoMuted(String userId, boolean videoMuted) {
        AVEngineKitModule.fireCallSessionEvent2js("didVideoMuted", userId, videoMuted);
    }

    @Override
    public void didReportAudioVolume(String userId, int volume) {
        AVEngineKitModule.fireCallSessionEvent2js("didReportAudioVolume", userId, volume);
    }

    @Override
    public void didAudioDeviceChanged(AVAudioManager.AudioDevice audioDevice) {
        AVEngineKitModule.fireCallSessionEvent2js("didAudioDeviceChanged", audioDevice);
    }

    @Override
    public void didChangeType(String userId, boolean audience, boolean screenSharing) {
        AVEngineKitModule.fireCallSessionEvent2js("didChangeType", userId, audience, screenSharing);
    }

    @Override
    public void didMuteStateChanged(List<String> participants) {
        AVEngineKitModule.fireCallSessionEvent2js("didMuteStateChanged", participants);
    }

    @Override
    public void didMediaLostPacket(String media, int lostPacket, boolean screenSharing) {
        AVEngineKitModule.fireCallSessionEvent2js("didMediaLostPacket", media, lostPacket, screenSharing);
    }

    @Override
    public void didMediaLostPacket(String userId, String media, int lostPacket, boolean uplink, boolean screenSharing) {
        // js 不支持方法重载，故使用不同名字
        AVEngineKitModule.fireCallSessionEvent2js("didUserMediaLostPacket", userId, media, lostPacket, uplink, screenSharing);
    }
}
