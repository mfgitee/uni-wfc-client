package cn.wildfirechat.uni.av;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.taobao.weex.ComponentObserver;
import com.taobao.weex.WXSDKManager;
import com.taobao.weex.bridge.JSCallback;
import com.taobao.weex.ui.component.WXComponent;

import org.webrtc.RendererCommon;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import cn.wildfirechat.avenginekit.AVAudioManager;
import cn.wildfirechat.avenginekit.AVEngineKit;
import cn.wildfirechat.model.Conversation;
import cn.wildfirechat.remote.ChatManager;
import cn.wildfirechat.uni.client.ClientUniAppHookProxy;
import io.dcloud.feature.uniapp.AbsSDKInstance;
import io.dcloud.feature.uniapp.annotation.UniJSMethod;
import io.dcloud.feature.uniapp.common.UniModule;

/**
 * 野火IM Android uikit扩展
 */
public class AVEngineKitModule extends UniModule implements ComponentObserver, OnVoipFloatingWindowClickListener {

    private static final String TAG = "AVEngineModule";

    static AbsSDKInstance uniSDKInstance;

    @UniJSMethod(uiThread = true)
    public void initAVEngineKit() {
        uniSDKInstance = mUniSDKInstance;
        AVEngineKit.init(uniSDKInstance.getContext(), AVEngineCallbackWrapper.INSTANCE);
        AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = true;

        VoipFloatingWindowManager.getInstance().init(uniSDKInstance.getContext(), this);
    }

    /**
     * sdk后处理
     */
    @Override
    public void onActivityDestroy() {
        super.onActivityDestroy();
        componentStringMap.clear();
        // 删除监听器
        Log.i(TAG, "应用销毁后处理");
    }

    @UniJSMethod(uiThread = false)
    public void addICEServer(String url, String name, String password) {
        AVEngineKit.Instance().addIceServer(url, name, password);
    }

    @UniJSMethod(uiThread = false)
    public String startSingleCall(String target, boolean audioOnly) {
        return startSingleCallWithLine(target, 0, audioOnly);
    }

    @UniJSMethod(uiThread = false)
    public String startSingleCallWithLine(String target, int line, boolean audioOnly) {
        AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = false;
        Conversation conversation = new Conversation(Conversation.ConversationType.Single, target, line);
        AVEngineKit.CallSession session = AVEngineKit.Instance().startCall(conversation, Collections.singletonList(conversation.target), audioOnly, CallSessionCallbackWrapper.INSTANCE);
        if (session != null) {
            session.autoSwitchVideoType = false;
            AudioManager audioManager = (AudioManager) uniSDKInstance.getContext().getSystemService(Context.AUDIO_SERVICE);
            audioManager.setMode(audioOnly ? AudioManager.MODE_IN_COMMUNICATION : AudioManager.MODE_NORMAL);
            audioManager.setSpeakerphoneOn(!audioOnly);
            return JSONObject.toJSONString(JSCallSession.fromCallSession(session), ClientUniAppHookProxy.serializeConfig);
        }
        return null;
    }

    @UniJSMethod(uiThread = true)
    public void leaveConference(String callId, boolean destroyRoom) {
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session != null && TextUtils.equals(session.getCallId(), callId)) {
            session.leaveConference(destroyRoom);
        }
    }

    @UniJSMethod(uiThread = true)
    public void kickoffParticipant(String callId, String userId, JSCallback successCB, JSCallback failCB) {
        JSAVGeneralCallback jsavGeneralCallback = new JSAVGeneralCallback(successCB, failCB);
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session != null && TextUtils.equals(session.getCallId(), callId)) {
            session.kickoffParticipant(userId, jsavGeneralCallback);
        }
    }

    private Map<WXComponent, Pair<String, Boolean>> componentStringMap = new WeakHashMap<>();

    @UniJSMethod(uiThread = true)
    public void setLocalVideoView(String callId, String ref) {
        Log.d(TAG, "setLocalVideoView " + ref);
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session == null) {
            return;
        }
        WXComponent component = WXSDKManager.getInstance().getWXRenderManager().getWXComponent(this.mWXSDKInstance.getInstanceId(), ref);
        FrameLayout frameLayout = (FrameLayout) component.getHostView();
        if (frameLayout == null) {
            Log.e(TAG, "setLocalVideoView frameLayout is null");
            this.mWXSDKInstance.setComponentObserver(this);
            componentStringMap.put(component, new Pair<>(ChatManager.Instance().getUserId(), false));
        } else {
            session.setupLocalVideoView(frameLayout, RendererCommon.ScalingType.SCALE_ASPECT_BALANCED);
        }
    }

    @UniJSMethod(uiThread = true)
    public void setRemoteVideoView(String callId, String userId, boolean screenSharing, String ref) {
        Log.d(TAG, "setRemoteVideoView " + userId + " " + screenSharing + " " + ref);
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session == null) {
            return;
        }

        WXComponent component = WXSDKManager.getInstance().getWXRenderManager().getWXComponent(this.mWXSDKInstance.getInstanceId(), ref);
        FrameLayout frameLayout = (FrameLayout) component.getHostView();
        if (frameLayout == null) {
            Log.e(TAG, "setRemoteVideoView frameLayout is null");
            this.mWXSDKInstance.setComponentObserver(this);
            componentStringMap.put(component, new Pair<>(userId, screenSharing));
        } else {
            this.resetRemoteVideoViewLayoutParams(frameLayout);
            Log.d(TAG, "setRemoteVideoView " + userId + " " + screenSharing + " " + frameLayout);
            session.setupRemoteVideoView(userId, screenSharing, frameLayout, RendererCommon.ScalingType.SCALE_ASPECT_BALANCED);
        }
    }

    @UniJSMethod(uiThread = true)
    public void setParticipantVideoType(String callId, String userId, boolean screenSharing, int videoType) {
        if (videoType < 0 || videoType > 2) {
            Log.e(TAG, "setParticipantVideoType videoType error: " + videoType);
            return;
        }
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session != null) {
            session.setParticipantVideoType(userId, screenSharing, AVEngineKit.VideoType.values()[videoType]);
        }
    }

    @UniJSMethod(uiThread = false)
    public String getParticipantProfiles(String callId) {
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session != null) {
            List<AVEngineKit.ParticipantProfile> profiles = session.getParticipantProfiles();
            return JSONObject.toJSONString(profiles, ClientUniAppHookProxy.serializeConfig);
        }
        return null;
    }

    @UniJSMethod(uiThread = false)
    public String getParticipantProfile(String callId, String userId, boolean screenSharing) {
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session != null) {
            AVEngineKit.ParticipantProfile profile = session.getParticipantProfile(userId, screenSharing);
            return JSONObject.toJSONString(profile, ClientUniAppHookProxy.serializeConfig);
        }
        return null;
    }

    @UniJSMethod(uiThread = false)
    public String getMyProfile(String callId) {
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session != null) {
            AVEngineKit.ParticipantProfile profile = session.getMyProfile();
            return JSONObject.toJSONString(profile, ClientUniAppHookProxy.serializeConfig);
        }
        return null;
    }

    @UniJSMethod(uiThread = false)
    public void muteAudio(String callId, boolean mute) {
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session != null) {
            session.muteAudio(mute);
        }
    }

    @UniJSMethod(uiThread = false)
    public void muteVideo(String callId, boolean mute) {
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session != null) {
            session.muteVideo(mute);
        }
    }

    @UniJSMethod(uiThread = true)
    public void setSpeakerOn(String callId, boolean speakerOn) {
        AVAudioManager audioManager = AVEngineKit.Instance().getAVAudioManager();
        AVAudioManager.AudioDevice currentAudioDevice = audioManager.getSelectedAudioDevice();
        if (currentAudioDevice == AVAudioManager.AudioDevice.WIRED_HEADSET || currentAudioDevice == AVAudioManager.AudioDevice.BLUETOOTH) {
            Log.d(TAG, "setSpeakerOn failed, current audio device: " + currentAudioDevice.name());
            return;
        }
        audioManager.selectAudioDevice(speakerOn ? AVAudioManager.AudioDevice.SPEAKER_PHONE : AVAudioManager.AudioDevice.EARPIECE);
    }

    @UniJSMethod(uiThread = true)
    public void downgrade2Voice(String callId) {
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session != null) {
            session.setAudioOnly(true);
        }
    }


    @UniJSMethod(uiThread = false)
    public String startMultiCall(String groupId, List<String> participants, boolean audioOnly) {
        AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = true;
        Conversation conversation = new Conversation(Conversation.ConversationType.Group, groupId);
        AVEngineKit.CallSession session = AVEngineKit.Instance().startCall(conversation, participants, audioOnly, CallSessionCallbackWrapper.INSTANCE);
        if (session != null) {
            session.autoSwitchVideoType = false;
            AudioManager audioManager = (AudioManager) uniSDKInstance.getContext().getSystemService(Context.AUDIO_SERVICE);
            audioManager.setMode(AudioManager.MODE_NORMAL);
            audioManager.setSpeakerphoneOn(true);
            return JSONObject.toJSONString(JSCallSession.fromCallSession(session), ClientUniAppHookProxy.serializeConfig);
        }
        return null;
    }

    @UniJSMethod(uiThread = false)
    public String startConference(String callId, boolean audioOnly, String pin, String host, String title, String desc, boolean audience, boolean advance, boolean record, String callExtra) {
        AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = true;
        AVEngineKit.CallSession session = AVEngineKit.Instance().startConference(callId, audioOnly, pin, host, title, desc, audience, advance, record, CallSessionCallbackWrapper.INSTANCE);
        if (session != null) {
            session.autoSwitchVideoType = false;
            return JSONObject.toJSONString(JSCallSession.fromCallSession(session), ClientUniAppHookProxy.serializeConfig);
        } else {
            return null;
        }
    }

    @UniJSMethod(uiThread = false)
    public String joinConference(String callId, boolean audioOnly, String pin, String host, String title, String desc, boolean audience, boolean advance, boolean muteAudio, boolean muteVideo, String extra) {
        AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = true;
        AVEngineKit.CallSession session = AVEngineKit.Instance().joinConference(callId, audioOnly, pin, host, title, desc, audience, advance, muteAudio, muteVideo, CallSessionCallbackWrapper.INSTANCE);
        if (session != null) {
            session.autoSwitchVideoType = false;
            return JSONObject.toJSONString(JSCallSession.fromCallSession(session), ClientUniAppHookProxy.serializeConfig);
        }
        return null;
    }

    @UniJSMethod(uiThread = false)
    public boolean isSupportMultiCall() {
        return AVEngineKit.isSupportMultiCall();
    }

    @UniJSMethod(uiThread = false)
    public boolean isSupportConference() {
        return AVEngineKit.isSupportConference();
    }

    @UniJSMethod(uiThread = false)
    public void setVideoProfile(int profile, boolean swapWidthHeight) {
        AVEngineKit.Instance().setVideoProfile(profile, swapWidthHeight);
    }


    @UniJSMethod(uiThread = false)
    public void answerCall(String callId, boolean audioOnly) {
        AVEngineKit.CallSession callSession = AVEngineKit.Instance().getCurrentSession();
        if (callSession != null
            && TextUtils.equals(callId, callSession.getCallId())
            && callSession.getState() == AVEngineKit.CallState.Incoming) {
            callSession.setCallback(CallSessionCallbackWrapper.INSTANCE);
            Conversation conversation = callSession.getConversation();
            if (conversation.type == Conversation.ConversationType.Single) {
                AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = false;
            } else {
                AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = true;
            }
            callSession.answerCall(audioOnly);
        } else {
            // TODO
        }
    }

    @UniJSMethod(uiThread = true)
    public void endCall(String callId) {
        Log.e(TAG, "endCall " + callId);
        AVEngineKit.CallSession callSession = AVEngineKit.Instance().getCurrentSession();
        if (callSession != null
            && (callSession.getState() != AVEngineKit.CallState.Idle
            && callSession.getCallId().equals(callId))) {
            callSession.endCall();
            callSession.setCallback(null);
        }
    }

    @UniJSMethod(uiThread = false)
    public String currentCallSession() {
        AVEngineKit.CallSession callSession = AVEngineKit.Instance().getCurrentSession();
        if (callSession == null) {
            return null;
        }
        return JSONObject.toJSONString(JSCallSession.fromCallSession(callSession), ClientUniAppHookProxy.serializeConfig);
    }

    @UniJSMethod(uiThread = false)
    public boolean canSwitchAudience(String callId) {
        if (AVEngineKit.isSupportConference()) {
            AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
            return session != null && session.canSwitchAudience();
        }
        return false;
    }

    @UniJSMethod(uiThread = false)
    public boolean switchAudience(String callId, boolean audience) {
        if (AVEngineKit.isSupportConference()) {
            AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
            return session != null && session.switchAudience(audience);
        }
        return false;
    }

    @UniJSMethod(uiThread = true)
    public void switchCamera(String callId) {
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session != null) {
            session.switchCamera();
        }
    }

    @UniJSMethod(uiThread = true)
    public void inviteNewParticipant(String callId, List<String> userIds) {
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session != null) {
            session.inviteNewParticipants(userIds);
        }
    }


    @UniJSMethod(uiThread = false)
    public boolean checkOverlayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Context context = uniSDKInstance.getContext();
            if (!Settings.canDrawOverlays(uniSDKInstance.getContext())) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + context.getPackageName()));

                List<ResolveInfo> infos = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                if (!infos.isEmpty()) {
                    context.startActivity(intent);
                }
                return false;
            }
        }
        return true;
    }

    @UniJSMethod(uiThread = true)
    public void minimize(String callId, String focusVideoUser) {
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session != null) {
            VoipFloatingWindowManager.getInstance().showFloatingWindow(session, focusVideoUser);
        }
    }

    @Override
    public void onCreate(WXComponent component) {

    }

    @Override
    public void onPreDestory(WXComponent component) {

    }

    @Override
    public void onViewCreated(WXComponent component, View view) {
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session == null) {
            return;
        }
        for (Map.Entry<WXComponent, Pair<String, Boolean>> entry : componentStringMap.entrySet()) {
            WXComponent wxComponent = entry.getKey();
            if (component == wxComponent) {
                Pair<String, Boolean> pair = entry.getValue();
                if (pair.first.equals(ChatManager.Instance().getUserId())) {
                    session.setupLocalVideoView((FrameLayout) wxComponent.getHostView(), RendererCommon.ScalingType.SCALE_ASPECT_BALANCED);
                } else {
                    FrameLayout frameLayout = (FrameLayout) wxComponent.getHostView();
                    this.resetRemoteVideoViewLayoutParams(frameLayout);
                    session.setupRemoteVideoView(pair.first, pair.second, frameLayout, RendererCommon.ScalingType.SCALE_ASPECT_BALANCED);
                }
            }
        }
        componentStringMap.remove(component);
    }

    @Override
    public void onClickFloatingWindow() {
        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
        if (session != null) {
            String type = "conference";
            Conversation conversation = session.getConversation();
            if (conversation != null) {
                type = conversation.type == Conversation.ConversationType.Single ? "single" : "multi";
            }
            fireCallSessionEvent2js("resumeVoipPage", type);
        }
    }

    // PC 端发起音视频通话时，由于方向是横屏，不是居中显示，所以设置强制居中
    private void resetRemoteVideoViewLayoutParams(FrameLayout frameLayout) {
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        frameLayout.setLayoutParams(params);
    }

    static void fireAVEngineEvent2js(String event, Object... args) {
        fireEngineEvent2js("wfc-av-event", event, args);
    }

    static void fireCallSessionEvent2js(String event, Object... args) {
        fireEngineEvent2js("wfc-av-session-event", event, args);
    }

    private static void fireEngineEvent2js(String channel, String event, Object... args) {
        if (uniSDKInstance != null) {
            JSONObject object = new JSONObject();
            JSONArray array = new JSONArray();
            array.add(event);
            Collections.addAll(array, args);
            object.put("args", array);
            uniSDKInstance.fireGlobalEventCallback(channel, object);
        }
    }
}
