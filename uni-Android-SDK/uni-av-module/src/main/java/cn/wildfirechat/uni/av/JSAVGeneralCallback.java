package cn.wildfirechat.uni.av;

import com.taobao.weex.bridge.JSCallback;

import cn.wildfirechat.avenginekit.AVEngineKit;


public class JSAVGeneralCallback implements AVEngineKit.GeneralCallback {

    private final JSCallback successCB;
    private final JSCallback failCB;

    public JSAVGeneralCallback(JSCallback successCB, JSCallback failCB) {
        this.successCB = successCB;
        this.failCB = failCB;
    }

    @Override
    public void onSuccess() {
        if (successCB != null) {
            successCB.invoke(null);
        }
    }

    @Override
    public void onFailure(int i) {
        if (failCB != null) {
            failCB.invoke(i);
        }
    }
}
