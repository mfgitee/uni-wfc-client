package cn.wildfirechat.uni.av;

import com.alibaba.fastjson.JSON;

import cn.wildfirechat.avenginekit.AVEngineKit;
import cn.wildfirechat.uni.client.ClientUniAppHookProxy;

public class AVEngineCallbackWrapper implements AVEngineKit.AVEngineCallback {

    public static AVEngineCallbackWrapper INSTANCE = new AVEngineCallbackWrapper();

    private AVEngineCallbackWrapper() {
    }


    @Override
    public void onReceiveCall(AVEngineKit.CallSession callSession) {
        callSession.autoSwitchVideoType = false;
        AVEngineKitModule.fireAVEngineEvent2js("onReceiveCall", JSON.toJSONString(JSCallSession.fromCallSession(callSession)), ClientUniAppHookProxy.serializeConfig);
    }

    @Override
    public void shouldStartRing(boolean b) {
        AVEngineKitModule.fireAVEngineEvent2js("shouldStartRing", b);
    }

    @Override
    public void shouldStopRing() {
        AVEngineKitModule.fireAVEngineEvent2js("shouldStopRing");
    }

    @Override
    public void didCallEnded(AVEngineKit.CallEndReason reason, int duration) {
        AVEngineKitModule.fireAVEngineEvent2js("didCallEnded", reason, duration);
    }

}
