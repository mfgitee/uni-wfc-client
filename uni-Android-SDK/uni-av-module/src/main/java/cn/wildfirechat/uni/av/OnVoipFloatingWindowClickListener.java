package cn.wildfirechat.uni.av;

public interface OnVoipFloatingWindowClickListener {
    void onClickFloatingWindow();
}
