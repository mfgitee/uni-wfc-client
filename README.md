# 野火UniApp原生插件
野火UniApp原生插件源码，包括Android平台源码和iOS平台源码。

## 目录说明
* [uni-Android-SDK](./uni-Android-SDK)目录是Android平台插件的源码；
* [uni-iOS-SDK](./uni-iOS-SDK)目录是iOS平台插件的源码；
* [nativeplugin](./nativeplugin)目录是插件的模版，把Android平台和iOS平台插件源码编译成库后，放到模版下的对应目录即可离线使用。

## Android平台编译
终端窗口进入到[uni-Android-SDK](./uni-Android-SDK)目录，执行命令```./gradlew build aR```，编译成功后，把```./client/build/outputs/aar/client-release.aar```文件和```./uni-client-module/build/outputs/aar/uni-client-module-release.aar```拷贝到插件模版的```android```目录下。

## iOS平台编译
终端窗口进入到[uni-iOS-SDK](./uni-iOS-SDK)目录，执行命令```sh release_plugin.sh```，编译成功后生成的库文件拷贝到插件模版的```ios```目录下。

## UniApp野火IM架构
![架构图](./assets/uniapp_app_arch.png)

如上图，最下部Android和iOS的SDK是野火IM的标准原生SDK；野火SDK之上是基于UniApp规范的插件原生代码。当应用选用野火插件后，UniPlatform就提供了JS的接口可以调用原生接口。```wfc client```是对接口的进一步封装，封装出更容易使用的接口。最后是使用```wfc client```接口的UI界面。

## 使用方法
请参考[插件使用说明](./nativeplugin/wf-uni-wfc-client/README.md)。

## 音视频的历史问题
野火在uniapp项目上的音视频的实现有2个方案：
1. 方案1：使用原生UI，就是把android平台和ios平台的音视频SDK和UI代码全都集成到uniapp项目中。这种方案的问题是引入的无关代码太多，且原生UI无法修改，跟uniapp互通也很不方便，不利于二次开发。
2. 方案2：是使用原生的音视频SDK，但UI层使用vuejs编写，UI和SDK使用uniapp插件的方式沟通。这种方案引入的SDK比较小，且修改方便，有利于大家做自定义。

我们在2023.11.29日起，正式采用方案2，同时方案1保留在```rtc-native-ui```分支。已经使用方案1的用户可以继续使用```rtc-native-ui```的分支，如果有需求可以切换到```main```分支去，以后我们的开发重点将会放到```main```分支上去。

## 鸣谢
本项目Android平台参考了[wildfire-uniplugin-demo](https://github.com/PentaTea/wildfire-uniplugin-demo)，特此感谢！
