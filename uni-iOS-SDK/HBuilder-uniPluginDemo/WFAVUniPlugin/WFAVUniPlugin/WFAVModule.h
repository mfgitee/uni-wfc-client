//
//  WFAVModule.h
//  WFAVUniPlugin
//
//  Created by Rain on 2023/11/17.
//

#import "DCUniModule.h"

NS_ASSUME_NONNULL_BEGIN
@class WFAVRtcView;

@interface WFAVModule : DCUniModule
- (void)onViewCreated:(WFAVRtcView *)rtcView;
@end

NS_ASSUME_NONNULL_END
