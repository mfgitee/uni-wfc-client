//
//  WFAVProxy.h
//  WFAVUniPlugin
//
//  Created by Rain on 2023/11/17.
//

#import <Foundation/Foundation.h>
#import "UniPluginProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface WFAVProxy : NSObject<UniPluginProtocol>

@end

NS_ASSUME_NONNULL_END
