//
//  WFAVModule.m
//  WFAVUniPlugin
//
//  Created by Rain on 2023/11/17.
//

#import "WFAVModule.h"
#import "WFAVRtcView.h"
#import <WFChatClient/WFCChatClient.h>
#import <WFAVEngineKit/WFAVEngineKit.h>
#import "WFAVFloatingWindow.h"

@interface WFAVCallSessionDelegater : NSObject <WFAVCallSessionDelegate>
@property(nonatomic, strong)NSString *callId;
@property (nonatomic, weak) DCUniSDKInstance *uniInstance;
@end

@implementation WFAVCallSessionDelegater
- (instancetype)initWithCallId:(NSString *)callId uniInstance:(DCUniSDKInstance *)uniInstance {
    self = [super init];
    if(self) {
        self.callId = callId;
        self.uniInstance = uniInstance;
    }
    return self;
}
- (void)didCallEndWithReason:(WFAVCallEndReason)reason {
    [self onIndication:@[@"didCallEndWithReason", @(reason)]];
}

- (void)didChangeInitiator:(NSString * _Nullable)initiator { 
    if(!initiator) initiator = @"";
    [self onIndication:@[@"didChangeInitiator", initiator]];
}

- (void)didChangeMode:(BOOL)isAudioOnly { 
    [self onIndication:@[@"didChangeMode", @(isAudioOnly)]];
}

- (void)didChangeState:(WFAVEngineState)state { 
    [self onIndication:@[@"didChangeState", @(state)]];
}

- (void)didCreateLocalVideoTrack:(RTCVideoTrack * _Nonnull)localVideoTrack { 
    [self onIndication:@[@"didCreateLocalVideoTrack"]];
}

- (void)didError:(NSError * _Nonnull)error { 
    [self onIndication:@[@"didError", error.description]];
}

- (void)didGetStats:(NSArray * _Nonnull)stats { 
    
}

- (void)didParticipantConnected:(NSString * _Nonnull)userId screenSharing:(BOOL)screenSharing { 
    [self onIndication:@[@"didParticipantConnected", userId, @(screenSharing)]];
}

- (void)didParticipantJoined:(NSString * _Nonnull)userId screenSharing:(BOOL)screenSharing { 
    [self onIndication:@[@"didParticipantJoined", userId, @(screenSharing)]];
}

- (void)didParticipantLeft:(NSString * _Nonnull)userId screenSharing:(BOOL)screenSharing withReason:(WFAVCallEndReason)reason { 
    [self onIndication:@[@"didParticipantLeft", userId, @(screenSharing)]];
}

- (void)didReceiveRemoteVideoTrack:(RTCVideoTrack * _Nonnull)remoteVideoTrack fromUser:(NSString * _Nonnull)userId screenSharing:(BOOL)screenSharing { 
    [self onIndication:@[@"didReceiveRemoteVideoTrack", userId, @(screenSharing)]];
}

- (void)didVideoMuted:(BOOL)videoMuted fromUser:(NSString * _Nonnull)userId { 
    [self onIndication:@[@"didVideoMuted", userId, @(videoMuted)]];
}

- (void)didReportAudioVolume:(NSInteger)volume ofUser:(NSString *_Nonnull)userId {
    [self onIndication:@[@"didReportAudioVolume", userId, @(volume)]];
}

- (void)didChangeType:(BOOL)audience ofUser:(NSString *_Nonnull)userId screenSharing:(BOOL)screenSharing {
    [self onIndication:@[@"didChangeType", userId, @(audience), @(screenSharing)]];
}

- (void)didChangeAudioRoute {
/*
public static enum AudioDevice {
 SPEAKER_PHONE,
 WIRED_HEADSET,
 EARPIECE,
 BLUETOOTH,
 NONE;
}
*/
    int audioDevice;
    if([[WFAVEngineKit sharedEngineKit].currentSession isSpeaker]) {
        audioDevice = 0;
    } else if([[WFAVEngineKit sharedEngineKit].currentSession isHeadsetPluggedIn]) {
        audioDevice = 1;
    } else if([[WFAVEngineKit sharedEngineKit].currentSession isBluetoothSpeaker]) {
        audioDevice = 3;
    } else {
        audioDevice = 2;
    }
    [self onIndication:@[@"didAudioDeviceChanged", @(audioDevice)]];
}

- (void)didMuteStateChanged:(NSArray<NSString *> *_Nonnull)userIds {
    [self onIndication:@[@"didMuteStateChanged", userIds]];
}

- (void)didMedia:(NSString *_Nullable)media lostPackage:(int)lostPackage screenSharing:(BOOL)screenSharing {
    [self onIndication:@[@"didMediaLostPacket", media, @(lostPackage), @(screenSharing)]];
}

- (void)didMedia:(NSString *_Nullable)media lostPackage:(int)lostPackage uplink:(BOOL)uplink ofUser:(NSString *_Nonnull)userId screenSharing:(BOOL)screenSharing {
    [self onIndication:@[@"didUserMediaLostPacket", userId, media, @(lostPackage), @(screenSharing)]];
}

- (void)onIndication:(NSArray *)args {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    dict[@"args"] = args;
    dict[@"timestamp"] = @([[[NSDate alloc] init] timeIntervalSince1970]);
    dict[@"callId"] = self.callId;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ONWFCAVSessionEvent" object:@"wfc-av-session-event" userInfo:dict];
}
@end



@interface WFAVModule () <WFAVEngineDelegate>
@property(nonatomic, strong)NSMutableDictionary<WFAVRtcView*, NSArray*> *componentStringMap;
@property(nonatomic, strong)NSMutableDictionary<NSString *, WFAVCallSessionDelegater*> *delegaterMap;
@end

WFAVModule *gWFAVModule = nil;

@implementation WFAVModule
- (instancetype)init {
    self = [super init];
    if (self) {
        self.componentStringMap = [[NSMutableDictionary alloc] init];
        self.delegaterMap = [[NSMutableDictionary alloc] init];
    }
    return self;
}
//@UniJSMethod(uiThread = true)
//public void initAVEngineKit() {
//    uniSDKInstance = mUniSDKInstance;
//    AVEngineKit.init(uniSDKInstance.getContext(), AVEngineCallbackWrapper.INSTANCE);
//    AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = true;
//}
UNI_EXPORT_METHOD_SYNC(@selector(initAVEngineKit))
- (void)initAVEngineKit {
    NSLog(@"call client init");
    [WFAVEngineKit notRegisterVoipPushService];
    [WFAVEngineKit sharedEngineKit].delegate = self;
    gWFAVModule = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSessioEvent:) name:@"ONWFCAVSessionEvent" object:nil];
}

- (void)onReceiveSessioEvent:(NSNotification *)notification {
    [self.uniInstance fireGlobalEvent:@"wfc-av-session-event" params:notification.userInfo];
}
///**
// * sdk后处理
// */
//@Override
//public void onActivityDestroy() {
//    super.onActivityDestroy();
//    componentStringMap.clear();
//    // 删除监听器
//    Log.i(TAG, "应用销毁后处理");
//}
//
//@UniJSMethod(uiThread = false)
//public void addICEServer(String url, String name, String password) {
//    AVEngineKit.Instance().addIceServer(url, name, password);
//}
UNI_EXPORT_METHOD_SYNC(@selector(addICEServer:name:password:))
- (void)addICEServer:(NSString *)url name:(NSString *)name password:(NSString *)password {
    [[WFAVEngineKit sharedEngineKit] addIceServer:url userName:name password:password];
}

//@UniJSMethod(uiThread = false)
//public String startSingleCall(String target, boolean audioOnly) {
//    return startSingleCallWithLine(target, 0, audioOnly);
//}
UNI_EXPORT_METHOD_SYNC(@selector(startSingleCall:audioOnly:))
- (NSString *)startSingleCall:(NSString *)userId audioOnly:(BOOL)audioOnly {
    return [self startSingleCallWithLine:userId line:0 audioOnly:audioOnly];
}
//@UniJSMethod(uiThread = false)
//public String startSingleCallWithLine(String target, int line, boolean audioOnly) {
//    AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = false;
//    Conversation conversation = new Conversation(Conversation.ConversationType.Single, target, line);
//    AVEngineKit.CallSession session = AVEngineKit.Instance().startCall(conversation, Collections.singletonList(conversation.target), audioOnly, CallSessionCallbackWrapper.INSTANCE);
//    if (session != null) {
//        AudioManager audioManager = (AudioManager) uniSDKInstance.getContext().getSystemService(Context.AUDIO_SERVICE);
//        audioManager.setMode(audioOnly ? AudioManager.MODE_IN_COMMUNICATION : AudioManager.MODE_NORMAL);
//        audioManager.setSpeakerphoneOn(!audioOnly);
//        return JSONObject.toJSONString(JSCallSession.fromCallSession(session), ClientUniAppHookProxy.serializeConfig);
//    }
//    return null;
//}
UNI_EXPORT_METHOD_SYNC(@selector(startSingleCallWithLine:line:audioOnly:))
- (NSString *)startSingleCallWithLine:(NSString *)userId line:(int)line audioOnly:(BOOL)audioOnly {
    WFAVCallSessionDelegater *delegater = [[WFAVCallSessionDelegater alloc] initWithCallId:nil uniInstance:self.uniInstance];
    __block WFAVCallSession *session;
    dispatch_sync(dispatch_get_main_queue(), ^{
        session = [[WFAVEngineKit sharedEngineKit] startCall:@[userId] audioOnly:audioOnly callExtra:nil conversation:[WFCCConversation conversationWithType:Single_Type target:userId line:line] sessionDelegate:delegater];
    });
    
    if(session) {
        delegater.callId = session.callId;
        [self.delegaterMap setValue:delegater forKey:session.callId];
        return [self callSession2String:session];
    }
    return nil;
}

//@UniJSMethod(uiThread = true)
//public void leaveConference(String callId, boolean destroyRoom) {
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session != null && TextUtils.equals(session.getCallId(), callId)) {
//        session.leaveConference(destroyRoom);
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(leaveConference:destroyRoom:))
- (void)leaveConference:(NSString *)callId destroyRoom:(BOOL)destroyRoom {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
        if(session != nil && [session.callId isEqualToString:callId]) {
            [session leaveConference:destroyRoom];
        }
    });
}

//public void kickoffParticipant(String callId, String userId, JSCallback successCB, JSCallback failCB) {
//    JSAVGeneralCallback jsavGeneralCallback = new JSAVGeneralCallback(successCB, failCB);
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session != null && TextUtils.equals(session.getCallId(), callId)) {
//        session.kickoffParticipant(userId, jsavGeneralCallback);
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(kickoffParticipant:userId:success:error:))
- (void)kickoffParticipant:(NSString *)callId userId:(NSString *)userId success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
        if(session != nil && [session.callId isEqualToString:callId]) {
            [session kickoffParticipant:userId success:^{
                successCB(nil, false);
            } error:^(int error_code) {
                errorCB(@(error_code), NO);
            }];
        }
    });
}

//private Map<WXComponent, Pair<String, Boolean>> componentStringMap = new WeakHashMap<>();
//
//@UniJSMethod(uiThread = true)
//public void setLocalVideoView(String userId, String ref) {
//    Log.d(TAG, "setLocalVideoView " + userId + " " + ref);
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session == null) {
//        return;
//    }
//    WXComponent component = WXSDKManager.getInstance().getWXRenderManager().getWXComponent(this.mWXSDKInstance.getInstanceId(), ref);
//    FrameLayout frameLayout = (FrameLayout) component.getHostView();
//    if (frameLayout == null) {
//        Log.e(TAG, "setLocalVideoView frameLayout is null");
//        this.mWXSDKInstance.setComponentObserver(this);
//        componentStringMap.put(component, new Pair<>(userId, false));
//    } else {
//        session.setupLocalVideoView(frameLayout, RendererCommon.ScalingType.SCALE_ASPECT_BALANCED);
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(setLocalVideoView:view:))
- (void)setLocalVideoView:(NSString *)callId view:(NSString *)viewRef {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
        if(session.state == kWFAVEngineStateIdle) {
            return;
        }
        
        WXComponent *component = [self.uniInstance.componentManager componentForRef:viewRef];
        if(component.view == nil) {
            self.componentStringMap[component] = @[[WFCCNetworkService sharedInstance].userId, @(false)];
        } else {
            if(!component.view.frame.size.width) {
                NSLog(@"视频窗口(%@)大小为0，视频将无法显示。这个问题一般是setLocalVideoView发生在布局之前导致的，请在布局之后调用这个方法", viewRef);
            }
            component.view.clipsToBounds = YES;
            component.view.layer.masksToBounds = YES;
            [session setupLocalVideoView:component.view scalingType:kWFAVVideoScalingTypeAspectFit];
        }
    });
}

//@UniJSMethod(uiThread = true)
//public void setRemoteVideoView(String userId, boolean screenSharing, String ref) {
//    Log.d(TAG, "setRemoteVideoView " + userId + " " + screenSharing + " " + ref);
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session == null) {
//        return;
//    }
//
//    WXComponent component = WXSDKManager.getInstance().getWXRenderManager().getWXComponent(this.mWXSDKInstance.getInstanceId(), ref);
//    FrameLayout frameLayout = (FrameLayout) component.getHostView();
//    if (frameLayout == null) {
//        Log.e(TAG, "setRemoteVideoView frameLayout is null");
//        this.mWXSDKInstance.setComponentObserver(this);
//        componentStringMap.put(component, new Pair<>(userId, screenSharing));
//    } else {
//        this.resetRemoteVideoViewLayoutParams(frameLayout);
//        Log.d(TAG, "setRemoteVideoView " + userId + " " + screenSharing + " " + frameLayout);
//        session.setupRemoteVideoView(userId, screenSharing, frameLayout, RendererCommon.ScalingType.SCALE_ASPECT_BALANCED);
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(setRemoteVideoView:userId:screenSharing:view:))
- (void)setRemoteVideoView:(NSString *)callId userId:(NSString *)userId screenSharing:(BOOL)screenSharing view:(NSString *)viewRef {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
        if(session.state == kWFAVEngineStateIdle) {
            return;
        }
        
        WXComponent *component = [self.uniInstance.componentManager componentForRef:viewRef];
        if(component.view == nil) {
            self.componentStringMap[component] = @[userId, @(screenSharing)];
        } else {
            if(!component.view.frame.size.width) {
                NSLog(@"视频窗口(%@)大小为0，视频将无法显示。这个问题一般是setupRemoteVideoView发生在布局之前导致的，请在布局之后调用这个方法。", viewRef);
            }
            component.view.clipsToBounds = YES;
            component.view.layer.masksToBounds = YES;
            [session setupRemoteVideoView:component.view scalingType:kWFAVVideoScalingTypeAspectFit forUser:userId screenSharing:screenSharing];
        }
    });
}
//@UniJSMethod(uiThread = true)
//public void setParticipantVideoType(String userId, boolean screenSharing, int videoType) {
//    if (videoType < 0 || videoType > 2) {
//        Log.e(TAG, "setParticipantVideoType videoType error: " + videoType);
//        return;
//    }
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session != null) {
//        session.setParticipantVideoType(userId, screenSharing, AVEngineKit.VideoType.values()[videoType]);
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(setParticipantVideoType:userId:screenSharing:videoType:))
- (void)setParticipantVideoType:(NSString *)callId userId:(NSString *)userId screenSharing:(BOOL)screenSharing videoType:(int)videoType {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
        if(session.state == kWFAVEngineStateIdle) {
            return;
        }
        
        if (videoType < 0 || videoType > 2) {
            return;
        }
        
        [session setParticipant:userId screenSharing:screenSharing videoType:(WFAVVideoType)videoType];
    });
}
//@UniJSMethod(uiThread = false)
//public String getParticipantProfiles() {
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session != null) {
//        List<AVEngineKit.ParticipantProfile> profiles = session.getParticipantProfiles();
//        return JSONObject.toJSONString(profiles, ClientUniAppHookProxy.serializeConfig);
//    }
//    return null;
//}
UNI_EXPORT_METHOD_SYNC(@selector(getParticipantProfiles:))
- (NSString *)getParticipantProfiles:(NSString *)callId {
    WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
    if(session.state == kWFAVEngineStateIdle) {
        return nil;
    }
    
    NSArray<WFAVParticipantProfile *> *profiles = session.participants;
    NSMutableArray<NSDictionary *> *dicts = [[NSMutableArray alloc] init];
    [profiles enumerateObjectsUsingBlock:^(WFAVParticipantProfile * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [dicts addObject:[self profile2Dict:obj]];
    }];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dicts options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}
//@UniJSMethod(uiThread = false)
//public String getParticipantProfile(String userId, boolean screenSharing) {
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session != null) {
//        AVEngineKit.ParticipantProfile profile = session.getParticipantProfile(userId, screenSharing);
//        return JSONObject.toJSONString(profile, ClientUniAppHookProxy.serializeConfig);
//    }
//    return null;
//}
UNI_EXPORT_METHOD_SYNC(@selector(getParticipantProfile:userId:screenSharing:))
- (NSString *)getParticipantProfile:(NSString *)callId userId:(NSString *)userId screenSharing:(BOOL)screenSharing {
    WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
    if(session.state == kWFAVEngineStateIdle) {
        return nil;
    }
    
    WFAVParticipantProfile *profile = [session profileOfUser:userId isScreenSharing:screenSharing];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[self profile2Dict:profile] options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}

//@UniJSMethod(uiThread = false)
//public String getMyProfile() {
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session != null) {
//        AVEngineKit.ParticipantProfile profile = session.getMyProfile();
//        return JSONObject.toJSONString(profile, ClientUniAppHookProxy.serializeConfig);
//    }
//    return null;
//}
UNI_EXPORT_METHOD_SYNC(@selector(getMyProfile:))
- (NSString *)getMyProfile:(NSString *)callId {
    WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
    if(session.state == kWFAVEngineStateIdle) {
        return nil;
    }
    
    WFAVParticipantProfile *profile = session.myProfile;
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[self profile2Dict:profile] options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}

//@UniJSMethod(uiThread = false)
//public void muteAudio(boolean mute) {
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session != null) {
//        session.muteAudio(mute);
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(muteAudio:mute:))
- (void)muteAudio:(NSString *)callId mute:(BOOL)mute {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
        if(session.state == kWFAVEngineStateIdle) {
            return;
        }
        
        [session muteAudio:mute];
    });
}

//@UniJSMethod(uiThread = false)
//public void muteVideo(boolean mute) {
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session != null) {
//        session.muteVideo(mute);
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(muteVideo:mute:))
- (void)muteVideo:(NSString *)callId mute:(BOOL)mute {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
        if(session.state == kWFAVEngineStateIdle) {
            return;
        }
        
        [session muteVideo:mute];
    });
}

//@UniJSMethod(uiThread = true)
//public void setSpeakerOn(boolean speakerOn) {
//    AVAudioManager audioManager = AVEngineKit.Instance().getAVAudioManager();
//    AVAudioManager.AudioDevice currentAudioDevice = audioManager.getSelectedAudioDevice();
//    if (currentAudioDevice == AVAudioManager.AudioDevice.WIRED_HEADSET || currentAudioDevice == AVAudioManager.AudioDevice.BLUETOOTH) {
//        Log.d(TAG, "setSpeakerOn failed, current audio device: " + currentAudioDevice.name());
//        return;
//    }
//    audioManager.selectAudioDevice(speakerOn ? AVAudioManager.AudioDevice.SPEAKER_PHONE : AVAudioManager.AudioDevice.EARPIECE);
//}
UNI_EXPORT_METHOD_SYNC(@selector(setSpeakerOn:on:))
- (void)setSpeakerOn:(NSString *)callId on:(BOOL)speakerOn {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
        if(session.state == kWFAVEngineStateIdle) {
            return;
        }
        
        [session enableSpeaker:speakerOn];
    });
}

//@UniJSMethod(uiThread = true)
//public void downgrade2Voice() {
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session != null) {
//        session.setAudioOnly(true);
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(downgrade2Voice:))
- (void)downgrade2Voice:(NSString *)callId {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
        if(session.state == kWFAVEngineStateIdle) {
            return;
        }
        [session setAudioOnly:YES];
    });
}

//@UniJSMethod(uiThread = false)
//public String startMultiCall(String groupId, List<String> participants, boolean audioOnly) {
//    AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = true;
//    Conversation conversation = new Conversation(Conversation.ConversationType.Group, groupId);
//    AVEngineKit.CallSession session = AVEngineKit.Instance().startCall(conversation, participants, audioOnly, CallSessionCallbackWrapper.INSTANCE);
//    if (session != null) {
//        AudioManager audioManager = (AudioManager) uniSDKInstance.getContext().getSystemService(Context.AUDIO_SERVICE);
//        audioManager.setMode(AudioManager.MODE_NORMAL);
//        audioManager.setSpeakerphoneOn(true);
//        return JSONObject.toJSONString(JSCallSession.fromCallSession(session), ClientUniAppHookProxy.serializeConfig);
//    }
//    return null;
//}
UNI_EXPORT_METHOD_SYNC(@selector(startMultiCall:participants:audioOnly:))
- (NSString *)startMultiCall:(NSString *)groupId participants:(NSArray<NSString *> *)participants audioOnly:(BOOL)audioOnly {
    WFCCConversation *conversation = [WFCCConversation conversationWithType:Group_Type target:groupId line:0];
    WFAVCallSessionDelegater *delegater = [[WFAVCallSessionDelegater alloc] initWithCallId:nil uniInstance:self.uniInstance];
    __block WFAVCallSession *session;
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        session = [[WFAVEngineKit sharedEngineKit] startCall:participants audioOnly:audioOnly callExtra:nil conversation:conversation sessionDelegate:delegater];
    });
    
    if(session) {
        delegater.callId = session.callId;
        [self.delegaterMap setObject:delegater forKey:session.callId];
        return [self callSession2String:session];
    }
    return nil;
}

//@UniJSMethod(uiThread = false)
//public String startConference(String callId, boolean audioOnly, String pin, String host, String title, String desc, boolean audience, boolean advance, boolean record, String callExtra) {
//    AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = true;
//    AVEngineKit.CallSession session = AVEngineKit.Instance().startConference(callId, audioOnly, pin, host, title, desc, audience, advance, record, CallSessionCallbackWrapper.INSTANCE);
//    if (session != null) {
//        return JSONObject.toJSONString(JSCallSession.fromCallSession(session), ClientUniAppHookProxy.serializeConfig);
//    } else {
//        return null;
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(startConference:audioOnly:pin:host:title:desc:audience:advanced:record:callExtra:))
- (NSString *)startConference:(NSString *)callId audioOnly:(BOOL)audioOnly pin:(NSString *)pin host:(NSString *)host title:(NSString * _Nullable)title desc:(NSString * _Nullable)desc audience:(BOOL)audience advanced:(BOOL)advanced record:(BOOL)record callExtra:(NSString * _Nullable)callExtra {
    WFAVCallSessionDelegater *delegater = [[WFAVCallSessionDelegater alloc] initWithCallId:nil uniInstance:self.uniInstance];
    __block WFAVCallSession *session;
    dispatch_sync(dispatch_get_main_queue(), ^{
        session = [[WFAVEngineKit sharedEngineKit] startConference:callId audioOnly:audioOnly pin:pin host:host title:title desc:desc callExtra:callExtra audience:audience advanced:advanced record:record maxParticipants:20 sessionDelegate:delegater];
    });
    
    if(session) {
        delegater.callId = session.callId;
        [self.delegaterMap setObject:delegater forKey:session.callId];
        return [self callSession2String:session];
    }
    return nil;
}

//@UniJSMethod(uiThread = false)
//public String joinConference(String callId, boolean audioOnly, String pin, String host, String title, String desc, boolean audience, boolean advance, boolean muteAudio, boolean muteVideo, String extra) {
//    AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = true;
//    AVEngineKit.CallSession session = AVEngineKit.Instance().joinConference(callId, audioOnly, pin, host, title, desc, audience, advance, muteAudio, muteVideo, CallSessionCallbackWrapper.INSTANCE);
//    if (session != null) {
//        return JSONObject.toJSONString(JSCallSession.fromCallSession(session), ClientUniAppHookProxy.serializeConfig);
//    }
//    return null;
//}
UNI_EXPORT_METHOD_SYNC(@selector(joinConference:audioOnly:pin:host:title:desc:audience:advanced:muteAudio:muteVideo:callExtra:))
- (NSString *)joinConference:(NSString *)callId audioOnly:(BOOL)audioOnly pin:(NSString *)pin host:(NSString *)host title:(NSString * _Nullable)title desc:(NSString * _Nullable)desc audience:(BOOL)audience advanced:(BOOL)advanced muteAudio:(BOOL)muteAudio muteVideo:(BOOL)muteVideo callExtra:(NSString * _Nullable)callExtra {
    WFAVCallSessionDelegater *delegater = [[WFAVCallSessionDelegater alloc] initWithCallId:nil uniInstance:self.uniInstance];
    __block WFAVCallSession *session;
    dispatch_sync(dispatch_get_main_queue(), ^{
        session = [[WFAVEngineKit sharedEngineKit] joinConference:callId audioOnly:audioOnly pin:pin host:host title:title desc:desc callExtra:callExtra audience:audience advanced:advanced muteAudio:muteAudio muteVideo:muteVideo sessionDelegate:delegater];
    });
    
    if(session) {
        delegater.callId = session.callId;
        [self.delegaterMap setObject:delegater forKey:session.callId];
        return [self callSession2String:session];
    }
    return nil;
}

//@UniJSMethod(uiThread = false)
//public boolean isSupportMultiCall() {
//    return AVEngineKit.isSupportMultiCall();
//}
UNI_EXPORT_METHOD_SYNC(@selector(isSupportMultiCall))
- (BOOL)isSupportMultiCall {
    return YES;
}
//@UniJSMethod(uiThread = false)
//public boolean isSupportConference() {
//    return AVEngineKit.isSupportConference();
//}
UNI_EXPORT_METHOD_SYNC(@selector(isSupportConference))
- (BOOL)isSupportConference {
    return [WFAVEngineKit sharedEngineKit].supportConference;
}

//@UniJSMethod(uiThread = false)
//public void setVideoProfile(int profile, boolean swapWidthHeight) {
//    AVEngineKit.Instance().setVideoProfile(profile, swapWidthHeight);
//}
UNI_EXPORT_METHOD_SYNC(@selector(setVideoProfile:swapWidthHeight:))
- (void)setVideoProfile:(int)profile swapWidthHeight:(BOOL)swapWidthHeight {
    [[WFAVEngineKit sharedEngineKit] setVideoProfile:profile swapWidthHeight:swapWidthHeight];
}

//
//
//@UniJSMethod(uiThread = false)
//public void answerCall(String callId, boolean audioOnly) {
//    AVEngineKit.CallSession callSession = AVEngineKit.Instance().getCurrentSession();
//    if (callSession != null
//        && TextUtils.equals(callId, callSession.getCallId())
//        && callSession.getState() == AVEngineKit.CallState.Incoming) {
//        callSession.setCallback(CallSessionCallbackWrapper.INSTANCE);
//        Conversation conversation = callSession.getConversation();
//        if (conversation.type == Conversation.ConversationType.Single) {
//            AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = false;
//        } else {
//            AVEngineKit.DISABLE_SURFACE_VIEW_AUTO_OVERLAY = true;
//        }
//        callSession.answerCall(audioOnly);
//    } else {
//        // TODO
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(answerCall:audioOnly:))
- (void)answerCall:(NSString *)callId audioOnly:(BOOL)audioOnly {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
        if(session.state == kWFAVEngineStateIdle) {
            return;
        }
        if([session.callId isEqualToString:callId]) {
            [session answerCall:audioOnly callExtra:nil];
        }
    });
}

//@UniJSMethod(uiThread = true)
//public void endCall(String callId) {
//    Log.e(TAG, "endCall " + callId);
//    AVEngineKit.CallSession callSession = AVEngineKit.Instance().getCurrentSession();
//    if (callSession != null
//        && (callSession.getState() != AVEngineKit.CallState.Idle
//        && callSession.getCallId().equals(callId))) {
//        callSession.endCall();
//        callSession.setCallback(null);
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(endCall:))
- (void)endCall:(NSString *)callId {
    WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
    if(session.state == kWFAVEngineStateIdle) {
        return;
    }
    if([session.callId isEqualToString:callId]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [session endCall];
        });
    }
}

//@UniJSMethod(uiThread = false)
//public String currentCallSession() {
//    AVEngineKit.CallSession callSession = AVEngineKit.Instance().getCurrentSession();
//    if (callSession == null) {
//        return null;
//    }
//    return JSONObject.toJSONString(JSCallSession.fromCallSession(callSession), ClientUniAppHookProxy.serializeConfig);
//}
UNI_EXPORT_METHOD_SYNC(@selector(currentCallSession))
- (NSString *)currentCallSession {
    WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
    if(!session || session.state == kWFAVEngineStateIdle) {
        return nil;
    }
    return [self callSession2String:session];
}

//@UniJSMethod(uiThread = false)
//public boolean canSwitchAudience() {
//    if (AVEngineKit.isSupportConference()) {
//        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//        return session != null && session.canSwitchAudience();
//    }
//    return false;
//}
UNI_EXPORT_METHOD_SYNC(@selector(canSwitchAudience:))
- (BOOL)canSwitchAudience:(NSString *)callId {
    WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
    if(!session || session.state == kWFAVEngineStateIdle) {
        return NO;
    }
    if([self isSupportConference]) {
        return [session canSwitchAudience];
    }
    return NO;
}

//@UniJSMethod(uiThread = false)
//public boolean switchAudience(boolean audience) {
//    if (AVEngineKit.isSupportConference()) {
//        AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//        return session != null && session.switchAudience(audience);
//    }
//    return false;
//}
UNI_EXPORT_METHOD_SYNC(@selector(switchAudience:audience:))
- (BOOL)switchAudience:(NSString *)callId audience:(BOOL)audience {
    WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
    if(!session || session.state == kWFAVEngineStateIdle) {
        return NO;
    }
    if([self isSupportConference]) {
        __block BOOL value = false;
        dispatch_sync(dispatch_get_main_queue(), ^{
            value = [session switchAudience:audience];
        });
        return value;
    }
    return NO;
}

//@UniJSMethod(uiThread = true)
//public void switchCamera() {
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session != null) {
//        session.switchCamera();
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(switchCamera:))
- (void)switchCamera:(NSString *)callId {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
        if(!session || session.state == kWFAVEngineStateIdle) {
            return;
        }
        [session switchCamera];
    });
}

//@UniJSMethod(uiThread = true)
//public void inviteNewParticipant(List<String> userIds) {
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session != null) {
//        session.inviteNewParticipants(userIds);
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(inviteNewParticipant:userIds:))
- (void)inviteNewParticipant:(NSString *)callId userIds:(NSArray<NSString *> *)participants {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
        if(!session || session.state == kWFAVEngineStateIdle) {
            return;
        }
        [session inviteNewParticipants:participants];
    });
}

//@UniJSMethod(uiThread = false)
//public boolean checkOverlayPermission() {
//    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//        Context context = uniSDKInstance.getContext();
//        if (!Settings.canDrawOverlays(uniSDKInstance.getContext())) {
//            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + context.getPackageName()));
//
//            List<ResolveInfo> infos = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
//            if (!infos.isEmpty()) {
//                context.startActivity(intent);
//            }
//            return false;
//        }
//    }
//    return true;
//}
UNI_EXPORT_METHOD_SYNC(@selector(checkOverlayPermission))
- (BOOL)checkOverlayPermission {
    return YES;
}

//@UniJSMethod(uiThread = true)
//public void minimize() {
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session != null) {
//        VoipFloatingWindowManager.getInstance().showFloatingWindow(session);
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(minimize:focus:))
- (void)minimize:(NSString *)callId focus:(NSString *)userId {
    __weak typeof(self)ws = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if([WFAVFloatingWindow isShowing:[WFAVEngineKit sharedEngineKit].currentSession]) {
            return;
        }
        if([WFAVEngineKit sharedEngineKit].currentSession.state != kWFAVEngineStateIdle) {
            NSString *focusUser = userId;
            
            if(!focusUser || (![[WFAVEngineKit sharedEngineKit].currentSession.participantIds containsObject:focusUser] && ![focusUser isEqualToString:[WFCCNetworkService sharedInstance].userId])) {
                if([[WFAVEngineKit sharedEngineKit].currentSession.participantIds count]) {
                    focusUser = [WFAVEngineKit sharedEngineKit].currentSession.participants[0].userId;
                } else {
                    focusUser = [WFCCNetworkService sharedInstance].userId;
                }
            }
            
            [self.delegaterMap removeObjectForKey:[WFAVEngineKit sharedEngineKit].currentSession.callId];
            [WFAVFloatingWindow startCallFloatingWindow:[WFAVEngineKit sharedEngineKit].currentSession focusUser:focusUser withTouchedBlock:^(WFAVCallSession *callSession) {
                [ws resume];
            }];
        }
    });
}

//@Override
//public void onClickFloatingWindow() {
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session != null) {
//        String type = "conference";
//        Conversation conversation = session.getConversation();
//        if (conversation != null) {
//            type = conversation.type == Conversation.ConversationType.Single ? "single" : "multi";
//        }
//        fireCallSessionEvent2js("resumeVoipPage", type);
//    }
//}
- (void)resume {
    WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
    if(session) {
        NSString *type = @"conference";
        if(![session isConference]) {
            type = session.conversation.type == Single_Type ? @"single" : @"multi";
        }
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        dict[@"args"] = @[@"resumeVoipPage", type];
        dict[@"timestamp"] = @([[[NSDate alloc] init] timeIntervalSince1970]);
        dict[@"callId"] = session.callId;
        [self.uniInstance fireGlobalEvent:@"wfc-av-session-event" params:dict];
        
        WFAVCallSessionDelegater *delegater = [[WFAVCallSessionDelegater alloc] initWithCallId:session.callId uniInstance:self.uniInstance];
        session.delegate = delegater;
        [self.delegaterMap setValue:delegater forKey:session.callId];
    }
}

//@Override
//public void onCreate(WXComponent component) {
//
//}
//
//@Override
//public void onPreDestory(WXComponent component) {
//
//}
//
//@Override
//public void onViewCreated(WXComponent component, View view) {
//    AVEngineKit.CallSession session = AVEngineKit.Instance().getCurrentSession();
//    if (session == null) {
//        return;
//    }
//    for (Map.Entry<WXComponent, Pair<String, Boolean>> entry : componentStringMap.entrySet()) {
//        WXComponent wxComponent = entry.getKey();
//        if (component == wxComponent) {
//            Pair<String, Boolean> pair = entry.getValue();
//            if (pair.first.equals(ChatManager.Instance().getUserId())) {
//                session.setupLocalVideoView((FrameLayout) wxComponent.getHostView(), RendererCommon.ScalingType.SCALE_ASPECT_BALANCED);
//            } else {
//                FrameLayout frameLayout = (FrameLayout) wxComponent.getHostView();
//                this.resetRemoteVideoViewLayoutParams(frameLayout);
//                session.setupRemoteVideoView(pair.first, pair.second, frameLayout, RendererCommon.ScalingType.SCALE_ASPECT_BALANCED);
//            }
//        }
//    }
//    componentStringMap.remove(component);
//}
- (void)onViewCreated:(WFAVRtcView *)rtcView {
    WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
    if(session.state == kWFAVEngineStateIdle) {
        return;
    }
    
    NSArray *arr = self.componentStringMap[rtcView];
    if(arr) {
        NSString *userId = arr[0];
        BOOL screenSharing = [arr[1] boolValue];
        UIView *view = rtcView.view;
        NSString *viewRef = rtcView.ref;
        dispatch_async(dispatch_get_main_queue(), ^{
            view.clipsToBounds = YES;
            view.layer.masksToBounds = YES;
            if([userId isEqualToString:[WFCCNetworkService sharedInstance].userId]) {
                if(!view.frame.size.width) {
                    NSLog(@"视频窗口(%@)大小为0，视频将无法显示。这个问题一般是setLocalVideoView发生在布局之前导致的，请在布局之后调用这个方法。", viewRef);
                }
                [session setupLocalVideoView:view scalingType:kWFAVVideoScalingTypeAspectBalanced];
            } else {
                if(!view.frame.size.width) {
                    NSLog(@"视频窗口(%@)大小为0，视频将无法显示。这个问题一般是setupRemoteVideoView发生在布局之前导致的，请在布局之后调用这个方法。", viewRef);
                }
                [session setupRemoteVideoView:view scalingType:kWFAVVideoScalingTypeAspectBalanced forUser:userId screenSharing:screenSharing];
            }
        });
        [self.componentStringMap removeObjectForKey:rtcView];
    }
}

- (NSString *)callSession2String:(WFAVCallSession *)session {
    if(!session || session.state == kWFAVEngineStateIdle) {
        return nil;
    }
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    dict[@"callId"] = session.callId;
    if(session.initiator.length) {
        dict[@"initiator"] = session.initiator;
    }
    if(session.inviter.length) {
        dict[@"inviter"] = session.inviter;
    }
    dict[@"state"] = @(session.state);
    dict[@"startTime"] = @(session.startTime);
    dict[@"connectedTime"] = @(session.connectedTime);
    dict[@"endTime"] = @(session.endTime);
    if(session.conversation) {
        dict[@"conversation"] = [session.conversation toJsonObj];
    }
    dict[@"audioOnly"] = @(session.audioOnly);
    dict[@"endReason"] = @(session.endReason);
    dict[@"conference"] = @(session.conference);
    dict[@"audience"] = @(session.audience);
    dict[@"audioMuted"] = @(session.audioMuted);
    dict[@"videoMuted"] = @(session.videoMuted);
    dict[@"advanced"] = @(session.advanced);
    dict[@"multiCall"] = @(session.multiCall);
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}

- (NSDictionary *)profile2Dict:(WFAVParticipantProfile *)profile {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    dict[@"userId"] = profile.userId;
    dict[@"startTime"] = @(profile.startTime);
    dict[@"state"] = @(profile.state);
    dict[@"videoMuted"] = @(profile.videoMuted);
    dict[@"audioMuted"] = @(profile.audioMuted);
    dict[@"audience"] = @(profile.audience);
    dict[@"screenSharing"] = @(profile.screeSharing);
    dict[@"callExtra"] = profile.callExtra;
    dict[@"videoType"] = @(profile.videoType);
    
    return dict;
}

- (void)onIndication:(NSArray *)args {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    dict[@"args"] = args;
    dict[@"timestamp"] = @([[[NSDate alloc] init] timeIntervalSince1970]);
    [self.uniInstance fireGlobalEvent:@"wfc-av-event" params:dict];
}

#pragma mark - WFAVEngineDelegate
- (void)didReceiveCall:(WFAVCallSession *)session {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([WFAVEngineKit sharedEngineKit].currentSession.state != kWFAVEngineStateIncomming && [WFAVEngineKit sharedEngineKit].currentSession.state != kWFAVEngineStateConnected && [WFAVEngineKit sharedEngineKit].currentSession.state != kWFAVEngineStateConnecting) {
            return;
        }
        
        WFAVCallSessionDelegater *delegater = [[WFAVCallSessionDelegater alloc] initWithCallId:session.callId uniInstance:self.uniInstance];
        session.delegate = delegater;
        [self.delegaterMap setValue:delegater forKey:session.callId];
        
        [self onIndication:@[@"onReceiveCall", [self callSession2String:session]]];
    });
}

- (void)shouldStartRing:(BOOL)isIncoming {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([WFAVEngineKit sharedEngineKit].currentSession.state == kWFAVEngineStateIncomming || [WFAVEngineKit sharedEngineKit].currentSession.state == kWFAVEngineStateOutgoing) {
            [self onIndication:@[@"shouldStartRing", @(isIncoming)]];
        }
    });
}

- (void)shouldStopRing {
    [self onIndication:@[@"shouldStopRing"]];
}

- (void)didCallEnded:(WFAVCallEndReason) reason duration:(int)callDuration {
    [self onIndication:@[@"didCallEnded", @(reason), @(callDuration)]];
    [self.componentStringMap removeAllObjects];
}

- (void)didReceiveIncomingPushWithPayload:(PKPushPayload * _Nonnull)payload forType:(NSString * _Nonnull)type {
    
}
@end
