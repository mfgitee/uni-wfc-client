//
//  WFAVRtcView.m
//  WFAVUniPlugin
//
//  Created by Rain on 2023/11/17.
//

#import "WFAVRtcView.h"
#import "WFAVModule.h"


extern WFAVModule *gWFAVModule;
@interface WFAVRtcView ()
@property(nonatomic, strong)UIView *nativeView;
@end

@implementation WFAVRtcView
-(void)onCreateComponentWithRef:(NSString *)ref type:(NSString *)type
                    styles:(NSDictionary *)styles
                    attributes:(NSDictionary *)attributes
                         events:(NSArray *)events uniInstance:(DCUniSDKInstance *)uniInstance {
    [super onCreateComponentWithRef:ref type:type styles:styles attributes:attributes events:events uniInstance:uniInstance];
    
}

- (UIView *)loadView {
    if(!self.nativeView) {
        if([NSThread isMainThread]) {
            self.nativeView = [[UIView alloc] init];
            self.nativeView.clipsToBounds = YES;
        } else {
            dispatch_sync(dispatch_get_main_queue(), ^{
                self.nativeView = [[UIView alloc] init];
                self.nativeView.clipsToBounds = YES;
            });
        }
    }
    return self.nativeView;
}

-(void)viewWillLoad {
    [gWFAVModule onViewCreated:self];
}
@end
