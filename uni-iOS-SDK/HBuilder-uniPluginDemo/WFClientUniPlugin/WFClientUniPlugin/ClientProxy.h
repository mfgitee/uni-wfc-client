//
//  ClientProxy.h
//  WFClientUniPlugin
//
//  Created by Rain on 2022/5/30.
//  Copyright © 2022 DCloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UniPluginProtocol.h"


NS_ASSUME_NONNULL_BEGIN

@interface WFClientProxy : NSObject <UniPluginProtocol>

@end

NS_ASSUME_NONNULL_END
