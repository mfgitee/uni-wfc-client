//
//  NullGroupInfo.m
//  WFClientUniPlugin
//
//  Created by Rain on 2022/6/3.
//  Copyright © 2022 DCloud. All rights reserved.
//

#import "NullGroupInfo.h"

@implementation NullGroupInfo
+ (instancetype)nullGroupOfId:(NSString *)groupId {
    NullGroupInfo *groupInfo = [[NullGroupInfo alloc] init];
    groupInfo.groupId = groupId;
    return groupInfo;
}
- (id)toJsonObj {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    dict[@"target"] = self.groupId;
    dict[@"name"] = @"群聊";
    return dict;
}
@end
