//
//  ClientModule.m
//  WFClientUniPlugin
//
//  Created by Rain on 2022/5/30.
//  Copyright © 2022 DCloud. All rights reserved.
//

#import "ClientModule.h"
#import <AVFoundation/AVFoundation.h>
#import <WFChatClient/WFCChatClient.h>
#import "NullUserInfo.h"
#import "NullGroupInfo.h"
#import <Bugly/Bugly.h>

@interface WFClientModule () <ConnectionStatusDelegate, ConferenceEventDelegate, ConnectToServerDelegate, ReceiveMessageDelegate, OnlineEventDelegate, TrafficDataDelegate, UIDocumentPickerDelegate>
@property(nonatomic, strong)NSString *userId;
@property(nonatomic, strong)NSString *token;

@property(nonatomic, strong)UniModuleKeepAliveCallback selectFileSuccessCB;
@property(nonatomic, strong)UniModuleKeepAliveCallback selectFileFailureCB;
@end

@implementation WFClientModule

//private static final String TAG = "WfcClientModule";
//static AbsSDKInstance uniSDKInstance;
//
//private String userId;
//private String token;
//
//// 不知道为啥，未触发
//@Override
//public void onActivityCreate() {
//    super.onActivityCreate();
//}
//
//@Override
//public void onActivityDestroy() {
//    super.onActivityDestroy();
//}
//
//// 一定要调这个函数，触发对mUniSDKInstance 的赋值
//@UniJSMethod(uiThread = false)
//public void init() {
//    ClientModule.uniSDKInstance = mUniSDKInstance;
//}
UNI_EXPORT_METHOD_SYNC(@selector(initProto))
- (void)initProto {
    NSLog(@"call client init");
    [WFCCNetworkService sharedInstance].sendLogCommand = @"*#marslog#";
}

UNI_EXPORT_METHOD_SYNC(@selector(startBugly:))
- (void)startBugly:(NSString *)appId {
    NSLog(@"startBugly:%@", appId);
    [Bugly startWithAppId:appId];
}

//@UniJSMethod(uiThread = false)
//public void connect(String imServerHost, String userId, String token) {
//    this.userId = userId;
//    this.token = token;
//    if (mUniSDKInstance.getContext() instanceof Activity) {
//        ChatManager.Instance().setIMServerHost(imServerHost);
//        ChatManager.Instance().connect(userId, token);
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(connect:userId:token:))
- (int64_t)connect:(NSString *)host userId:(NSString *)userId token:(NSString *)token {
    [[WFCCNetworkService sharedInstance] setServerAddress:host];
    [WFCCNetworkService sharedInstance].connectionStatusDelegate = self;
    [WFCCNetworkService sharedInstance].connectToServerDelegate = self;
    [WFCCNetworkService sharedInstance].receiveMessageDelegate = self;
    [WFCCNetworkService sharedInstance].onlineEventDelegate = self;
    [WFCCNetworkService sharedInstance].trafficDataDelegate = self;


    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGroupInfoUpdated:) name:kGroupInfoUpdated object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGroupMemberUpdated:) name:kGroupMemberUpdated object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUserInfoUpdated:) name:kUserInfoUpdated object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFriendListUpdated:) name:kFriendListUpdated object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFriendRequestUpdated:) name:kFriendRequestUpdated object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSettingUpdated:) name:kSettingUpdated object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onChannelInfoUpdated:) name:kChannelInfoUpdated object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUserOnlineStateUpdated:) name:kUserOnlineStateUpdated object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMediaUploadProgress:) name:kUploadMediaMessageProgresse object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMessageStatusUpdated:) name:kSendingMessageStatusUpdated object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMessageUpdated:) name:kMessageUpdated object:nil];
    
    return [[WFCCNetworkService sharedInstance] connect:userId token:token];
}
//
//// ui 线程的方法异步执行
//// 非 ui 线程的方法同步执行
//@UniJSMethod(uiThread = false)
//public String getClientId() {
//    Log.d(TAG, "getClientId " + ChatManager.Instance().getClientId());
//    return ChatManager.Instance().getClientId();
//}
UNI_EXPORT_METHOD_SYNC(@selector(getClientId))
- (NSString *)getClientId {
    return [[WFCCNetworkService sharedInstance] getClientId];
}

UNI_EXPORT_METHOD_SYNC(@selector(getUserId))
- (NSString *)getUserId {
    return [WFCCNetworkService sharedInstance].userId;
}

UNI_EXPORT_METHOD_SYNC(@selector(setDeviceToken:token:))
- (void)setDeviceToken:(int)type token:(NSString *)token {
    [[WFCCNetworkService sharedInstance] setDeviceToken:token pushType:type];
}

UNI_EXPORT_METHOD(@selector(chooseFile:success:error:))
- (void)chooseFile:(NSString *)type success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    NSArray*documentTypes =@[
            @"public.content",
            @"public.data",
            @"com.microsoft.powerpoint.ppt",
            @"com.microsoft.word.doc",
            @"com.microsoft.excel.xls",
            @"com.microsoft.powerpoint.pptx",
            @"com.microsoft.word.docx",
            @"com.microsoft.excel.xlsx",
            @"public.avi",
            @"public.3gpp",
            @"public.mpeg-4",
            @"com.compuserve.gif",
            @"public.jpeg",
            @"public.png",
            @"public.plain-text",
            @"com.adobe.pdf"
            ];

    UIDocumentPickerViewController *picker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:documentTypes inMode:UIDocumentPickerModeOpen];
    picker.delegate = self;
    
    if (@available(iOS 11.0, *)) {
        picker.allowsMultipleSelection = YES;
    }
    self.selectFileSuccessCB = successCB;
    self.selectFileFailureCB = errorCB;
    picker.modalPresentationStyle = UIModalPresentationFullScreen;
    [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:picker animated:YES completion:nil];
}


//@UniJSMethod(uiThread = false)
//public void registerMessageFlag(int type, int flag) {
//    PersistFlag persistFlag = PersistFlag.flag(flag);
//    ChatManager.Instance().registerMessageFlag(type, persistFlag);
//}
UNI_EXPORT_METHOD_SYNC(@selector(registerMessageFlag:flag:))
- (void)registerMessageFlag:(int)type flag:(int)flag {
    [[WFCCIMService sharedWFCIMService] registerMessageFlag:type flag:flag];
}
//@UniJSMethod(uiThread = false)
//public void useSM4() {
//    ChatManager.Instance().useSM4();
//}
UNI_EXPORT_METHOD_SYNC(@selector(useSM4))
- (void)useSM4 {
    [[WFCCNetworkService sharedInstance] useSM4];
}
//@UniJSMethod(uiThread = false)
//public void setProxyInfo(String host, String ip, int port, String userName, String password) {
//    Socks5ProxyInfo proxyInfo = new Socks5ProxyInfo(host, ip, port, userName, password);
//    ChatManager.Instance().setProxyInfo(proxyInfo);
//}
UNI_EXPORT_METHOD_SYNC(@selector(setProxyInfo:ip:port:username:password:))
- (void)setProxyInfo:(NSString *)host ip:(NSString *)ip port:(int)port username:(NSString *)username password:(NSString *)password {
    [[WFCCNetworkService sharedInstance] setProxyInfo:host ip:ip port:port username:username password:password];
}

//@UniJSMethod(uiThread = false)
//public void disconnect(int code) {
//    ChatManager.Instance().disconnect(false, false);
//}
UNI_EXPORT_METHOD_SYNC(@selector(disconnect:clearSession:))
- (void)disconnect:(BOOL)disablePush clearSession:(BOOL)clearSession {
    [[WFCCNetworkService sharedInstance] disconnect:disablePush clearSession:clearSession];
}

UNI_EXPORT_METHOD_SYNC(@selector(setSendLogCommand:))
- (void)setSendLogCommand:(NSString *)cmd {
    [WFCCNetworkService sharedInstance].sendLogCommand = cmd;
}

//@UniJSMethod(uiThread = false)
//public long getServerDeltaTime() {
//    return ChatManager.Instance().getServerDeltaTime();
//}
UNI_EXPORT_METHOD_SYNC(@selector(getServerDeltaTime))
- (long)getServerDeltaTime {
    return [WFCCNetworkService sharedInstance].serverDeltaTime;
}
//@UniJSMethod(uiThread = false)
//public int getConnectionStatus() {
//    return ChatManager.Instance().getConnectionStatus();
//}
UNI_EXPORT_METHOD_SYNC(@selector(getConnectionStatus))
- (int)getConnectionStatus {
    return (int)[WFCCNetworkService sharedInstance].currentConnectionStatus;
}

//@UniJSMethod(uiThread = false)
//public void setBackupAddressStrategy(int strategy) {
//    ChatManager.Instance().setBackupAddressStrategy(strategy);
//}
UNI_EXPORT_METHOD_SYNC(@selector(setBackupAddressStrategy:))
- (void)setBackupAddressStrategy:(int)strategy {
    [[WFCCNetworkService sharedInstance] setBackupAddressStrategy:strategy];
}
//@UniJSMethod(uiThread = false)
//public void setBackupAddress(String backupHost, int backPort) {
//    ChatManager.Instance().setBackupAddress(backupHost, backPort);
//}
UNI_EXPORT_METHOD_SYNC(@selector(setBackupAddress:backupPort:))
- (void)setBackupAddress:(NSString *)backupHost backupPort:(int)backupPort {
    [[WFCCNetworkService sharedInstance] setBackupAddress:backupHost port:backupPort];
}
//@UniJSMethod(uiThread = false)
//public void setUserAgent(String userAgent) {
//    ChatManager.Instance().setProtoUserAgent(userAgent);
//}
UNI_EXPORT_METHOD_SYNC(@selector(setUserAgent:))
- (void)setUserAgent:(NSString *)userAgent {
    [[WFCCNetworkService sharedInstance] setProtoUserAgent:userAgent];
}
//@UniJSMethod(uiThread = false)
//public void addHttpHeader(String header, String value) {
//    ChatManager.Instance().addHttpHeader(header, value);
//}
UNI_EXPORT_METHOD_SYNC(@selector(addHttpHeader:value:))
- (void)addHttpHeader:(NSString *)header value:(NSString *)value {
    [[WFCCNetworkService sharedInstance] addHttpHeader:header value:value];
}

UNI_EXPORT_METHOD_SYNC(@selector(notify:content:))
- (void)notify:(NSString *)title content:(NSString *)content {
    UILocalNotification *localNote = [[UILocalNotification alloc] init];
    localNote.alertTitle = title;
    localNote.alertBody = content;
    dispatch_async(dispatch_get_main_queue(), ^{
      [[UIApplication sharedApplication] scheduleLocalNotification:localNote];
    });
}

UNI_EXPORT_METHOD_SYNC(@selector(clearAllNotification))
- (void)clearAllNotification {
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

//@UniJSMethod(uiThread = true)
//public void getAuthCode(String appId, int appType, String host, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().getAuthCode(appId, appType, host, new JSGeneralCallback2(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(getAuthCode:appType:host:success:error:))
- (void)getAuthCode:(NSString *)appId appType:(int)appType host:(NSString *)host success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] getAuthCode:appId type:appType host:host success:^(NSString *authCode) {
        if(successCB) {
            successCB(authCode, NO);
        }
    } error:^(int error_code) {
        if(errorCB) {
            errorCB(@(error_code), NO);
        }
    }];
}


//@UniJSMethod(uiThread = true)
//public void configApplication(String appId, int appType, long timestamp, String nonceStr, String signature, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().configApplication(appId, appType, timestamp, nonceStr, signature, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(configApplication:appType:timestamp:nonceStr:signature:success:error:))
- (void)configApplication:(NSString *)appId appType:(int)appType timestamp:(int64_t)timestamp nonceStr:(NSString *)nonceStr signature:(NSString *)signature success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] configApplication:appId type:appType timestamp:timestamp nonce:nonceStr signature:signature success:^(void){
        if(successCB) {
            successCB(nil, NO);
        }
    } error:^(int error_code) {
        if(errorCB) {
            errorCB(@(error_code), NO);
        }
    }];
}

//@UniJSMethod(uiThread = false)
//public String getUserInfo(String userId, boolean refresh, String groupId) {
//    UserInfo userInfo = ChatManager.Instance().getUserInfo(userId, groupId, refresh);
//    return JSONObject.toJSONString(userInfo, ClientUniAppHookProxy.serializeConfig);
//}
UNI_EXPORT_METHOD_SYNC(@selector(getUserInfo:refresh:groupId:))
- (NSString *)getUserInfo:(NSString *)userId refresh:(BOOL)refresh groupId:(NSString *)groupId {
    WFCCUserInfo *userInfo = [[WFCCIMService sharedWFCIMService] getUserInfo:userId inGroup:groupId refresh:refresh];
    if(userInfo)
        return [userInfo toJsonStr];
    else
        return [[NullUserInfo nullUserOfId:userId] toJsonStr];;
}
//@UniJSMethod(uiThread = false)
//public String getUserInfos(List<String> userIds, String groupId) {
//    List<UserInfo> userInfos = ChatManager.Instance().getUserInfos(userIds, groupId);
//    return JSONObject.toJSONString(userInfos, ClientUniAppHookProxy.serializeConfig);
//}

UNI_EXPORT_METHOD_SYNC(@selector(getUserInfos:groupId:))
- (NSString *)getUserInfos:(NSArray<NSString *> *)userIds groupId:(NSString *)groupId {
    NSArray<WFCCUserInfo *> *userInfos = [[WFCCIMService sharedWFCIMService] getUserInfos:userIds inGroup:groupId];
    return [self convertModelListToString:userInfos];
}
//@UniJSMethod(uiThread = true)
//public void getUserInfoEx(String userId, boolean refresh, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().getUserInfo(userId, refresh, new GetUserInfoCallback() {
//        @Override
//        public void onSuccess(UserInfo userInfo) {
//            if (successCB != null) {
//                successCB.invoke(JSONObject.toJSONString(userInfo, ClientUniAppHookProxy.serializeConfig));
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            if (failCB != null) {
//                failCB.invoke(errorCode);
//            }
//        }
//    });
//}
UNI_EXPORT_METHOD(@selector(getUserInfoEx:refresh:success:error:))
- (void)getUserInfoEx:(NSString *)userId refresh:(BOOL)refresh success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] getUserInfo:userId refresh:refresh success:^(WFCCUserInfo *userInfo) {
        if(successCB) {
            successCB([userInfo toJsonStr], NO);
        }
    } error:^(int errorCode) {
        if(errorCB) {
            errorCB(@(errorCode), NO);
        }
    }];
}
//@UniJSMethod(uiThread = true)
//public void searchUser(String keyword, int searchType, int page, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().searchUser(keyword, ChatManager.SearchUserType.type(searchType), page, new SearchUserCallback() {
//        @Override
//        public void onSuccess(List<UserInfo> userInfos) {
//            if (successCB != null) {
//                successCB.invoke(JSONObject.toJSONString(userInfos, ClientUniAppHookProxy.serializeConfig));
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            if (failCB != null) {
//                failCB.invoke(errorCode);
//            }
//        }
//    });
//}
UNI_EXPORT_METHOD(@selector(searchUser:searchType:page:success:error:))
- (void)searchUser:(NSString *)keyword searchType:(int)searchType page:(int)page success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    __weak typeof(self)ws = self;
    [[WFCCIMService sharedWFCIMService] searchUser:keyword searchType:searchType page:page success:^(NSArray<WFCCUserInfo *> *machedUsers) {
        if(successCB) {
            successCB([ws convertModelListToString:machedUsers], NO);
        }
    } error:^(int errorCode) {
        if(errorCB) {
            errorCB(@(errorCode), NO);
        }
    }];
}
//@UniJSMethod(uiThread = false)
//public String searchFriends(String keyword) {
//    List<UserInfo> friends = ChatManager.Instance().searchFriends(keyword);
//    return JSONObject.toJSONString(friends, ClientUniAppHookProxy.serializeConfig);
//}

UNI_EXPORT_METHOD_SYNC(@selector(searchFriends:))
- (NSString *)searchFriends:(NSString *)keyword {
    NSArray<WFCCUserInfo *> *userInfos = [[WFCCIMService sharedWFCIMService] searchFriends:keyword];
    return [self convertModelListToString:userInfos];
}
//@UniJSMethod(uiThread = false)
//public String searchGroups(String keyword) {
//    List<GroupSearchResult> groupSearchResults = ChatManager.Instance().searchGroups(keyword);
//    return JSONObject.toJSONString(groupSearchResults, ClientUniAppHookProxy.serializeConfig);
//}

UNI_EXPORT_METHOD_SYNC(@selector(searchGroups:))
- (NSString *)searchGroups:(NSString *)keyword {
    NSArray<WFCCGroupSearchInfo *> *groupSearchInfos = [[WFCCIMService sharedWFCIMService] searchGroups:keyword];
    return [self convertModelListToString:groupSearchInfos];
}
//@UniJSMethod(uiThread = false)
//public String getIncommingFriendRequest() {
//    List<FriendRequest> friendRequests = ChatManager.Instance().getFriendRequest(true);
//    return JSONObject.toJSONString(friendRequests, ClientUniAppHookProxy.serializeConfig);
//}

UNI_EXPORT_METHOD_SYNC(@selector(getIncommingFriendRequest))
- (NSString *)getIncommingFriendRequest {
    NSArray<WFCCFriendRequest *> *infos = [[WFCCIMService sharedWFCIMService] getIncommingFriendRequest];
    return [self convertModelListToString:infos];
}
//@UniJSMethod(uiThread = false)
//public String getOutgoingFriendRequest() {
//    List<FriendRequest> friendRequests = ChatManager.Instance().getFriendRequest(false);
//    return JSONObject.toJSONString(friendRequests, ClientUniAppHookProxy.serializeConfig);
//}
UNI_EXPORT_METHOD_SYNC(@selector(getOutgoingFriendRequest))
- (NSString *)getOutgoingFriendRequest {
    NSArray<WFCCFriendRequest *> *infos = [[WFCCIMService sharedWFCIMService] getIncommingFriendRequest];
    return [self convertModelListToString:infos];
}
//@UniJSMethod(uiThread = false)
//public String getFriendRequest(String userId, boolean incoming) {
//    FriendRequest friendRequest = ChatManager.Instance().getFriendRequest(userId, incoming);
//    return JSONObject.toJSONString(friendRequest, ClientUniAppHookProxy.serializeConfig);
//}
UNI_EXPORT_METHOD_SYNC(@selector(getFriendRequest:direction:))
- (NSString *)getFriendRequest:(NSString *)userId incoming:(BOOL)incoming {
    return [[[WFCCIMService sharedWFCIMService] getFriendRequest:userId direction:incoming] toJsonStr];
}

//@UniJSMethod(uiThread = true)
//public void loadFriendRequestFromRemote() {
//    ChatManager.Instance().loadFriendRequestFromRemote();
//}

UNI_EXPORT_METHOD_SYNC(@selector(loadFriendRequestFromRemote))
- (void)loadFriendRequestFromRemote {
    [[WFCCIMService sharedWFCIMService] loadFriendRequestFromRemote];
}

//@UniJSMethod(uiThread = false)
//public String getFavUsers() {
//    Map<String, String> userIdMap = ChatManager.Instance().getUserSettings(UserSettingScope.FavoriteUser);
//    List<String> userIds = new ArrayList<>();
//    if (userIdMap != null && !userIdMap.isEmpty()) {
//        for (Map.Entry<String, String> entry : userIdMap.entrySet()) {
//            if (entry.getValue().equals("1")) {
//                userIds.add(entry.getKey());
//            }
//        }
//    }
//    return JSONObject.toJSONString(userIds, ClientUniAppHookProxy.serializeConfig);
//}
UNI_EXPORT_METHOD_SYNC(@selector(getFavUsers))
- (NSString *)getFavUsers {
    NSArray<NSString *> *favs = [[WFCCIMService sharedWFCIMService] getFavUsers];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:favs options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}

//@UniJSMethod(uiThread = false)
//public boolean isFavUser(String userId) {
//    return ChatManager.Instance().isFavUser(userId);
//}
UNI_EXPORT_METHOD_SYNC(@selector(isFavUser:))
- (BOOL)isFavUser:(NSString *)userId {
    return [[WFCCIMService sharedWFCIMService] isFavUser:userId];
}
//@UniJSMethod(uiThread = true)
//public void setFavUser(String userId, boolean fav, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().setFavUser(userId, fav, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(setFavUser:isFav:success:error:))
- (void)setFavUser:(NSString *)userId isFav:(BOOL)isFav success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] setFavUser:userId fav:isFav success:^{
        if(successCB) {
            successCB(nil, NO);
        }
    } error:^(int errorCode) {
        if(errorCB) {
            errorCB(@(errorCode), NO);
        }
    }];
}

//@UniJSMethod(uiThread = true)
//public void getRemoteMessages(String strConv, String beforeUid, int count, JSCallback successCB, JSCallback failCB) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().getRemoteMessages(conversation, null, Long.parseLong(beforeUid), count, new GetRemoteMessageCallback() {
//        @Override
//        public void onSuccess(List<Message> messages) {
//            if (successCB != null) {
//                successCB.invoke(JSONObject.toJSONString(Util.messagesToJSMessages(messages), ClientUniAppHookProxy.serializeConfig));
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            if (failCB != null) {
//                failCB.invoke(errorCode);
//            }
//
//        }
//    });
//
//}
UNI_EXPORT_METHOD(@selector(getRemoteMessages:beforeUid:count:success:error:))
- (void)getRemoteMessages:(NSString *)strConv beforeUid:(NSString *)beforeUid count:(int)count success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    __weak typeof(self)ws = self;
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    [[WFCCIMService sharedWFCIMService] getRemoteMessages:conversation before:atoll([beforeUid UTF8String]) count:count contentTypes:nil success:^(NSArray<WFCCMessage *> *messages) {
        if(successCB) {
            successCB([ws convertModelListToString:messages], NO);
        }
    } error:^(int error_code) {
        if (errorCB) {
            errorCB(@(error_code), NO);
        }
    }];
}
//@UniJSMethod(uiThread = true)
//public void getRemoteMessage(String messageUid, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().getRemoteMessage(Long.parseLong(messageUid), new GetOneRemoteMessageCallback() {
//        @Override
//        public void onSuccess(Message messages) {
//            if (successCB != null) {
//                successCB.invoke(JSONObject.toJSONString(JSMessage.fromMessage(messages), ClientUniAppHookProxy.serializeConfig));
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            if (failCB != null) {
//                failCB.invoke(errorCode);
//            }
//        }
//    });
//}
UNI_EXPORT_METHOD(@selector(getRemoteMessage:success:error:))
- (void)getRemoteMessage:(NSString *)strMessageUid count:(int)count success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] getRemoteMessage:atoll([strMessageUid UTF8String]) success:^(WFCCMessage *message) {
        if(successCB) {
            if(message) {
                successCB([message toJsonStr], NO);
            } else {
                successCB(nil, NO);
            }
        }
    } error:^(int error_code) {
        if (errorCB) {
            errorCB(@(error_code), NO);
        }
    }];
}
//@UniJSMethod(uiThread = false)
//public int getUnreadFriendRequestStatus() {
//    return ChatManager.Instance().getUnreadFriendRequestStatus();
//}
UNI_EXPORT_METHOD_SYNC(@selector(getUnreadFriendRequestStatus))
- (int)getUnreadFriendRequestStatus {
    return [[WFCCIMService sharedWFCIMService] getUnreadFriendRequestStatus];
}
//@UniJSMethod(uiThread = true)
//public void clearUnreadFriendRequestStatus() {
//    ChatManager.Instance().clearUnreadFriendRequestStatus();
//}
UNI_EXPORT_METHOD_SYNC(@selector(clearUnreadFriendRequestStatus))
- (void)clearUnreadFriendRequestStatus {
    [[WFCCIMService sharedWFCIMService] clearUnreadFriendRequestStatus];
}
//@UniJSMethod(uiThread = true)
//public void deleteFriend(String userId, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().deleteFriend(userId, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(getRemoteMessage:success:error:))
- (void)deleteFriend:(NSString *)userId success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] deleteFriend:userId success:^() {
        if(successCB) {
            successCB(nil, NO);
        }
    } error:^(int error_code) {
        if (errorCB) {
            errorCB(@(error_code), NO);
        }
    }];
}
//@UniJSMethod(uiThread = true)
//public void handleFriendRequest(String userId, boolean accept, JSCallback successCB, JSCallback failCB, String extra) {
//    ChatManager.Instance().handleFriendRequest(userId, accept, extra, new JSGeneralCallback(successCB, failCB));
//}
//
UNI_EXPORT_METHOD(@selector(handleFriendRequest:accept:success:error:extra:))
- (void)handleFriendRequest:(NSString *)userId accept:(BOOL)accept success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB extra:(NSString *)extra {
    [[WFCCIMService sharedWFCIMService] handleFriendRequest:userId accept:accept extra:extra success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public boolean isBlackListed(String userId) {
//    return ChatManager.Instance().isBlackListed(userId);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(isBlackListed:))
- (BOOL)isBlackListed:(NSString *)userId {
    return [[WFCCIMService sharedWFCIMService] isBlackListed:userId];
}
//@UniJSMethod(uiThread = false)
//public String getBlackList() {
//    List<String> list = ChatManager.Instance().getBlackList(false);
//    return JSONObject.toJSONString(list, ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getBlackList))
- (NSString *)getBlackList {
    NSArray *bl = [[WFCCIMService sharedWFCIMService] getBlackList:NO];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:bl options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}
//@UniJSMethod(uiThread = true)
//public void setBlackList(String userId, boolean block, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().setBlackList(userId, block, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(setBlackList:block:success:error:))
- (void)setBlackList:(NSString *)userId block:(BOOL)block success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] setBlackList:userId isBlackListed:block success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public String getMyFriendList(boolean refresh) {
//    List<String> list = ChatManager.Instance().getMyFriendList(refresh);
//    return JSONObject.toJSONString(list, ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getMyFriendList:))
- (NSString *)getMyFriendList:(BOOL)refresh {
    NSArray *bl = [[WFCCIMService sharedWFCIMService] getMyFriendList:refresh];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:bl options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}
//@UniJSMethod(uiThread = false)
//public String getFriendList(boolean refresh) {
//    List<Friend> list = ChatManager.Instance().getFriendList(refresh);
//    return JSONObject.toJSONString(list, ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getFriendList:))
- (NSString *)getFriendList:(BOOL)refresh {
    NSArray<WFCCFriend *> *bl = [[WFCCIMService sharedWFCIMService] getFriendList:refresh];
    return [self convertModelListToString:bl];
}
//@UniJSMethod(uiThread = false)
//public String getFriendAlias(String userId) {
//    return ChatManager.Instance().getFriendAlias(userId);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getFriendAlias:))
- (NSString *)getFriendAlias:(NSString *)userId {
    return [[WFCCIMService sharedWFCIMService] getFriendAlias:userId];
}
//@UniJSMethod(uiThread = true)
//public void setFriendAlias(String userId, String alias, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().setFriendAlias(userId, alias, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(setFriendAlias:alias:success:error:))
- (void)setFriendAlias:(NSString *)userId alias:(NSString *)alias success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] setFriend:userId alias:alias success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void createGroup(String groupId, int type, String name, String portrait, String groupExtra, List<String> memberIds, String memberExtra, List<Integer> lines, String messagePayloadString, JSCallback successCB, JSCallback failCB) {
//    GroupInfo.GroupType groupType = GroupInfo.GroupType.type(type);
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayloadString);
//    ChatManager.Instance().createGroup(groupId, name, portrait, groupType, groupExtra, memberIds, memberExtra, lines, messageContent, new JSGeneralCallback2(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(createGroup:type:name:portrait:groupExtra:memberIds:memberExtra:lines:messagePayload:success:error:))
- (void)createGroup:(NSString *)groupId type:(int)type name:(NSString *)name portrait:(NSString *)portrait groupExtra:(NSString *)groupExtra memberIds:(NSArray<NSString *> *)memberIds memberExtra:(NSString *)memberExtra lines:(NSArray<NSNumber *> *)lines messagePayload:(NSString *)strMessagePayload success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] createGroup:groupId name:name portrait:portrait type:type groupExtra:groupExtra members:memberIds memberExtra:memberExtra notifyLines:lines notifyContent:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] success:^(NSString *groupId) {
        if(successCB) successCB(groupId, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void setGroupManager(String groupId, boolean isSet, List<String> memberIds, List<Integer> lines, String messagePayloadStr, JSCallback successCB, JSCallback failCB) {
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayloadStr);
//    ChatManager.Instance().setGroupManager(groupId, isSet, memberIds, lines, messageContent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(setGroupManager:isSet:memberIds:lines:messagePayload:success:error:))
- (void)setGroupManager:(NSString *)groupId isSet:(BOOL)isSet memberIds:(NSArray<NSString *> *)memberIds lines:(NSArray<NSNumber *> *)lines messagePayload:(NSString *)strMessagePayload success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] setGroupManager:groupId isSet:isSet memberIds:memberIds notifyLines:lines notifyContent:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void allowGroupMember(String groupId, boolean isSet, List<String> memberIds, List<Integer> lines, String messagePayloadString, JSCallback successCB, JSCallback failCB) {
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayloadString);
//    ChatManager.Instance().allowGroupMember(groupId, isSet, memberIds, lines, messageContent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(allowGroupMember:isSet:memberIds:lines:messagePayload:success:error:))
- (void)allowGroupMember:(NSString *)groupId isSet:(BOOL)isSet memberIds:(NSArray<NSString *> *)memberIds lines:(NSArray<NSNumber *> *)lines messagePayload:(NSString *)strMessagePayload success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] allowGroupMember:groupId isSet:isSet memberIds:memberIds notifyLines:lines notifyContent:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void muteGroupMember(String groupId, boolean isSet, List<String> memberIds, List<Integer> lines, String messagePayloadString, JSCallback successCB, JSCallback failCB) {
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayloadString);
//    ChatManager.Instance().muteGroupMember(groupId, isSet, memberIds, lines, messageContent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(muteGroupMember:isSet:memberIds:lines:messagePayload:success:error:))
- (void)muteGroupMember:(NSString *)groupId isSet:(BOOL)isSet memberIds:(NSArray<NSString *> *)memberIds lines:(NSArray<NSNumber *> *)lines messagePayload:(NSString *)strMessagePayload success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] muteGroupMember:groupId isSet:isSet memberIds:memberIds notifyLines:lines notifyContent:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public String getGroupInfo(String groupId, boolean refresh) {
//    GroupInfo groupInfo = ChatManager.Instance().getGroupInfo(groupId, refresh);
//    return JSONObject.toJSONString(groupInfo, ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getGroupInfo:refresh:))
- (NSString *)getGroupInfo:(NSString *)groupId refresh:(BOOL)refresh {
    WFCCGroupInfo *groupInfo = [[WFCCIMService sharedWFCIMService] getGroupInfo:groupId refresh:refresh];
    if(groupInfo)
        return [groupInfo toJsonStr];
    else
        return [[NullGroupInfo nullGroupOfId:groupId] toJsonStr];
}

//@UniJSMethod(uiThread = false)
//public String getGroupInfos(List<String> groupIds, boolean refresh) {
//    List<GroupInfo> groupInfos = ChatManager.Instance().getGroupInfos(groupIds, refresh);
//    return JSONObject.toJSONString(groupInfos, ClientUniAppHookProxy.serializeConfig);
//}
UNI_EXPORT_METHOD_SYNC(@selector(getGroupInfos:refresh:))
- (NSString *)getGroupInfos:(NSArray<NSString *> *)groupIds refresh:(BOOL)refresh {
    NSArray<WFCCGroupInfo *> *groupInfos = [[WFCCIMService sharedWFCIMService] getGroupInfos:groupIds refresh:refresh];
    return [self convertModelListToString:groupInfos];
}

//@UniJSMethod(uiThread = true)
//public void getGroupInfoEx(String groupId, boolean refresh, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().getGroupInfo(groupId, refresh, new GetGroupInfoCallback() {
//        @Override
//        public void onSuccess(GroupInfo groupInfo) {
//            if (successCB != null) {
//                successCB.invoke(JSONObject.toJSONString(groupInfo, ClientUniAppHookProxy.serializeConfig));
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            if (failCB != null) {
//                failCB.invoke(errorCode);
//            }
//        }
//    });
//}
UNI_EXPORT_METHOD(@selector(getGroupInfoEx:refresh:success:error:))
- (void)getGroupInfoEx:(NSString *)groupId refresh:(BOOL)refresh success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] getGroupInfo:groupId refresh:refresh success:^(WFCCGroupInfo *groupInfo) {
        if(successCB) successCB([groupInfo toJsonStr], NO);
    } error:^(int errorCode) {
        if(errorCB) errorCB(@(errorCode), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void addMembers(String groupId, List<String> memberIds, String extra, List<Integer> lines, String messagePayloadString, JSCallback successCB, JSCallback failCB) {
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayloadString);
//    ChatManager.Instance().addGroupMembers(groupId, memberIds, extra, lines, messageContent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(addMembers:memberIds:extra:lines:messagePayload:success:error:))
- (void)addMembers:(NSString *)groupId memberIds:(NSArray<NSString *> *)memberIds extra:(NSString *)extra lines:(NSArray<NSNumber *> *)lines messagePayload:(NSString *)strMessagePayload success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] addMembers:memberIds toGroup:groupId memberExtra:extra notifyLines:lines notifyContent:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public String getGroupMembers(String groupId, boolean refresh) {
//    List<GroupMember> groupMembers = ChatManager.Instance().getGroupMembers(groupId, refresh);
//    return JSONObject.toJSONString(groupMembers, ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getGroupMembers:refresh:))
- (NSString *)getGroupMembers:(NSString *)groupId refresh:(BOOL)refresh {
    NSArray<WFCCGroupMember *> *gms = [[WFCCIMService sharedWFCIMService] getGroupMembers:groupId forceUpdate:refresh];
    return [self convertModelListToString:gms];
}
//@UniJSMethod(uiThread = false)
//public String getGroupMember(String groupId, String memberId) {
//    GroupMember groupMember = ChatManager.Instance().getGroupMember(groupId, memberId);
//    return JSONObject.toJSONString(groupMember, ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getGroupMember:memberId:))
- (NSString *)getGroupMember:(NSString *)groupId memberId:(NSString *)memberId {
    WFCCGroupMember *groupMember = [[WFCCIMService sharedWFCIMService] getGroupMember:groupId memberId:memberId];
    return [groupMember toJsonStr];
}

//@UniJSMethod(uiThread = false)
//public String getGroupMembersByType(String groupId, int groupMemberType) {
//    List<GroupMember> groupMembers = ChatManager.Instance().getGroupMembersByType(groupId, GroupMember.GroupMemberType.type(groupMemberType));
//    return JSONObject.toJSONString(groupMembers, ClientUniAppHookProxy.serializeConfig);
//}
UNI_EXPORT_METHOD_SYNC(@selector(getGroupMembersByType:type:))
- (NSString *)getGroupMembersByType:(NSString *)groupId type:(WFCCGroupMemberType)memberType {
    NSArray<WFCCGroupMember *> *members = [[WFCCIMService sharedWFCIMService] getGroupMembers:groupId type:memberType];
    return [self convertModelListToString:members];
}

//@UniJSMethod(uiThread = true)
//public void getGroupMembersEx(String groupId, boolean refresh, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().getGroupMembers(groupId, refresh, new GetGroupMembersCallback() {
//        @Override
//        public void onSuccess(List<GroupMember> groupMembers) {
//            if (successCB != null) {
//                successCB.invoke(JSONObject.toJSONString(groupMembers, ClientUniAppHookProxy.serializeConfig));
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            if (failCB != null) {
//                failCB.invoke(errorCode);
//            }
//        }
//    });
//}
UNI_EXPORT_METHOD(@selector(getGroupMembersEx:refresh:success:error:))
- (void)getGroupMembersEx:(NSString *)groupId refresh:(BOOL)refresh  success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    __weak typeof(self)ws = self;
    [[WFCCIMService sharedWFCIMService] getGroupMembers:groupId refresh:refresh success:^(NSString *groupId, NSArray<WFCCGroupMember *> *ms) {
        if(successCB) successCB([ws convertModelListToString:ms], NO);
    } error:^(int errorCode) {
        if(errorCB) errorCB(@(errorCode), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void kickoffMembers(String groupId, List<String> memberIds, List<Integer> lines, String messagePayloadString, JSCallback successCB, JSCallback failCB) {
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayloadString);
//    ChatManager.Instance().removeGroupMembers(groupId, memberIds, lines, messageContent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(kickoffMembers:memberIds:lines:messagePayload:success:error:))
- (void)kickoffMembers:(NSString *)groupId memberIds:(NSArray<NSString *> *)memberIds lines:(NSArray<NSNumber *> *)lines messagePayload:(NSString *)strMessagePayload success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] kickoffMembers:memberIds fromGroup:groupId notifyLines:lines notifyContent:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void quitGroup(String groupId, List<Integer> lines, String messagePayloadString, JSCallback successCB, JSCallback failCB) {
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayloadString);
//    ChatManager.Instance().quitGroup(groupId, lines, messageContent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(quitGroup:lines:messagePayload:success:error:))
- (void)quitGroup:(NSString *)groupId lines:(NSArray<NSNumber *> *)lines messagePayload:(NSString *)strMessagePayload success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] quitGroup:groupId notifyLines:lines notifyContent:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}

UNI_EXPORT_METHOD(@selector(quitGroupEx:keepMessage:lines:messagePayload:success:error:))
- (void)quitGroupEx:(NSString *)groupId keepMessage:(BOOL)keepMessage lines:(NSArray<NSNumber *> *)lines messagePayload:(NSString *)strMessagePayload success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] quitGroup:groupId keepMessage:keepMessage notifyLines:lines notifyContent:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void dismissGroup(String groupId, List<Integer> lines, String messagePayloadString, JSCallback successCB, JSCallback failCB) {
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayloadString);
//    ChatManager.Instance().dismissGroup(groupId, lines, messageContent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(dismissGroup:lines:messagePayload:success:error:))
- (void)dismissGroup:(NSString *)groupId lines:(NSArray<NSNumber *> *)lines messagePayload:(NSString *)strMessagePayload success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] dismissGroup:groupId notifyLines:lines notifyContent:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void modifyGroupInfo(String groupId, int type, String newValue, List<Integer> lines, String messagePayloadString, JSCallback successCB, JSCallback failCB) {
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayloadString);
//    ModifyGroupInfoType modifyGroupInfoType = ModifyGroupInfoType.type(type);
//    ChatManager.Instance().modifyGroupInfo(groupId, modifyGroupInfoType, newValue, lines, messageContent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(modifyGroupInfo:type:newValue:lines:messagePayload:success:error:))
- (void)modifyGroupInfo:(NSString *)groupId type:(int)type newValue:(NSString *)newValue lines:(NSArray<NSNumber *> *)lines messagePayload:(NSString *)strMessagePayload success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] modifyGroupInfo:groupId type:type newValue:newValue notifyLines:lines notifyContent:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void modifyGroupAlias(String groupId, String alias, List<Integer> lines, String messagePayloadString, JSCallback successCB, JSCallback failCB) {
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayloadString);
//    ChatManager.Instance().modifyGroupAlias(groupId, alias, lines, messageContent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(modifyGroupAlias:alias:lines:messagePayload:success:error:))
- (void)modifyGroupAlias:(NSString *)groupId alias:(NSString *)alias lines:(NSArray<NSNumber *> *)lines messagePayload:(NSString *)strMessagePayload success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] modifyGroupAlias:groupId alias:alias notifyLines:lines notifyContent:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void modifyGroupMemberAlias(String groupId, String memberId, String alias, List<Integer> lines, String messagePayloadString, JSCallback successCB, JSCallback failCB) {
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayloadString);
//    ChatManager.Instance().modifyGroupMemberAlias(groupId, memberId, alias, lines, messageContent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(modifyGroupMemberAlias:memberId:alias:lines:messagePayload:success:error:))
- (void)modifyGroupMemberAlias:(NSString *)groupId memberId:(NSString *)memberId alias:(NSString *)alias lines:(NSArray<NSNumber *> *)lines messagePayload:(NSString *)strMessagePayload success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] modifyGroupMemberAlias:groupId memberId:memberId alias:alias notifyLines:lines notifyContent:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void modifyGroupMemberExtra(String groupId, String extra, String alias, List<Integer> lines, String messagePayloadString, JSCallback successCB, JSCallback failCB) {
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayloadString);
//    ChatManager.Instance().modifyGroupMemberExtra(groupId, extra, alias, lines, messageContent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(modifyGroupMemberExtra:extra:alias:lines:messagePayload:success:error:))
- (void)modifyGroupMemberExtra:(NSString *)groupId extra:(NSString *)extra alias:(NSString *)alias lines:(NSArray<NSNumber *> *)lines messagePayload:(NSString *)strMessagePayload success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] modifyGroupMemberExtra:groupId extra:extra notifyLines:lines notifyContent:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void transferGroup(String groupId, String newOwner, List<Integer> lines, String messagePayloadString, JSCallback successCB, JSCallback failCB) {
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayloadString);
//    ChatManager.Instance().transferGroup(groupId, newOwner, lines, messageContent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(transferGroup:newOwner:lines:messagePayload:success:error:))
- (void)transferGroup:(NSString *)groupId newOwner:(NSString *)newOwner lines:(NSArray<NSNumber *> *)lines messagePayload:(NSString *)strMessagePayload success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] transferGroup:groupId to:newOwner notifyLines:lines notifyContent:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public String getFavGroups() {
//    List<String> favGroupIds = new ArrayList<>();
//    Map<String, String> groupIdMap = ChatManager.Instance().getUserSettings(UserSettingScope.FavoriteGroup);
//    if (groupIdMap != null && !groupIdMap.isEmpty()) {
//        for (Map.Entry<String, String> entry : groupIdMap.entrySet()) {
//            if (entry.getValue().equals("1")) {
//                favGroupIds.add(entry.getKey());
//            }
//        }
//    }
//    return JSONObject.toJSONString(favGroupIds, ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getFavGroups))
- (NSString *)getFavGroups {
    NSArray<NSString *> *favgs = [[WFCCIMService sharedWFCIMService] getFavGroups];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:favgs options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}
//@UniJSMethod(uiThread = false)
//public boolean isFavGroup(String groupId) {
//    return ChatManager.Instance().isFavGroup(groupId);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(isFavGroup:))
- (BOOL)isFavGroup:(NSString *)groupId {
    return [[WFCCIMService sharedWFCIMService] isFavGroup:groupId];
}
//@UniJSMethod(uiThread = true)
//public void setFavGroup(String groupId, boolean fav, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().setFavGroup(groupId, fav, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(setFavGroup:fav:success:error:))
- (void)setFavGroup:(NSString *)groupId fav:(BOOL)fav success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] setFavGroup:groupId fav:fav success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int errorCode) {
        if(errorCB) errorCB(@(errorCode), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public String getUserSetting(int scope, String key) {
//    return ChatManager.Instance().getUserSetting(scope, key);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getUserSetting:key:))
- (NSString *)getUserSetting:(int)scope key:(NSString *)key {
    return [[WFCCIMService sharedWFCIMService] getUserSetting:scope key:key];
}
//@UniJSMethod(uiThread = false)
//public String getUserSettings(int scope) {
//    Map<String, String> settings = ChatManager.Instance().getUserSettings(scope);
//    JSONArray array = Util.strStrMap2Array(settings);
//    return JSONObject.toJSONString(array, ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getUserSettings:))
- (NSString *)getUserSettings:(int)scope {
    NSDictionary *sts = [[WFCCIMService sharedWFCIMService] getUserSettings:scope];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[self map2Array:sts] options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}
//@UniJSMethod(uiThread = true)
//public void setUserSetting(int scope, String key, String value, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().setUserSetting(scope, key, value, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(setUserSetting:key:value:success:error:))
- (void)setUserSetting:(int)scope key:(NSString *)key value:(NSString *)value success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] setUserSetting:scope key:key value:value success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void modifyMyInfo(int type, String value, JSCallback successCB, JSCallback failCB) {
//    List<ModifyMyInfoEntry> entries = new ArrayList<>();
//    ModifyMyInfoEntry entry = new ModifyMyInfoEntry(ModifyMyInfoType.type(type), value);
//    entries.add(entry);
//    ChatManager.Instance().modifyMyInfo(entries, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(modifyMyInfo:value:success:error:))
- (void)modifyMyInfo:(int)type value:(NSString *)value success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] modifyMyInfo:@{@(type):value} success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public boolean isGlobalSlient() {
//    return ChatManager.Instance().isGlobalSilent();
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(isGlobalSlient))
- (BOOL)isGlobalSlient {
    return [[WFCCIMService sharedWFCIMService] isGlobalSilent];
}
//@UniJSMethod(uiThread = true)
//public void setGlobalSlient(boolean silent, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().setGlobalSilent(silent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(setGlobalSlient:success:error:))
- (void)setGlobalSlient:(BOOL)silent success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] setGlobalSilent:silent success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public boolean isHiddenNotificationDetail() {
//    return ChatManager.Instance().isHiddenNotificationDetail();
//}
//

UNI_EXPORT_METHOD_SYNC(@selector(isHiddenNotificationDetail))
- (BOOL)isHiddenNotificationDetail {
    return [[WFCCIMService sharedWFCIMService] isHiddenNotificationDetail];
}
//@UniJSMethod(uiThread = true)
//public void setHiddenNotificationDetail(boolean hide, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().setHiddenNotificationDetail(hide, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(setHiddenNotificationDetail:success:error:))
- (void)setHiddenNotificationDetail:(BOOL)hide success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] setHiddenNotificationDetail:hide success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public boolean isHiddenGroupMemberName(String groupId) {
//    // TODO
//    return false;
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(isHiddenGroupMemberName:))
- (BOOL)isHiddenGroupMemberName:(NSString *)groupId {
    return [[WFCCIMService sharedWFCIMService] isHiddenGroupMemberName:groupId];
}
//@UniJSMethod(uiThread = false)
//public boolean isUserReceiptEnabled() {
//    return ChatManager.Instance().isUserEnableReceipt();
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(isUserReceiptEnabled))
- (BOOL)isUserReceiptEnabled {
    return [[WFCCIMService sharedWFCIMService] isUserEnableReceipt];
}
//@UniJSMethod(uiThread = true)
//public void setUserReceiptEnable(boolean enable, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().setUserEnableReceipt(enable, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(setUserReceiptEnable:success:error:))
- (void)setUserReceiptEnable:(BOOL)enable success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] setUserEnableReceipt:enable success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void joinChatroom(String chatroomId, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().joinChatRoom(chatroomId, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(joinChatroom:success:error:))
- (void)joinChatroom:(NSString *)chatroomId success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] joinChatroom:chatroomId success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void quitChatroom(String chatroomId, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().quitChatRoom(chatroomId, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(quitChatroom:success:error:))
- (void)quitChatroom:(NSString *)chatroomId success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] quitChatroom:chatroomId success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void getChatroomInfo(String chatroomId, long updateDt, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().getChatRoomInfo(chatroomId, updateDt, new GetChatRoomInfoCallback() {
//        @Override
//        public void onSuccess(ChatRoomInfo chatRoomInfo) {
//            if (successCB != null) {
//                successCB.invoke(JSONObject.toJSONString(chatRoomInfo, ClientUniAppHookProxy.serializeConfig));
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            if (failCB != null) {
//                failCB.invoke(errorCode);
//            }
//        }
//    });
//}
UNI_EXPORT_METHOD(@selector(getChatroomInfo:updateDt:success:error:))
- (void)getChatroomInfo:(NSString *)chatroomId updateDt:(long long)updateDt success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] getChatroomInfo:chatroomId upateDt:updateDt success:^(WFCCChatroomInfo *chatroomInfo) {
        if(successCB) successCB([chatroomInfo toJsonStr], NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void getChatroomMemberInfo(String chatroomId, int maxCount, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().getChatRoomMembersInfo(chatroomId, maxCount, new GetChatRoomMembersInfoCallback() {
//        @Override
//        public void onSuccess(ChatRoomMembersInfo chatRoomMembersInfo) {
//            if (successCB != null) {
//                successCB.invoke(JSONObject.toJSONString(chatRoomMembersInfo, ClientUniAppHookProxy.serializeConfig));
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            if (failCB != null) {
//                failCB.invoke(errorCode);
//            }
//        }
//    });
//}
UNI_EXPORT_METHOD(@selector(getChatroomMemberInfo:maxCount:updateDt:success:error:))
- (void)getChatroomMemberInfo:(NSString *)chatroomId maxCount:(int)maxCount updateDt:(long long)updateDt success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] getChatroomMemberInfo:chatroomId maxCount:maxCount success:^(WFCCChatroomMemberInfo *memberInfo) {
        if(successCB) successCB([memberInfo toJsonStr], NO);
    } error:^(int error_code) {
        if(errorCB)errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void createChannel(String name, String portrait, int status, String desc, String extra, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().createChannel(null, name, portrait, desc, extra, new JSGeneralCallback2(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(createChannel:portrait:desc:extra:success:error:))
- (void)createChannel:(NSString *)name portrait:(NSString *)portrait status:(int)status desc:(NSString *)desc extra:(NSString *)extra success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] createChannel:nil portrait:portrait desc:desc extra:extra success:^(WFCCChannelInfo *channelInfo) {
        if(successCB) successCB([channelInfo toJsonStr], NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public String getChannelInfo(String channelId, boolean refresh) {
//    ChannelInfo channelInfo = ChatManager.Instance().getChannelInfo(channelId, refresh);
//    return JSONObject.toJSONString(channelInfo, ClientUniAppHookProxy.serializeConfig);
//}
UNI_EXPORT_METHOD_SYNC(@selector(getChannelInfo:refresh:))
- (NSString *)getChannelInfo:(NSString *)channelId refresh:(BOOL)refresh {
    NSString *ret = [[[WFCCIMService sharedWFCIMService] getChannelInfo:channelId refresh:refresh] toJsonStr];
    if(!ret) {
        return @"";
    }
    return ret;
}
//@UniJSMethod(uiThread = true)
//public void modifyChannelInfo(String channelId, int type, String newValue, JSCallback successCB, JSCallback failCB) {
//    ModifyChannelInfoType modifyChannelInfoType = ModifyChannelInfoType.type(type);
//    ChatManager.Instance().modifyChannelInfo(channelId, modifyChannelInfoType, newValue, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(modifyChannelInfo:type:newValue:success:error:))
- (void)modifyChannelInfo:(NSString *)channelId type:(int)type newValue:(NSString *)newValue success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] modifyChannelInfo:channelId type:type newValue:newValue success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void searchChannel(String keyword, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().searchChannel(keyword, new SearchChannelCallback() {
//        @Override
//        public void onSuccess(List<ChannelInfo> channelInfos) {
//            if (successCB != null) {
//                successCB.invoke(JSONObject.toJSONString(channelInfos, ClientUniAppHookProxy.serializeConfig));
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            if (failCB != null) {
//                failCB.invoke(errorCode);
//            }
//        }
//    });
//}
UNI_EXPORT_METHOD(@selector(searchChannel:success:error:))
- (void)searchChannel:(NSString *)keyword success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    __weak typeof(self)ws = self;
    [[WFCCIMService sharedWFCIMService] searchChannel:keyword success:^(NSArray<WFCCChannelInfo *> *machedChannels) {
        if(successCB) successCB([ws convertModelListToString:machedChannels], NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public boolean isListenedChannel(String channelId) {
//    return ChatManager.Instance().isListenedChannel(channelId);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(isListenedChannel:))
- (BOOL)isListenedChannel:(NSString *)channelId {
    return [[WFCCIMService sharedWFCIMService] isListenedChannel:channelId];
}
//@UniJSMethod(uiThread = true)
//public void listenChannel(String channelId, boolean listen, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().listenChannel(channelId, listen, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(listenChannel:listen:success:error:))
- (void)listenChannel:(NSString *)channelId listen:(BOOL)listen success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] listenChannel:channelId listen:listen success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public String getMyChannels() {
//    List<String> channelIds = ChatManager.Instance().getMyChannels();
//    return JSONObject.toJSONString(channelIds, ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getMyChannels))
- (NSString *)getMyChannels {
    NSArray *list = [[WFCCIMService sharedWFCIMService] getMyChannels];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:list options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}
//@UniJSMethod(uiThread = false)
//public String getListenedChannels() {
//    List<String> channelIds = ChatManager.Instance().getListenedChannels();
//    return JSONObject.toJSONString(channelIds, ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getListenedChannels))
- (NSString *)getListenedChannels {
    NSArray *list = [[WFCCIMService sharedWFCIMService] getListenedChannels];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:list options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}
//@UniJSMethod(uiThread = true)
//public void destoryChannel(String channelId, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().destoryChannel(channelId, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(destoryChannel:success:error:))
- (void)destoryChannel:(NSString *)channelId success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] destoryChannel:channelId success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public String getConversationInfos(List<Integer> types, List<Integer> lines) {
//    List<Conversation.ConversationType> conversationTypes = new ArrayList<>();
//    for (Integer type : types) {
//        conversationTypes.add(Conversation.ConversationType.type(type));
//    }
//    List<ConversationInfo> conversationInfos = ChatManager.Instance().getConversationList(conversationTypes, lines);
//    List<JSConversationInfo> jsConversationInfos = Util.conversationInfosToJSConversationInfos(conversationInfos);
//    return JSONObject.toJSONString(jsConversationInfos, ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getConversationInfos:lines:))
- (NSString *)getConversationInfos:(NSArray<NSNumber *> *)types lines:(NSArray<NSNumber *> *)lines {
    NSArray<WFCCConversationInfo *> *conversationList = [[WFCCIMService sharedWFCIMService] getConversationInfos:types lines:lines];
    NSString *ret = [self convertModelListToString:conversationList];
    return ret;
}
//@UniJSMethod(uiThread = false)
//public String getConversationInfo(String strConv) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ConversationInfo conversationInfo = ChatManager.Instance().getConversation(conversation);
//    return JSONObject.toJSONString(JSConversationInfo.fromConversationInfo(conversationInfo), ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getConversationInfo:))
- (NSString *)getConversationInfo:(NSString *)strConv {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    WFCCConversationInfo *convInfo = [[WFCCIMService sharedWFCIMService] getConversationInfo:conversation];
    return [convInfo toJsonStr];
}
//@UniJSMethod(uiThread = false)
//public String searchConversation(String keyword, List<Integer> types, List<Integer> lines) {
//    List<Conversation.ConversationType> conversationTypes = new ArrayList<>();
//    for (Integer type : types) {
//        conversationTypes.add(Conversation.ConversationType.type(type));
//    }
//    List<ConversationSearchResult> conversationInfos = ChatManager.Instance().searchConversation(keyword, conversationTypes, lines);
//    return JSONObject.toJSONString(conversationInfos, ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(searchConversation:types:lines:))
- (NSString *)searchConversation:(NSString *)keywork types:(NSArray<NSNumber *> *)types lines:(NSArray<NSNumber *> *)lines {
    NSArray<WFCCConversationSearchInfo *> *resultList = [[WFCCIMService sharedWFCIMService] searchConversation:keywork inConversation:types lines:lines];
    return [self convertModelListToString:resultList];
}
//@UniJSMethod(uiThread = false)
//public void removeConversation(String strConv, boolean clearMsg) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().removeConversation(conversation, clearMsg);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(removeConversation:clearMsg:))
- (void)removeConversation:(NSString *)strConv clearMsg:(BOOL)clearMsg {
    [[WFCCIMService sharedWFCIMService] removeConversation:[self conversationFromJsonString:strConv] clearMessage:clearMsg];
}
//@UniJSMethod(uiThread = true)
//public void setConversationTop(String strConv, boolean top, JSCallback successCB, JSCallback failCB) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().setConversationTop(conversation, top, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(setConversationTop:top:success:error:))
- (void)setConversationTop:(NSString *)strConv top:(BOOL)top success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] setConversation:[self conversationFromJsonString:strConv] top:top success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void setConversationSlient(String strConv, boolean silent, JSCallback successCB, JSCallback failCB) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().setConversationSilent(conversation, silent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(setConversationSlient:silent:success:error:))
- (void)setConversationSlient:(NSString *)strConv silent:(BOOL)silent success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] setConversation:[self conversationFromJsonString:strConv] silent:silent success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public void setConversationDraft(String strConv, String draft) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().setConversationDraft(conversation, draft);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(setConversationDraft:draft:))
- (void)setConversationDraft:(NSString *)strConv draft:(NSString *)draft {
    [[WFCCIMService sharedWFCIMService] setConversation:[self conversationFromJsonString:strConv] draft:draft];
}
//@UniJSMethod(uiThread = false)
//public void setConversationTimestamp(String strConv, String timestamp) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().setConversationTimestamp(conversation, Long.parseLong(timestamp));
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(setConversationTimestamp:timestamp:))
- (void)setConversationTimestamp:(NSString *)strConv timestamp:(NSString *)timestamp {
    [[WFCCIMService sharedWFCIMService] setConversation:[self conversationFromJsonString:strConv] timestamp:atoll([timestamp UTF8String])];
}
//@UniJSMethod(uiThread = false)
//public String getUnreadCount(List<Integer> types, List<Integer> lines) {
//    List<Conversation.ConversationType> conversationTypes = new ArrayList<>();
//    for (Integer type : types) {
//        conversationTypes.add(Conversation.ConversationType.type(type));
//    }
//    UnreadCount unreadCount = ChatManager.Instance().getUnreadCountEx(conversationTypes, lines);
//    return JSONObject.toJSONString(unreadCount, ClientUniAppHookProxy.serializeConfig);
//}
UNI_EXPORT_METHOD_SYNC(@selector(getUnreadCount:lines:))
- (NSString *)getUnreadCount:(NSArray<NSNumber *> *)types lines:(NSArray<NSNumber *> *)lines {
    return [[[WFCCIMService sharedWFCIMService] getUnreadCount:types lines:lines] toJsonStr];
}
//
//@UniJSMethod(uiThread = false)
//public String getConversationUnreadCount(String strConv) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    UnreadCount unreadCount = ChatManager.Instance().getUnreadCount(conversation);
//    return JSONObject.toJSONString(unreadCount, ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getConversationUnreadCount:))
- (NSString *)getConversationUnreadCount:(NSString *)strConv {
    return [[[WFCCIMService sharedWFCIMService] getUnreadCount:[self conversationFromJsonString:strConv]] toJsonStr];
}
//@UniJSMethod(uiThread = false)
//public void clearUnreadStatus(String strConv) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().clearUnreadStatus(conversation);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(clearUnreadStatus:))
- (void)clearUnreadStatus:(NSString *)strConv {
    [[WFCCIMService sharedWFCIMService] clearUnreadStatus:[self conversationFromJsonString:strConv]];
}
//@UniJSMethod(uiThread = false)
//public void setLastReceivedMessageUnRead(String strConv, String messageUid, String timestamp) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().markAsUnRead(conversation, false);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(setLastReceivedMessageUnRead:messageUid:timestamp:))
- (BOOL)setLastReceivedMessageUnRead:(NSString *)strConv messageUid:(NSString *)messageUidStr timestamp:(NSString *)timestampStr {
    return [[WFCCIMService sharedWFCIMService] markAsUnRead:[self conversationFromJsonString:strConv] syncToOtherClient:NO];
}
//@UniJSMethod(uiThread = false)
//public String getConversationRead(String strConv) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    Map<String, Long> conversationRead = ChatManager.Instance().getConversationRead(conversation);
//    return JSONObject.toJSONString(Util.strLongMap2Array(conversationRead), ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getConversationRead:))
- (NSString *)getConversationRead:(NSString *)strConv {
    NSDictionary<NSString *, NSNumber *> *reads = [[WFCCIMService sharedWFCIMService] getConversationRead:[self conversationFromJsonString:strConv]];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[self map2Array:reads] options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}
//@UniJSMethod(uiThread = false)
//public String getMessageDelivery(String strConv) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    Map<String, Long> messageDelivery = ChatManager.Instance().getMessageDelivery(conversation);
//    return JSONObject.toJSONString(Util.strLongMap2Array(messageDelivery), ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getMessageDelivery:))
- (NSString *)getMessageDelivery:(NSString *)strConv {
    NSDictionary<NSString *, NSNumber *> *ds = [[WFCCIMService sharedWFCIMService] getMessageDelivery:[self conversationFromJsonString:strConv]];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[self map2Array:ds] options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}
//@UniJSMethod(uiThread = false)
//public void clearAllUnreadStatus() {
//    ChatManager.Instance().clearAllUnreadStatus();
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(clearAllUnreadStatus))
- (void)clearAllUnreadStatus {
    [[WFCCIMService sharedWFCIMService] clearAllUnreadStatus];
}
//@UniJSMethod(uiThread = false)
//public Long getConversationFirstUnreadMessageId(String strConv) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    return ChatManager.Instance().getFirstUnreadMessageId(conversation);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getConversationFirstUnreadMessageId:))
- (int)getConversationFirstUnreadMessageId:(NSString *)strConv {
    return (int)[[WFCCIMService sharedWFCIMService] getFirstUnreadMessageId:[self conversationFromJsonString:strConv]];
}
//@UniJSMethod(uiThread = false)
//public void setMediaMessagePlayed(int messageId) {
//    ChatManager.Instance().setMediaMessagePlayed(messageId);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(setMediaMessagePlayed:))
- (void)setMediaMessagePlayed:(int)messageId {
    [[WFCCIMService sharedWFCIMService] setMediaMessagePlayed:messageId];
}
//@UniJSMethod(uiThread = false)
//public void setMessageLocalExtra(int messageId, String extra) {
//    ChatManager.Instance().setMessageLocalExtra(messageId, extra);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(setMessageLocalExtra:extra:))
- (void)setMessageLocalExtra:(int)messageId extra:(NSString *)extra {
    [[WFCCIMService sharedWFCIMService] setMessage:messageId localExtra:extra];
}
//@UniJSMethod(uiThread = false)
//public boolean isMyFriend(String userId) {
//    return ChatManager.Instance().isMyFriend(userId);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(isMyFriend:))
- (BOOL)isMyFriend:(NSString *)userId {
    return [[WFCCIMService sharedWFCIMService] isMyFriend:userId];
}
//@UniJSMethod(uiThread = true)
//public void sendFriendRequest(String userId, String reason, String extra, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().sendFriendRequest(userId, reason, extra, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(sendFriendRequest:reason:extra:success:error:))
- (void)sendFriendRequest:(NSString *)userId reason:(NSString *)reason extra:(NSString *)extra success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] sendFriendRequest:userId reason:reason extra:extra success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public String getMessages(String strConv, List<Integer> types, int fromIndex, boolean before, int count, String withUser) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    CountDownLatch latch = new CountDownLatch(1);
//
//    List<Message> messageList = new ArrayList<>();
//    ChatManager.Instance().getMessages(conversation, types, fromIndex, before, count, withUser, new GetMessageCallback() {
//        @Override
//        public void onSuccess(List<Message> messages, boolean hasMore) {
//            messageList.addAll(messages);
//            if (!hasMore) {
//                latch.countDown();
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            latch.countDown();
//        }
//    });
//    try {
//        latch.await();
//    } catch (InterruptedException e) {
//        e.printStackTrace();
//    }
//    return JSONObject.toJSONString(Util.messagesToJSMessages(messageList), ClientUniAppHookProxy.serializeConfig);
//}
//
- (NSArray<WFCCMessage *> *)sortMessages:(NSArray<WFCCMessage *> *)messages ascend:(BOOL)ascend {
    return [messages sortedArrayUsingComparator:^NSComparisonResult(WFCCMessage *  _Nonnull obj1, WFCCMessage *  _Nonnull obj2) {
        NSComparisonResult result = NSOrderedDescending;
        if(obj1.serverTime && obj2.serverTime) {
            if(obj1.serverTime == obj2.serverTime)
                result = NSOrderedSame;
            if(obj1.serverTime < obj2.serverTime)
                result = NSOrderedAscending;
        } else if(obj1.messageId == obj2.messageId) {
            result = NSOrderedSame;
        } else if(obj1.messageId < obj2.messageId) {
            result = NSOrderedAscending;
        }

        if(!ascend) {
            result = -result;
        }

        return result;
    }];
}
UNI_EXPORT_METHOD_SYNC(@selector(getMessages:types:fromIndex:before:count:withUser:))
- (NSString *)getMessages:(NSString *)strConv types:(NSArray<NSNumber *> *)types fromIndex:(int)fromIndex before:(BOOL)before count:(int)count withUser:(NSString *)withUser {
    NSArray<WFCCMessage *> *messages = [[WFCCIMService sharedWFCIMService] getMessages:[self conversationFromJsonString:strConv] contentTypes:types from:fromIndex count:before?count:-count withUser:withUser];
    messages = [self sortMessages:messages ascend:YES];
    return [self convertModelListToString:messages];
}
//@UniJSMethod(uiThread = false)
//public String getMessagesEx(List<Integer> convTypes, List<Integer> lines, List<Integer> contentTypes, int fromIndex, boolean before, int count, String withUser) {
//    CountDownLatch latch = new CountDownLatch(1);
//
//    List<Message> messageList = new ArrayList<>();
//
//    List<Conversation.ConversationType> conversationTypes = new ArrayList<>();
//    for (Integer type : convTypes) {
//        conversationTypes.add(Conversation.ConversationType.type(type));
//    }
//    ChatManager.Instance().getMessagesEx(conversationTypes, contentTypes, lines, fromIndex, before, count, withUser, new GetMessageCallback() {
//        @Override
//        public void onSuccess(List<Message> messages, boolean hasMore) {
//            messageList.addAll(messages);
//            if (!hasMore) {
//                latch.countDown();
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            latch.countDown();
//        }
//    });
//    try {
//        latch.await();
//    } catch (InterruptedException e) {
//        e.printStackTrace();
//    }
//    return JSONObject.toJSONString(Util.messagesToJSMessages(messageList), ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getMessagesEx:lines:contentTypes:fromIndex:before:count:withUser:))
- (NSString *)getMessagesEx:(NSArray<NSNumber *> *)convTypes lines:(NSArray<NSNumber *> *)lines contentTypes:(NSArray<NSNumber *> *)contentTypes fromIndex:(int)fromIndex before:(BOOL)before count:(int)count withUser:(NSString *)withUser {
    NSArray<WFCCMessage *> *messages = [[WFCCIMService sharedWFCIMService] getMessages:convTypes lines:lines contentTypes:contentTypes from:fromIndex count:before?count:-count withUser:withUser];
    messages = [self sortMessages:messages ascend:YES];
    return [self convertModelListToString:messages];
}
//@UniJSMethod(uiThread = false)
//public String getUserMessages(String userId, String strConv, List<Integer> types, int fromIndex, boolean before, int count) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//
//    CountDownLatch latch = new CountDownLatch(1);
//
//    List<Message> messageList = new ArrayList<>();
//    ChatManager.Instance().getUserMessages(userId, conversation, fromIndex, before, count, new GetMessageCallback() {
//        @Override
//        public void onSuccess(List<Message> messages, boolean hasMore) {
//            messageList.addAll(messages);
//            if (!hasMore) {
//                latch.countDown();
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            latch.countDown();
//        }
//    });
//    try {
//        latch.await();
//    } catch (InterruptedException e) {
//        e.printStackTrace();
//    }
//    return JSONObject.toJSONString(Util.messagesToJSMessages(messageList), ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getUserMessages:conversation:types:fromIndex:before:count:))
- (NSString *)getUserMessages:(NSString *)userId conversation:(NSString *)strConv types:(NSArray<NSNumber *> *)types fromIndex:(int)fromIndex before:(BOOL)before count:(int)count {
    NSArray<WFCCMessage *> *messages = [[WFCCIMService sharedWFCIMService] getUserMessages:userId conversation:[self conversationFromJsonString:strConv] contentTypes:types from:fromIndex count:before?count:-count];
    messages = [self sortMessages:messages ascend:YES];
    return [self convertModelListToString:messages];
}
//@UniJSMethod(uiThread = false)
//public String getUserMessagesEx(String userId, List<Integer> convTypes, List<Integer> lines, List<Integer> types, int fromIndex, boolean before, int count) {
//    List<Conversation.ConversationType> conversationTypes = new ArrayList<>();
//    for (Integer type : convTypes) {
//        conversationTypes.add(Conversation.ConversationType.type(type));
//    }
//
//    CountDownLatch latch = new CountDownLatch(1);
//    List<Message> messageList = new ArrayList<>();
//    ChatManager.Instance().getUserMessagesEx(userId, conversationTypes, lines, types, fromIndex, before, count, new GetMessageCallback() {
//        @Override
//        public void onSuccess(List<Message> messages, boolean hasMore) {
//            messageList.addAll(messages);
//            if (!hasMore) {
//                latch.countDown();
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            latch.countDown();
//        }
//    });
//    try {
//        latch.await();
//    } catch (InterruptedException e) {
//        e.printStackTrace();
//    }
//    return JSONObject.toJSONString(Util.messagesToJSMessages(messageList), ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getUserMessagesEx:convTypes:lines:contentTypes:fromIndex:before:count:))
- (NSString *)getUserMessagesEx:(NSString *)userId convTypes:(NSArray<NSNumber *> *)convTypes lines:(NSArray<NSNumber *> *)lines contentTypes:(NSArray<NSNumber *> *)contentTypes fromIndex:(int)fromIndex before:(BOOL)before count:(int)count {
    NSArray<WFCCMessage *> *messages = [[WFCCIMService sharedWFCIMService] getUserMessages:userId conversationTypes:convTypes lines:lines contentTypes:contentTypes from:fromIndex count:before?count:-count];
    messages = [self sortMessages:messages ascend:YES];
    return [self convertModelListToString:messages];
}

//@UniJSMethod(uiThread = true)
//public void getMessagesV2(String strConv, int fromIndex, boolean before, int count, String withUser, JSCallback successCB, JSCallback failCB) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().getMessages(conversation, fromIndex, before, count, withUser, new JSGetMessageCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD_SYNC(@selector(getMessagesV2:fromIndex:before:count:withUser:success:error:))
- (void)getMessagesV2:(NSString *)strConv fromIndex:(int)fromIndex before:(BOOL)before count:(int)count withUser:(NSString *)withUser success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    __weak typeof(self)ws = self;
    [[WFCCIMService sharedWFCIMService] getMessagesV2:conversation contentTypes:nil from:fromIndex count:count withUser:withUser success:^(NSArray<WFCCMessage *> *messages) {
        if(successCB) {
            messages = [ws sortMessages:messages ascend:YES];
            successCB([ws convertModelListToString:messages], NO);
        }
    } error:^(int error_code) {
        if (errorCB) {
            errorCB(@(error_code), NO);
        }
    }];
}

//@UniJSMethod(uiThread = true)
//public void getMessagesExV2(List<Integer> conversationTypes, List<Integer> lines, int fromIndex, boolean before, int count, String withUser, List<Integer> contentTypes, JSCallback successCB, JSCallback failCB) {
//    List<Conversation.ConversationType> conversationTypeList = new ArrayList<>();
//    for (Integer convType : conversationTypes) {
//        conversationTypeList.add(Conversation.ConversationType.type(convType));
//    }
//    ChatManager.Instance().getMessagesEx(conversationTypeList, lines, contentTypes, fromIndex, before, count, withUser, new JSGetMessageCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD_SYNC(@selector(getMessagesExV2:lines:contentTypes:fromIndex:before:count:withUser:success:error:))
- (void)getMessagesExV2:(NSArray<NSNumber *> *)convTypes lines:(NSArray<NSNumber *> *)lines contentTypes:(NSArray<NSNumber *> *)contentTypes fromIndex:(int)fromIndex before:(BOOL)before count:(int)count withUser:(NSString *)withUser success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    __weak typeof(self)ws = self;
    [[WFCCIMService sharedWFCIMService] getMessagesV2:convTypes lines:lines contentTypes:contentTypes from:fromIndex count:count withUser:withUser success:^(NSArray<WFCCMessage *> *messages) {
        if(successCB) {
            messages = [ws sortMessages:messages ascend:YES];
            successCB([ws convertModelListToString:messages], NO);
        }
    } error:^(int error_code) {
        if (errorCB) {
            errorCB(@(error_code), NO);
        }
    }];
}

//@UniJSMethod(uiThread = true)
//public void getMessagesEx2V2(List<Integer> conversationTypes, List<Integer> lines, List<Integer> messageStatuses, int fromIndex, boolean before, int count, String withUser, JSCallback successCB, JSCallback failCB) {
//    List<Conversation.ConversationType> conversationTypeList = new ArrayList<>();
//    for (Integer convType : conversationTypes) {
//        conversationTypeList.add(Conversation.ConversationType.type(convType));
//    }
//    List<MessageStatus> messageStatusList = new ArrayList<>();
//    for (Integer status : messageStatuses) {
//        messageStatusList.add(MessageStatus.status(status));
//    }
//    ChatManager.Instance().getMessagesEx2(conversationTypeList, lines, messageStatusList, fromIndex, before, count, withUser, new JSGetMessageCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD_SYNC(@selector(getMessagesEx2V2:lines:messageStatuses:fromIndex:before:count:withUser:success:error:))
- (void)getMessagesEx2V2:(NSArray<NSNumber *> *)convTypes lines:(NSArray<NSNumber *> *)lines messageStatuses:(NSArray<NSNumber *> *)contentTypes fromIndex:(int)fromIndex before:(BOOL)before count:(int)count withUser:(NSString *)withUser success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    __weak typeof(self)ws = self;
    [[WFCCIMService sharedWFCIMService] getMessagesV2:convTypes lines:lines messageStatus:contentTypes from:fromIndex count:count withUser:withUser success:^(NSArray<WFCCMessage *> *messages) {
        if(successCB) {
            messages = [ws sortMessages:messages ascend:YES];
            successCB([ws convertModelListToString:messages], NO);
        }
    } error:^(int error_code) {
        if (errorCB) {
            errorCB(@(error_code), NO);
        }
    }];
}

//@UniJSMethod(uiThread = true)
//public void getMessagesByTimestampV2(String strConv, List<Integer> contentTypes, long timestamp, boolean before, int count, String withUser, JSCallback successCB, JSCallback failCB) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().getMessagesByTimestamp(conversation, contentTypes, timestamp, before, count, withUser, new JSGetMessageCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD_SYNC(@selector(getMessagesByTimestampV2:types:timestamp:before:count:withUser:success:error:))
- (void)getMessagesByTimestampV2:(NSString *)strConv types:(NSArray<NSNumber *> *)types timestamp:(int64_t)timestamp before:(BOOL)before count:(int)count withUser:(NSString *)withUser success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    __weak typeof(self)ws = self;
    [[WFCCIMService sharedWFCIMService] getMessagesV2:conversation contentTypes:types fromTime:timestamp count:count withUser:withUser success:^(NSArray<WFCCMessage *> *messages) {
        if(successCB) {
            messages = [ws sortMessages:messages ascend:YES];
            successCB([ws convertModelListToString:messages], NO);
        }
    } error:^(int error_code) {
        if (errorCB) {
            errorCB(@(error_code), NO);
        }
    }];
}

//@UniJSMethod(uiThread = true)
//public void getUserMessagesV2(String userId, String strConv, int fromIndex, boolean before, int count, JSCallback successCB, JSCallback failCB) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().getUserMessages(userId, conversation, fromIndex, before, count, new JSGetMessageCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD_SYNC(@selector(getUserMessagesV2:conversation:from:before:count:withUser:success:error:))
- (void)getUserMessagesV2:(NSString *)userId conversation:(NSString *)strConv from:(int)fromIndex before:(BOOL)before count:(int)count withUser:(NSString *)withUser success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    __weak typeof(self)ws = self;
    [[WFCCIMService sharedWFCIMService] getUserMessagesV2:userId conversation:conversation contentTypes:nil from:fromIndex count:count success:^(NSArray<WFCCMessage *> *messages) {
        if(successCB) {
            messages = [ws sortMessages:messages ascend:YES];
            successCB([ws convertModelListToString:messages], NO);
        }
    } error:^(int error_code) {
        if (errorCB) {
            errorCB(@(error_code), NO);
        }
    }];
}

//@UniJSMethod(uiThread = true)
//public void getUserMessagesExV2(String userId, List<Integer> conversationTypes, List<Integer> lines, int fromIndex, boolean before, int count, List<Integer> contentTypes, JSCallback successCB, JSCallback failCB) {
//    List<Conversation.ConversationType> conversationTypeList = new ArrayList<>();
//    for (Integer convType : conversationTypes) {
//        conversationTypeList.add(Conversation.ConversationType.type(convType));
//    }
//    ChatManager.Instance().getUserMessagesEx(userId, conversationTypeList, lines, contentTypes, fromIndex, before, count, new JSGetMessageCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD_SYNC(@selector(getUserMessagesExV2:conversationTypes:lines:from:before:count:withUser:success:error:))
- (void)getUserMessagesExV2:(NSString *)userId conversationTypes:(NSArray<NSNumber *> *)convTypes lines:(NSArray<NSNumber *> *)lines from:(int)fromIndex before:(BOOL)before count:(int)count withUser:(NSString *)withUser success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    
    __weak typeof(self)ws = self;
    [[WFCCIMService sharedWFCIMService] getUserMessagesV2:userId conversationTypes:convTypes lines:lines contentTypes:nil from:fromIndex count:count success:^(NSArray<WFCCMessage *> *messages) {
        if(successCB) {
            messages = [ws sortMessages:messages ascend:YES];
            successCB([ws convertModelListToString:messages], NO);
        }
    } error:^(int error_code) {
        if (errorCB) {
            errorCB(@(error_code), NO);
        }
    }];
}

//@UniJSMethod(uiThread = false)
//public String getMessage(long messageId) {
//    Message message = ChatManager.Instance().getMessage(messageId);
//    return JSONObject.toJSONString(JSMessage.fromMessage(message), ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getMessage:))
- (NSString *)getMessage:(int)messageId {
    WFCCMessage *message = [[WFCCIMService sharedWFCIMService] getMessage:messageId];
    return [message toJsonStr];
}
//@UniJSMethod(uiThread = false)
//public String getMessageByUid(String messageUid) {
//    Message message = ChatManager.Instance().getMessageByUid(Long.parseLong(messageUid));
//    return JSONObject.toJSONString(JSMessage.fromMessage(message), ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(getMessageByUid:))
- (NSString *)getMessageByUid:(NSString *)strUid {
    WFCCMessage *message = [[WFCCIMService sharedWFCIMService] getMessageByUid:atoll([strUid UTF8String])];
    return [message toJsonStr];
}
//@UniJSMethod(uiThread = false)
//public String searchMessage(String strConv, String keyword) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    List<Message> messageList = ChatManager.Instance().searchMessage(conversation, keyword, false, 500, 0);
//    return JSONObject.toJSONString(Util.messagesToJSMessages(messageList), ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(searchMessage:keyword:withUser:))
- (NSString *)searchMessage:(NSString *)strConv keyword:(NSString *)keyword withUser:(NSString *)user {
    NSArray<WFCCMessage *> *messages = [[WFCCIMService sharedWFCIMService] searchMessage:[self conversationFromJsonString:strConv] keyword:keyword order:false limit:500 offset:0 withUser:user];
    return [self convertModelListToString:messages];
}
//@UniJSMethod(uiThread = false)
//public String searchMessageEx(String strConv, String keyword, boolean desc, int limit, int offset) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    List<Message> messageList = ChatManager.Instance().searchMessage(conversation, keyword, desc, limit, offset);
//    return JSONObject.toJSONString(Util.messagesToJSMessages(messageList), ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(searchMessageEx:keyword:desc:limit:offset:withUser:))
- (NSString *)searchMessageEx:(NSString *)strConv keyword:(NSString *)keyword desc:(BOOL)desc limit:(int)limit offset:(int)offset withUser:(NSString *)user {
    NSArray<WFCCMessage *> *messages = [[WFCCIMService sharedWFCIMService] searchMessage:[self conversationFromJsonString:strConv] keyword:keyword order:desc limit:limit offset:offset withUser:user];
    return [self convertModelListToString:messages];
}
//@UniJSMethod(uiThread = false)
//public String searchMessageByTypes(String strConv, String keyword, List<Integer> contentTypes, boolean desc, int limit, int offset) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    List<Message> messageList = ChatManager.Instance().searchMessageByTypes(conversation, keyword, contentTypes, desc, limit, offset);
//    return JSONObject.toJSONString(Util.messagesToJSMessages(messageList), ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(searchMessageByTypes:keyword:contentTypes:desc:limit:offset:withUser:))
- (NSString *)searchMessageByTypes:(NSString *)strConv keyword:(NSString *)keyword contentTypes:(NSArray<NSNumber *> *)contentTypes desc:(BOOL)desc limit:(int)limit offset:(int)offset withUser:(NSString *)user {
    NSArray<WFCCMessage *> *messages = [[WFCCIMService sharedWFCIMService] searchMessage:[self conversationFromJsonString:strConv] keyword:keyword contentTypes:contentTypes order:desc limit:limit offset:offset withUser:user];
    return [self convertModelListToString:messages];
}
//@UniJSMethod(uiThread = false)
//public String searchMessageByTypesAndTimes(String strConv, String keyword, List<Integer> contentTypes, long startTime, long endTime, boolean desc, int limit, int offset) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    List<Message> messageList = ChatManager.Instance().searchMessageByTypesAndTimes(conversation, keyword, contentTypes, startTime, endTime, desc, limit, offset);
//    return JSONObject.toJSONString(Util.messagesToJSMessages(messageList), ClientUniAppHookProxy.serializeConfig);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(searchMessageByTypesAndTimes:keyword:contentTypes:startTime:endTime:desc:limit:offset:withUser:))
- (NSString *)searchMessageByTypesAndTimes:(NSString *)strConv keyword:(NSString *)keyword contentTypes:(NSArray<NSNumber *> *)contentTypes startTime:(int)startTime endTime:(int)endTime desc:(BOOL)desc limit:(int)limit offset:(int)offset withUser:(NSString *)user {
    NSArray<WFCCMessage *> *messages = [[WFCCIMService sharedWFCIMService] searchMessage:[self conversationFromJsonString:strConv] keyword:keyword contentTypes:contentTypes startTime:startTime endTime:endTime order:desc limit:limit offset:offset withUser:user];
    return [self convertModelListToString:messages];
}
//public String searchMessageEx2(List<Integer> convTypes, List<Integer> lines, List<Integer> contentTypes, String keyword, int fromIndex, boolean desc, int count) {
//
//    List<Conversation.ConversationType> conversationTypes = new ArrayList<>();
//    for (Integer type : convTypes) {
//        conversationTypes.add(Conversation.ConversationType.type(type));
//    }
//    CountDownLatch latch = new CountDownLatch(1);
//    List<Message> messageList = new ArrayList<>();
//    ChatManager.Instance().searchMessagesEx(conversationTypes, lines, contentTypes, keyword, fromIndex, desc, count, new GetMessageCallback() {
//        @Override
//        public void onSuccess(List<Message> messages, boolean hasMore) {
//            messageList.addAll(messages);
//            if (!hasMore) {
//                latch.countDown();
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            latch.countDown();
//        }
//    });
//    try {
//        latch.await();
//    } catch (InterruptedException e) {
//        e.printStackTrace();
//    }
//    return JSONObject.toJSONString(Util.messagesToJSMessages(messageList), ClientUniAppHookProxy.serializeConfig);
//}
UNI_EXPORT_METHOD_SYNC(@selector(searchMessageEx2:lines:contentTypes:keyword:fromIndex:desc:count:withUser:))
- (NSString *)searchMessageEx2:(NSArray<NSNumber *> *)convTypes lines:(NSArray<NSNumber *> *)lines contentTypes:(NSArray<NSNumber *> *)contentTypes keyword:(NSString *)keyword fromIndex:(int)fromIndex desc:(BOOL)desc count:(int)count withUser:(NSString *)user {
    NSArray<WFCCMessage *> *messages = [[WFCCIMService sharedWFCIMService] searchMessage:convTypes lines:lines contentTypes:contentTypes keyword:keyword from:fromIndex count:count withUser:user];
    return [self convertModelListToString:messages];
}
//@UniJSMethod(uiThread = true)
//public void sendMessage(String strConv, String messagePayloadString, List<String> toUsers, int expireDuration, JSCallback preparedCB, JSCallback progressCB, JSCallback successCB, JSCallback failCB) {
//    Log.d(TAG, "sendMessage " + messagePayloadString + " " + messagePayloadString + " " + toUsers + " " + expireDuration);
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayloadString);
//    ChatManager.Instance().sendMessage(conversation, messageContent, toUsers.toArray(new String[0]), expireDuration, new SendMessageCallback() {
//        @Override
//        public void onSuccess(long messageUid, long timestamp) {
//            if (successCB != null) {
//                JSONArray array = new JSONArray();
//                array.add(messageUid + "");
//                array.add(timestamp);
//                successCB.invoke(array);
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            if (failCB != null) {
//                failCB.invoke(errorCode);
//            }
//        }
//
//        @Override
//        public void onPrepare(long messageId, long savedTime) {
//            if (preparedCB != null) {
//                JSONArray array = new JSONArray();
//                array.add(messageId);
//                array.add(savedTime);
//                preparedCB.invoke(array);
//            }
//        }
//
//        @Override
//        public void onProgress(long uploaded, long total) {
//            if (progressCB != null) {
//                JSONArray array = new JSONArray();
//                array.add(uploaded);
//                array.add(total);
//                progressCB.invokeAndKeepAlive(array);
//            }
//        }
//    });
//}
UNI_EXPORT_METHOD(@selector(sendMessage:messagePayload:toUsers:expireDuration:prepared:progress:success:error:))
- (void)sendMessage:(NSString *)strConv messagePayload:(NSString *)strMessagePayload toUsers:(NSArray<NSString *> *)toUsers expireDuration:(int)expireDuration prepared:(UniModuleKeepAliveCallback)preparedCB progress:(UniModuleKeepAliveCallback)progressCB  success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    WFCCMessage *message = [[WFCCIMService sharedWFCIMService] sendMedia:[self conversationFromJsonString:strConv] content:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] toUsers:toUsers expireDuration:expireDuration success:^(long long messageUid, long long timestamp) {
        if(successCB) successCB(@[[NSString stringWithFormat:@"%lld", messageUid], @(timestamp)], NO);
    } progress:^(long uploaded, long total) {
        if(progressCB) progressCB(@[@(uploaded), @(total)], YES);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
    if (message.messageId && preparedCB) {
        preparedCB(@[@(message.messageId), @(message.serverTime)], YES);
    }
}
//@UniJSMethod(uiThread = true)
//public void recall(String messageUid, JSCallback successCB, JSCallback failCB) {
//    Message message = ChatManager.Instance().getMessageByUid(Long.parseLong(messageUid));
//    if (message == null) {
//        message = new Message();
//        message.messageUid = Long.parseLong(messageUid);
//    }
//    ChatManager.Instance().recallMessage(message, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(recall:success:error:))
- (void)recall:(NSString *)strMessageUid success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    WFCCMessage *msg = [[WFCCIMService sharedWFCIMService] getMessageByUid:atoll([strMessageUid UTF8String])];
    [[WFCCIMService sharedWFCIMService] recall:msg success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void deleteRemoteMessage(String messageUid, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().deleteRemoteMessage(Long.parseLong(messageUid), new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(deleteRemoteMessage:success:error:))
- (void)deleteRemoteMessage:(NSString *)strMessageUid success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] deleteRemoteMessage:atoll([strMessageUid UTF8String]) success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void updateRemoteMessageContent(String messageUid, String messagePayload, boolean distribute, boolean updateLocal, JSCallback successCB, JSCallback failCB) {
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayload);
//    ChatManager.Instance().updateRemoteMessageContent(Long.parseLong(messageUid), messageContent, distribute, updateLocal, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(updateRemoteMessageContent:messagePayload:distribute:updateLocal:success:error:))
- (void)updateRemoteMessageContent:(NSString *)strMessageUid messagePayload:(NSString *)strMessagePayload distribute:(BOOL)distribute updateLocal:(BOOL)updateLocal success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] updateRemoteMessage:atoll([strMessageUid UTF8String]) content:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]] distribute:distribute updateLocal:updateLocal success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public void deleteMessage(long messageId) {
//    Message message = ChatManager.Instance().getMessage(messageId);
//    if (message == null) {
//        message = new Message();
//        message.messageId = messageId;
//    }
//    ChatManager.Instance().deleteMessage(message);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(deleteMessage:))
- (void)deleteMessage:(int)messageId {
    [[WFCCIMService sharedWFCIMService] deleteMessage:messageId];
}
//@UniJSMethod(uiThread = true)
//public void watchOnlineState(int convType, List<String> targets, int duration, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().watchOnlineState(convType, targets.toArray(new String[0]), duration, new WatchOnlineStateCallback() {
//        @Override
//        public void onSuccess(UserOnlineState[] userOnlineStates) {
//            if (successCB != null) {
//                successCB.invoke(JSONObject.toJSONString(userOnlineStates, ClientUniAppHookProxy.serializeConfig));
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            if (failCB != null) {
//                failCB.invoke(errorCode);
//            }
//        }
//    });
//}
UNI_EXPORT_METHOD(@selector(watchOnlineState:targets:duration:success:error:))
- (void)watchOnlineState:(int)convType targets:(NSArray<NSString *> *)targets duration:(int)duration success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    __weak typeof(self)ws = self;
    [[WFCCIMService sharedWFCIMService] watchOnlineState:convType targets:targets duration:duration success:^(NSArray<WFCCUserOnlineState *> *states) {
        if(successCB) successCB([ws convertModelListToString:states], NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void unwatchOnlineState(int convType, List<String> targets, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().unWatchOnlineState(convType, targets.toArray(new String[0]), new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(unwatchOnlineState:targets:success:error:))
- (void)unwatchOnlineState:(int)convType targets:(NSArray<NSString *> *)targets success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] unwatchOnlineState:convType targets:targets  success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public boolean isCommercialServer() {
//    return ChatManager.Instance().isCommercialServer();
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(isCommercialServer))
- (BOOL)isCommercialServer {
    return [[WFCCIMService sharedWFCIMService] isCommercialServer];
}
//@UniJSMethod(uiThread = false)
//public boolean isReceiptEnabled() {
//    return ChatManager.Instance().isReceiptEnabled();
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(isReceiptEnabled))
- (BOOL)isReceiptEnabled {
    return [[WFCCIMService sharedWFCIMService] isReceiptEnabled];
}
//@UniJSMethod(uiThread = false)
//public boolean isGlobalDisableSyncDraft() {
//    return ChatManager.Instance().isGlobalDisableSyncDraft();
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(isGlobalDisableSyncDraft))
- (BOOL)isGlobalDisableSyncDraft {
    return [[WFCCIMService sharedWFCIMService] isGlobalDisableSyncDraft];
}
//@UniJSMethod(uiThread = false)
//public boolean isEnableUserOnlineState() {
//    // fixme
//    return true;
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(isEnableUserOnlineState))
- (BOOL)isEnableUserOnlineState {
    return [[WFCCIMService sharedWFCIMService] isEnableUserOnlineState];
}
//@UniJSMethod(uiThread = true)
//public void getAuthorizedMediaUrl(String messageUid, int mediaType, String mediaPath, JSCallback successCB, JSCallback failCB) {
//    MessageContentMediaType messageContentMediaType = MessageContentMediaType.mediaType(mediaType);
//    ChatManager.Instance().getAuthorizedMediaUrl(Long.parseLong(messageUid), messageContentMediaType, mediaPath, new GetAuthorizedMediaUrlCallback() {
//        @Override
//        public void onSuccess(String url, String backupUrl) {
//            if (successCB != null) {
//                JSONArray array = new JSONArray();
//                array.add(url);
//                array.add(backupUrl);
//                successCB.invoke(array);
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            if (failCB != null) {
//                failCB.invoke(errorCode);
//            }
//
//        }
//    });
//}
UNI_EXPORT_METHOD(@selector(getAuthorizedMediaUrl:mediaType:mediaPath:success:error:))
- (void)getAuthorizedMediaUrl:(NSString *)strMessageUid mediaType:(int)mediaType mediaPath:(NSString *)mediaPath success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] getAuthorizedMediaUrl:atoll([strMessageUid UTF8String]) mediaType:mediaType mediaPath:mediaPath success:^(NSString *authorizedUrl, NSString *backupAuthorizedUrl) {
        if(successCB) successCB(@[authorizedUrl, backupAuthorizedUrl], NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public boolean isSupportBigFilesUpload() {
//    return ChatManager.Instance().isSupportBigFilesUpload();
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(isSupportBigFilesUpload))
- (BOOL)isSupportBigFilesUpload {
    return [[WFCCIMService sharedWFCIMService] isSupportBigFilesUpload];
}
//@UniJSMethod(uiThread = true)
//public void getUploadMediaUrl(String fileName, int mediaType, String contentType, JSCallback successCB, JSCallback failCB) {
//    MessageContentMediaType messageContentMediaType = MessageContentMediaType.mediaType(mediaType);
//    ChatManager.Instance().getUploadUrl(fileName, messageContentMediaType, contentType, new GetUploadUrlCallback() {
//        @Override
//        public void onSuccess(String uploadUrl, String remoteUrl, String backUploadupUrl, int serverType) {
//            if (successCB != null) {
//                JSONArray array = new JSONArray();
//                array.add(uploadUrl);
//                array.add(remoteUrl);
//                array.add(backUploadupUrl);
//                array.add(serverType);
//                successCB.invoke(array);
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            if (failCB != null) {
//                failCB.invoke(errorCode);
//            }
//        }
//    });
//}
UNI_EXPORT_METHOD(@selector(getUploadMediaUrl:mediaType:contentType:success:error:))
- (void)getUploadMediaUrl:(NSString *)fileName mediaType:(int)mediaType contentType:(NSString *)contentType success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] getUploadUrl:fileName mediaType:mediaType contentType:contentType success:^(NSString *uploadUrl, NSString *downloadUrl, NSString *backupUploadUrl, int type) {
        if(successCB) successCB(@[uploadUrl, downloadUrl, backupUploadUrl, @(type)], NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void getConversationFiles(String strConv, String fromUser, String beforeUid, int count, JSCallback successCB, JSCallback failCB) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().getConversationFileRecords(conversation, fromUser, Long.parseLong(beforeUid), count, new JSGetFileRecordCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(getConversationFiles:fromUser:beforeMessageUid:order:count:success:error:))
- (void)getConversationFiles:(NSString *)strConv fromUser:(NSString *)fromUser beforeUid:(NSString *)beforeUid count:(int)count success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    __weak typeof(self)ws = self;
    [[WFCCIMService sharedWFCIMService] getConversationFiles:[self conversationFromJsonString:strConv] fromUser:fromUser beforeMessageUid:atoll([beforeUid UTF8String]) order:FileRecordOrder_TIME_DESC count:count success:^(NSArray<WFCCFileRecord *> *files) {
        if(successCB) successCB([ws convertModelListToString:files], NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void getMyFiles(String beforeUid, int count, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().getMyFileRecords(Long.parseLong(beforeUid), count, new JSGetFileRecordCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(getMyFiles:count:success:error:))
- (void)getMyFiles:(NSString *)beforeUid count:(int)count success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    __weak typeof(self)ws = self;
    [[WFCCIMService sharedWFCIMService] getMyFiles:atoll([beforeUid UTF8String]) order:FileRecordOrder_TIME_DESC count:count success:^(NSArray<WFCCFileRecord *> *files) {
        if(successCB) successCB([ws convertModelListToString:files], NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void deleteFileRecord(String messageUid, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().deleteFileRecord(Long.parseLong(messageUid), new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(deleteFileRecord:success:error:))
- (void)deleteFileRecord:(NSString *)messageUid success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] deleteFileRecord:atoll([messageUid UTF8String]) success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public void clearMessages(String strConv) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().clearMessages(conversation);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(clearMessages:))
- (void)clearMessages:(NSString *)strConv {
    [[WFCCIMService sharedWFCIMService] clearMessages:[self conversationFromJsonString:strConv]];
}
//@UniJSMethod(uiThread = true)
//public void clearRemoteConversationMessages(String strConv, JSCallback successCB, JSCallback failCB) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().clearRemoteConversationMessage(conversation, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(clearRemoteConversationMessages:success:error:))
- (void)clearRemoteConversationMessages:(NSString *)strConv success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] clearRemoteConversationMessage:[self conversationFromJsonString:strConv] success:^{
        if(successCB) successCB(nil, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = false)
//public void clearMessagesByTime(String strConv, long before) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().clearMessages(conversation, before);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(clearMessagesByTime:before:))
- (void)clearMessagesByTime:(NSString *)strConv before:(int)before {
    [[WFCCIMService sharedWFCIMService] clearMessages:[self conversationFromJsonString:strConv] before:before];
}


//@UniJSMethod(uiThread = false)
//public void insertMessage(String strConv, String sender, String strMessagePayload, int status, boolean notify, long serverTime) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    MessageContent messageContent = messagePayloadStringToMessageContent(strMessagePayload);
//    MessageStatus messageStatus = MessageStatus.status(status);
//    ChatManager.Instance().insertMessage(conversation, sender, messageContent, messageStatus, notify, serverTime);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(insertMessage:sender:messagePayload:status:notify:toUsers:serverTime:))
- (void)insertMessage:(NSString *)strConv sender:(NSString *)sender messagePayload:(NSString *)strPayload status:(int)status notify:(BOOL)notify toUsers:(NSArray<NSString *> *)users serverTime:(int64_t)serverTime {
    [[WFCCIMService sharedWFCIMService] insert:[self conversationFromJsonString:strConv] sender:sender content:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strPayload]] status:status notify:notify toUsers:users serverTime:serverTime];
}
//@UniJSMethod(uiThread = false)
//public void updateMessage(long messageId, String messagePayload) {
//    MessageContent messageContent = messagePayloadStringToMessageContent(messagePayload);
//    ChatManager.Instance().updateMessage(messageId, messageContent);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(updateMessage:messagePayload:))
- (void)updateMessage:(long)messageId messagePayload:(NSString *)strMessagePayload {
    [[WFCCIMService sharedWFCIMService] updateMessage:messageId content:[WFCCRawMessageContent contentOfPayload:[self messagePayloadFromJsonString:strMessagePayload]]];
}
//@UniJSMethod(uiThread = false)
//public void updateMessageStatus(long messageId, int status) {
//    MessageStatus messageStatus = MessageStatus.status(status);
//    ChatManager.Instance().updateMessage(messageId, messageStatus);
//}
//
UNI_EXPORT_METHOD_SYNC(@selector(updateMessageStatus:status:))
- (void)updateMessageStatus:(long)messageId status:(int)status {
    [[WFCCIMService sharedWFCIMService] updateMessage:messageId status:status];
}
//@UniJSMethod(uiThread = true)
//public void uploadMedia(String fileName, String data, int mediaType, JSCallback successCB, JSCallback failCB, JSCallback progressCB) {
//    byte[] mediaData = Base64.decode(data, Base64.DEFAULT);
//    ChatManager.Instance().uploadMedia(fileName, mediaData, mediaType, new JSGeneralCallback2(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(uploadMedia:data:mediaType:success:error:progress:))
- (void)uploadMedia:(NSString *)fileName data:(NSString *)data mediaType:(int)mediaType success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB progress:(UniModuleKeepAliveCallback)progressCB {
    [[WFCCIMService sharedWFCIMService] uploadMedia:fileName mediaData:[[NSData alloc] initWithBase64EncodedString:data options:NSDataBase64DecodingIgnoreUnknownCharacters] mediaType:mediaType success:^(NSString *remoteUrl) {
        if(successCB) successCB(remoteUrl, NO);
    } progress:^(long uploaded, long total) {
        if(progressCB) progressCB(@[@(uploaded), @(total)], YES);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}

//@UniJSMethod(uiThread = true)
//public void uploadMediaFile(String mediaFilePath, int mediaType, JSCallback successCB, JSCallback failCB, JSCallback progressCB) {
//    ChatManager.Instance().uploadMediaFile(mediaFilePath, mediaType, new UploadMediaCallback() {
//        @Override
//        public void onSuccess(String result) {
//            if (successCB != null) {
//                successCB.invoke(result);
//            }
//        }
//
//        @Override
//        public void onProgress(long uploaded, long total) {
//            if (progressCB != null) {
//                JSONArray array = new JSONArray();
//                array.add(uploaded);
//                array.add(total);
//                progressCB.invokeAndKeepAlive(array);
//            }
//        }
//
//        @Override
//        public void onFail(int errorCode) {
//            if (failCB != null) {
//                failCB.invoke(errorCode);
//            }
//        }
//    });
//}
UNI_EXPORT_METHOD(@selector(uploadMediaFile:mediaType:success:error:progress:))
- (void)uploadMediaFile:(NSString *)mediaFilePath mediaType:(int)mediaType success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB progress:(UniModuleKeepAliveCallback)progressCB {
    NSString *fileName = [mediaFilePath lastPathComponent];
    NSData *data = [NSData dataWithContentsOfFile:mediaFilePath];
    [[WFCCIMService sharedWFCIMService] uploadMedia:fileName mediaData:data mediaType:mediaType success:^(NSString *remoteUrl) {
        if(successCB) successCB(remoteUrl, NO);
    } progress:^(long uploaded, long total) {
        if(progressCB) progressCB(@[@(uploaded), @(total)], YES);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}

//@UniJSMethod(uiThread = true)
//public void sendConferenceRequest(long sessionId, String roomId, String request, String data, boolean advance, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().sendConferenceRequest(sessionId, roomId, request, data, new JSGeneralCallback2(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(sendConferenceRequest:roomId:request:data:advance:success:error:))
- (void)sendConferenceRequest:(long long)sessionId roomId:(NSString *)roomId request:(NSString *)request data:(NSString *)data advance:(BOOL)advance success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    [[WFCCIMService sharedWFCIMService] sendConferenceRequest:sessionId room:roomId request:request advanced:advance data:data success:^(NSString *authorizedUrl) {
        if(successCB) successCB(authorizedUrl, NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void searchFiles(String keyword, String strConv, String fromUser, String beforeUid, int count, JSCallback successCB, JSCallback failCB) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    ChatManager.Instance().searchFileRecords(keyword, conversation, fromUser, Long.parseLong(beforeUid), count, new JSGetFileRecordCallback(successCB, failCB));
//
//}
UNI_EXPORT_METHOD(@selector(searchFiles:conversation:fromUser:beforeUid:count:success:error:))
- (void)searchFiles:(NSString *)keyword conversation:(NSString *)strConv fromUser:(NSString *)fromUser beforeUid:(NSString *)beforeUid count:(int)count success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    __weak typeof(self)ws = self;
    [[WFCCIMService sharedWFCIMService] searchFiles:keyword conversation:[self conversationFromJsonString:strConv] fromUser:fromUser beforeMessageUid:atoll([beforeUid UTF8String]) order:0 count:count success:^(NSArray<WFCCFileRecord *> *files) {
        if(successCB) successCB([ws convertModelListToString:files], NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//@UniJSMethod(uiThread = true)
//public void searchMyFiles(String keyword, String beforeMessageUid, int count, JSCallback successCB, JSCallback failCB) {
//    ChatManager.Instance().searchMyFileRecords(keyword, Long.parseLong(beforeMessageUid), count, new JSGetFileRecordCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD(@selector(createChannel:portrait:desc:extra:success:error:))
- (void)searchMyFiles:(NSString *)keyword beforeUid:(NSString *)beforeUid count:(int)count success:(UniModuleKeepAliveCallback)successCB error:(UniModuleKeepAliveCallback)errorCB {
    __weak typeof(self)ws = self;
    [[WFCCIMService sharedWFCIMService] searchMyFiles:keyword beforeMessageUid:atoll([beforeUid UTF8String]) order:0 count:count success:^(NSArray<WFCCFileRecord *> *files) {
        if(successCB) successCB([ws convertModelListToString:files], NO);
    } error:^(int error_code) {
        if(errorCB) errorCB(@(error_code), NO);
    }];
}
//private static class JSGeneralCallback implements GeneralCallback {
//
//    private JSCallback successCB = null;
//    private JSCallback failCB = null;
//
//    public JSGeneralCallback(JSCallback successCB, JSCallback failCB) {
//        this.successCB = successCB;
//        this.failCB = failCB;
//    }
//
//    @Override
//    public void onSuccess() {
//        if (successCB != null) {
//            successCB.invoke(null);
//        }
//    }
//
//    @Override
//    public void onFail(int errorCode) {
//        if (failCB != null) {
//            failCB.invoke(errorCode);
//        }
//    }
//}
//
//private static class JSGeneralCallback2 implements GeneralCallback2 {
//
//    private JSCallback successCB = null;
//    private JSCallback failCB = null;
//
//    public JSGeneralCallback2(JSCallback successCB, JSCallback failCB) {
//        this.successCB = successCB;
//        this.failCB = failCB;
//    }
//
//    @Override
//    public void onSuccess(String result) {
//        if (successCB != null) {
//            successCB.invoke(result);
//        }
//    }
//
//    @Override
//    public void onFail(int errorCode) {
//        if (failCB != null) {
//            failCB.invoke(errorCode);
//        }
//    }
//}
//
//private static class JSGetFileRecordCallback implements GetFileRecordCallback {
//
//    private JSCallback successCB = null;
//    private JSCallback failCB = null;
//
//    public JSGetFileRecordCallback(JSCallback successCB, JSCallback failCB) {
//        this.successCB = successCB;
//        this.failCB = failCB;
//    }
//
//    @Override
//    public void onSuccess(List<FileRecord> records) {
//        if (successCB != null) {
//            successCB.invoke(JSONObject.toJSONString(records, ClientUniAppHookProxy.serializeConfig));
//        }
//    }
//
//    @Override
//    public void onFail(int errorCode) {
//        if (failCB != null) {
//            failCB.invoke(errorCode);
//        }
//    }
//}
//
//private MessageContent messagePayloadStringToMessageContent(String text) {
//    MessagePayload messagePayload = null;
//    try {
//        messagePayload = JSONObject.parseObject(text, MessagePayload.class);
//    } catch (Exception e) {
//        Log.e(TAG, "parseObject to MessagePayload exception " + text);
//    }
//    if (messagePayload != null) {
//        return ChatManager.Instance().messageContentFromPayload(messagePayload, this.userId);
//    }
//    return null;
//}
//
//static <T> T parseObject(String text, Class<T> clazz) {
//    try {
//        return JSONObject.parseObject(text, clazz);
//    } catch (Exception e) {
//        Log.e(TAG, "parseObject exception " + clazz.getName());
//    }
//    return null;
//}

- (void)onIndication:(NSArray *)args {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    dict[@"args"] = args;
    dict[@"timestamp"] = @([[[NSDate alloc] init] timeIntervalSince1970]);
    [self.uniInstance fireGlobalEvent:@"wfc-event" params:dict];
}

- (void)onConnectionStatusChanged:(ConnectionStatus)status {
    NSMutableArray *args = [[NSMutableArray alloc] init];
    [args addObject:@"onConnectionStatusChange"];
    [args addObject:@(status)];
    [self onIndication:args];
}

- (void)onConferenceEvent:(NSString *)event {
    NSMutableArray *args = [[NSMutableArray alloc] init];
    [args addObject:@"onConferenceEvent"];
    [args addObject:event];
    [self onIndication:args];
}

- (void)onConnectToServer:(NSString *)host ip:(NSString *)ip port:(int)port {
    NSMutableArray *args = [[NSMutableArray alloc] init];
    [args addObject:@"onConnectToServer"];
    [args addObject:host];
    [args addObject:ip];
    [args addObject:@(port)];
    [self onIndication:args];
}

- (void)onReceiveMessage:(NSArray<WFCCMessage *> *)messages hasMore:(BOOL)hasMore {
    [self onIndication:@[@"onReceiveMessage", [self convertModelListToString:messages], @(hasMore)]];
}

-(void)onMessageReaded:(NSArray<WFCCReadReport *> *)readeds {
    [self onIndication:@[@"onMessageRead", [self convertModelListToString:readeds]]];

}

-(void)onMessageDelivered:(NSArray<WFCCDeliveryReport *> *)delivereds {
    [self onIndication:@[@"onMessageDelivered", [self convertModelListToString:delivereds]]];
}

-(void)onRecallMessage:(long long)messageUid {
    WFCCMessage *msg = [[WFCCIMService sharedWFCIMService] getMessageByUid:messageUid];
    if([msg.content isKindOfClass:[WFCCRecallMessageContent class]]) {
        WFCCRecallMessageContent *recall = (WFCCRecallMessageContent *)msg.content;
        [self onIndication:@[@"onRecallMessage", recall.operatorId, [NSString stringWithFormat:@"%lld", messageUid]]];
    }
}

-(void)onDeleteMessage:(long long)messageUid {
    [self onIndication:@[@"onDeleteMessage", [NSString stringWithFormat:@"%lld", messageUid]]];
}

- (void)onOnlineEvent:(NSArray<WFCCUserOnlineState *> *)events {
    [self onIndication:@[@"onUserOnlineEvent", [self convertModelListToString:events]]];
}

- (void)onTrafficData:(int64_t)send recv:(int64_t)recv {
    [self onIndication:@[@"onTrafficData", @(send), @(recv)]];
}

- (void)onGroupInfoUpdated:(NSNotification *)notification {
    NSArray *groupInfos = notification.userInfo[@"groupInfoList"];
    [self onIndication:@[@"onGroupInfoUpdate", [self convertModelListToString:groupInfos]]];
}

- (void)onGroupMemberUpdated:(NSNotification *)notification {
    NSString *groupId = notification.name;
    NSArray<WFCCGroupMember *> *members = notification.userInfo[@"members"];
    [self onIndication:@[@"onGroupMemberUpdated", groupId, [self convertModelListToString:members]]];
}

- (void)onUserInfoUpdated:(NSNotification *)notification {
    NSArray *userInfos = notification.userInfo[@"userInfoList"];
    [self onIndication:@[@"onUserInfoUpdated", [self convertModelListToString:userInfos]]];
}

- (void)onFriendListUpdated:(NSNotification *)notification {
    NSArray<NSString *> *friendIds = notification.object;
    [self onIndication:@[@"onFriendListUpdate", [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:friendIds options:kNilOptions error:nil] encoding:NSUTF8StringEncoding]]];
}
- (void)onFriendRequestUpdated:(NSNotification *)notification {
    NSArray<NSString *> *newFriendRequests = notification.object;
    [self onIndication:@[@"onFriendRequestUpdate", [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:newFriendRequests options:kNilOptions error:nil] encoding:NSUTF8StringEncoding]]];
}
- (void)onSettingUpdated:(NSNotification *)notification {
    [self onIndication:@[@"onSettingUpdate"]];
}
- (void)onChannelInfoUpdated:(NSNotification *)notification {
    NSArray *channelInfos = notification.userInfo[@"channelInfoList"];
    [self onIndication:@[@"onChannelInfoUpdated", [self convertModelListToString:channelInfos]]];
}
- (void)onUserOnlineStateUpdated:(NSNotification *)notification {
    NSArray<WFCCUserOnlineState *> *events = notification.userInfo[@"states"];
    [self onIndication:@[@"onUserOnlineStateUpdated", [self convertModelListToString:events]]];
}

- (void)onMediaUploadProgress:(NSNotification *)notification {
    WFCCMessage *msg = notification.userInfo[@"message"];
    BOOL finish = [notification.userInfo[@"finish"] boolValue];
    if(finish) {
        NSString *remoteUrl = notification.userInfo[@"remoteUrl"];
        [self onIndication:@[@"onMediaUpload", msg.toJsonStr, remoteUrl]];
    } else {
        int uploaded = [notification.userInfo[@"uploaded"] intValue];
        int total = [notification.userInfo[@"total"] intValue];
        [self onIndication:@[@"onProgress", msg.toJsonStr, @(uploaded), @(total)]];
    }
}

- (void)onMessageStatusUpdated:(NSNotification *)notification {
    int status = [notification.userInfo[@"status"] intValue];
    WFCCMessage *msg = notification.userInfo[@"message"];
    
    if(status == Message_Status_Sent) {
        [self onIndication:@[@"onSendSuccess", msg.toJsonStr]];
    } else if(status == Message_Status_Send_Failure) {
        int errorCode = [notification.userInfo[@"errorCode"] intValue];
        [self onIndication:@[@"onSendFail", msg.toJsonStr, @(errorCode)]];
    } else if(status == Message_Status_Sending) {
        int64_t saveTime = [notification.userInfo[@"saveTime"] longLongValue];
        [self onIndication:@[@"onSendPrepare", msg.toJsonStr, @(saveTime)]];
    }
}

- (void)onMessageUpdated:(NSNotification *)notification {
    long messageId = [notification.object longValue];
    WFCCMessage *msg = [[WFCCIMService sharedWFCIMService] getMessage:messageId];
    [self onIndication:@[@"onMessageUpdate", msg.toJsonStr]];
}

- (WFCCConversation *)conversationFromJsonString:(NSString *)strConv {
    NSError *__error = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[strConv dataUsingEncoding:NSUTF8StringEncoding]
                                                               options:kNilOptions
                                                                 error:&__error];
    WFCCConversation *conversation = [[WFCCConversation alloc] init];
    if (!__error) {
        conversation.type = [dictionary[@"type"] intValue];
        conversation.target = dictionary[@"target"];
        conversation.line = [dictionary[@"line"] intValue];
    }
    return conversation;
}

- (WFCCMessagePayload *)messagePayloadFromJsonString:(NSString *)strMessagePayload {
    if(!strMessagePayload.length)
        return nil;
    NSError *__error = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[strMessagePayload dataUsingEncoding:NSUTF8StringEncoding]
                                                               options:kNilOptions
                                                                 error:&__error];
    WFCCMediaMessagePayload *messagePayload = [[WFCCMediaMessagePayload alloc] init];
    if (!__error) {
        messagePayload.contentType = [dictionary[@"type"] intValue];
        messagePayload.searchableContent = dictionary[@"searchableContent"];
        messagePayload.pushContent = dictionary[@"pushContent"];
        messagePayload.pushData = dictionary[@"pushData"];
        messagePayload.content = dictionary[@"content"];
        messagePayload.localContent = dictionary[@"localContent"];
        messagePayload.mentionedType = [dictionary[@"mentionedType"] intValue];
        messagePayload.mentionedTargets = dictionary[@"mentionedTargets"];
        messagePayload.extra = dictionary[@"extra"];

        messagePayload.mediaType = [dictionary[@"mediaType"] intValue];
        messagePayload.remoteMediaUrl = dictionary[@"remoteMediaUrl"];
        if([messagePayload.remoteMediaUrl isKindOfClass:[NSNull class]]) {
            messagePayload.remoteMediaUrl = nil;
        }
        messagePayload.localMediaPath = dictionary[@"localMediaPath"];
        NSString *binContStr = dictionary[@"binaryContent"];
        if([binContStr isKindOfClass:NSString.class] && binContStr.length) {
            messagePayload.binaryContent = [[NSData alloc] initWithBase64EncodedString:dictionary[@"binaryContent"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
        } else {
            if(messagePayload.contentType == 3) {
                UIImage *thumbnail = [WFCCUtilities generateThumbnail:[UIImage imageWithContentsOfFile:messagePayload.localMediaPath] withWidth:120 withHeight:120];
                messagePayload.binaryContent = UIImageJPEGRepresentation(thumbnail, 0.45);
            } else if(messagePayload.contentType == 6) {
                NSURL *videoURL = [NSURL fileURLWithPath:messagePayload.localMediaPath];
                AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
                AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
                generate1.appliesPreferredTrackTransform = YES;
                NSError *err = NULL;
                CMTime time = CMTimeMake(1, 2);
                CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
                UIImage *thumbnail = [[UIImage alloc] initWithCGImage:oneRef];
                CMTime time2 = [asset1 duration];
                thumbnail = [WFCCUtilities generateThumbnail:thumbnail withWidth:120 withHeight:120];
                messagePayload.binaryContent = UIImageJPEGRepresentation(thumbnail, 0.45);
                int seconds = ceil(time2.value/time2.timescale);
                if(!messagePayload.content.length) {
                    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                    [dict setObject:@(seconds) forKey:@"duration"];
                    [dict setObject:@(seconds) forKey:@"d"];
                    messagePayload.content = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
                }
            }
        }
    }
    return messagePayload;
}

- (NSString *)convertModelListToString:(NSArray<WFCCJsonSerializer *> *)models {
    __block NSMutableArray *arr = [[NSMutableArray alloc] init];
    [models enumerateObjectsUsingBlock:^(WFCCJsonSerializer *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [arr addObject:[obj toJsonObj]];
    }];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:arr options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}

- (NSArray *)map2Array:(NSDictionary *)dict {
    __block NSMutableArray *output = [[NSMutableArray alloc] init];
    [dict enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        [output addObject:@{@"key":key, @"value":obj}];
    }];
    return output;
}

#pragma mark - UIDocumentPickerDelegate
//file path document/conversationresource/conv_line/conv_type/conv_target/mediatype/
- (NSString *)getCachePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *path = [documentDirectory stringByAppendingPathComponent:@"selected_files"];
        
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return path;
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentsAtURLs:(NSArray <NSURL *>*)urls {
    [controller dismissViewControllerAnimated:NO completion:nil];

    __block NSDictionary *dict = nil;
    for (NSURL *url in urls) {
       //获取授权
       BOOL fileUrlAuthozied = [url startAccessingSecurityScopedResource];
       if(fileUrlAuthozied){
           //通过文件协调工具来得到新的文件地址，以此得到文件保护功能
           NSFileCoordinator *fileCoordinator = [[NSFileCoordinator alloc] init];
           NSError *error;
           
           [fileCoordinator coordinateReadingItemAtURL:url options:0 error:&error byAccessor:^(NSURL *newURL) {
               if (!error) {
                   NSData *fileData = [NSData dataWithContentsOfURL:newURL];
                   NSString *cacheDir = [self getCachePath];
                   NSString *desFileName = [cacheDir stringByAppendingPathComponent:[newURL lastPathComponent]];
                   [fileData writeToFile:desFileName atomically:YES];
                   dict = @{@"path":[@"file://" stringByAppendingString:desFileName], @"name":[newURL lastPathComponent], @"size":@(fileData.length)};
               }
           }];
           
           [url stopAccessingSecurityScopedResource];
           break;
       }else{
           NSLog(@"授权失败");
       }
    }
    
    if (dict) {
        self.selectFileSuccessCB(dict, NO);
    } else {
        self.selectFileFailureCB(@(-1), NO);
    }
    
    self.selectFileSuccessCB = nil;
    self.selectFileFailureCB = nil;
}

- (void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller {
    self.selectFileFailureCB(@(-1), NO);
    self.selectFileSuccessCB = nil;
    self.selectFileFailureCB = nil;
}


@end
