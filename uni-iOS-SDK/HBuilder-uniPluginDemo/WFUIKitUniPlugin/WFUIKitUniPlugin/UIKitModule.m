//
//  UIKitModule.m
//  WFUIKitUniPlugin
//
//  Created by Rain on 2022/7/16.
//

#import "UIKitModule.h"
#import <WFAVEngineKit/WFAVEngineKit.h>
#import <WebRTC/WebRTC.h>
#import <WFChatClient/WFCChatClient.h>
#import <WFChatUIKit/WFChatUIKit.h>
#import "AppService.h"

@interface WFUIKitModule () <WFAVEngineDelegate>
@property(nonatomic, strong) AVAudioPlayer *audioPlayer;
@property(nonatomic, strong) UILocalNotification *localCallNotification;
@end

@implementation WFUIKitModule
//
//@UniJSMethod(uiThread = false)
//public void enableNativeNotification(boolean enable) {
//    WfcUIKit.getWfcUIKit().setEnableNativeNotification(enable);
//}
- (instancetype)init {
    self = [super init];
    if(self) {
        [WFAVEngineKit notRegisterVoipPushService];
        [WFAVEngineKit sharedEngineKit].delegate = self;
        [WFCUConfigManager globalManager].appServiceProvider = [AppService sharedAppService];
    }
    return self;
}
UNI_EXPORT_METHOD_SYNC(@selector(enableNativeNotification:))
- (void)enableNativeNotification:(BOOL)enable {
    
}
//@UniJSMethod(uiThread = false)
//public void setVideoProfile(int profile, boolean swapWidthHeight) {
//    AVEngineKit.Instance().setVideoProfile(profile, swapWidthHeight);
//}
UNI_EXPORT_METHOD_SYNC(@selector(setVideoProfile:swapWidthHeight:))
- (void)setVideoProfile:(int)profile swapWidthHeight:(BOOL)swapWidthHeight {
    [[WFAVEngineKit sharedEngineKit] setVideoProfile:profile swapWidthHeight:swapWidthHeight];
}

//@UniJSMethod(uiThread = false)
//public String currentCallSession() {
//    AVEngineKit.CallSession callSession = AVEngineKit.Instance().getCurrentSession();
//    if (callSession == null) {
//        return null;
//    }
//    return JSONObject.toJSONString(callSession, ClientUniAppHookProxy.serializeConfig);
//}
UNI_EXPORT_METHOD_SYNC(@selector(currentCallSession))
- (NSString *)currentCallSession {
    WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
    if(!session || session.state == kWFAVEngineStateIdle) {
        return nil;
    }
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    dict[@"callId"] = session.callId;
    if(session.initiator.length) {
        dict[@"initiator"] = session.initiator;
    }
    if(session.inviter.length) {
        dict[@"inviter"] = session.inviter;
    }
    dict[@"state"] = @(session.state);
    dict[@"startTime"] = @(session.startTime);
    dict[@"connectedTime"] = @(session.connectedTime);
    dict[@"endTime"] = @(session.endTime);
    if(session.conversation) {
        dict[@"conversation"] = [session.conversation toJsonObj];
    }
    dict[@"audioOnly"] = @(session.audioOnly);
    dict[@"endReason"] = @(session.endReason);
    dict[@"conference"] = @(session.conference);
    dict[@"audience"] = @(session.audience);
    dict[@"advanced"] = @(session.advanced);
    dict[@"multiCall"] = @(session.multiCall);
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}

//@UniJSMethod(uiThread = false)
//public void answerCall(boolean audioOnly) {
//    AVEngineKit.CallSession callSession = AVEngineKit.Instance().getCurrentSession();
//    if (callSession != null && callSession.getState() == AVEngineKit.CallState.Incoming) {
//        callSession.answerCall(audioOnly);
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(answerCall:))
- (void)answerCall:(BOOL)audioOnly {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
        if(session.state == kWFAVEngineStateIncomming) {
            [session answerCall:audioOnly callExtra:nil];
        };
    });
}

//@UniJSMethod(uiThread = false)
//public void endCall(String callId) {
//    AVEngineKit.CallSession callSession = AVEngineKit.Instance().getCurrentSession();
//    if (callSession != null && callSession.getState() != AVEngineKit.CallState.Idle && callSession.getCallId().equals(callId)) {
//        callSession.endCall();
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(endCall:))
- (void)endCall:(NSString *)callId {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFAVCallSession *session = [WFAVEngineKit sharedEngineKit].currentSession;
        if([session.callId isEqualToString:callId]) {
            [session endCall];
        }
    });
}

//@UniJSMethod(uiThread = false)
//public void addICEServer(String url, String name, String password) {
//    AVEngineKit.Instance().addIceServer(url, name, password);
//}
UNI_EXPORT_METHOD_SYNC(@selector(addICEServer:name:password:))
- (void)addICEServer:(NSString *)url name:(NSString *)name password:(NSString *)password {
    [[WFAVEngineKit sharedEngineKit] addIceServer:url userName:name password:password];
}
//@UniJSMethod(uiThread = true)
//public void startConversation(String strConv, String title) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    Context context = mUniSDKInstance.getContext();
//    Intent intent = new Intent(context, ConversationActivity.class);
//    intent.putExtra("conversation", conversation);
//    intent.putExtra("conversationTitle", title);
//    context.startActivity(intent);
//}
UNI_EXPORT_METHOD_SYNC(@selector(startConversation:title:))
- (void)startConversation:(NSString *)strConv title:(NSString *)title {
    WFCUMessageListViewController *mvc = [[WFCUMessageListViewController alloc] init];
    mvc.conversation = [self conversationFromJsonString:strConv];
    [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:mvc animated:YES completion:nil];
}
//@UniJSMethod(uiThread = true)
//public void startSingleCall(String target, boolean audioOnly) {
//    WfcUIKit.singleCall(mUniSDKInstance.getContext(), target, audioOnly);
//}
UNI_EXPORT_METHOD_SYNC(@selector(startSingleCall:audioOnly:))
- (void)startSingleCall:(NSString *)userId audioOnly:(BOOL)audioOnly {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFCUVideoViewController *videoVC = [[WFCUVideoViewController alloc] initWithTargets:@[userId] conversation:[WFCCConversation conversationWithType:Single_Type target:userId line:0] audioOnly:audioOnly];
        [[WFAVEngineKit sharedEngineKit] presentViewController:videoVC];
    });
}
//@UniJSMethod(uiThread = true)
//public void startMultiCall(String groupId, List<String> participants, boolean audioOnly) {
//    WfcUIKit.multiCall(mUniSDKInstance.getContext(), groupId, participants, audioOnly);
//}
UNI_EXPORT_METHOD_SYNC(@selector(startMultiCall:participants:audioOnly:))
- (void)startMultiCall:(NSString *)groupId participants:(NSArray *)participants audioOnly:(BOOL)audioOnly {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIViewController *videoVC = [[WFCUMultiVideoViewController alloc] initWithTargets:participants conversation:[WFCCConversation conversationWithType:Group_Type target:groupId line:0] audioOnly:audioOnly];
        [[WFAVEngineKit sharedEngineKit] presentViewController:videoVC];
    });
}

//@UniJSMethod(uiThread = true)
//public void setupAppServer(String appServerAddress, String authToken) {
//    AppService.APP_SERVER_ADDRESS = appServerAddress;
//    SharedPreferences sp = mUniSDKInstance.getContext().getSharedPreferences("WFC_OK_HTTP_COOKIES", Context.MODE_PRIVATE);
//    String host = HttpUrl.parse(appServerAddress).url().getHost();
//    sp.edit()
//        .putString("appServer", appServerAddress)
//        .putString("authToken:" + host, authToken).apply();
//}
UNI_EXPORT_METHOD_SYNC(@selector(setupAppServer:authToken:))
- (void)setupAppServer:(NSString *)appServerAddress authToken:(NSString *)authToken {
    [[AppService sharedAppService] setServerAddress:appServerAddress];
    [[AppService sharedAppService] setAppAuthToken:authToken];
}

//@UniJSMethod(uiThread = true)
//public void showConferenceInfo(String conferenceId, String password) {
//    Context context = mUniSDKInstance.getContext();
//    Intent intent = new Intent(context, ConferenceInfoActivity.class);
//    intent.putExtra("conferenceId", conferenceId);
//    intent.putExtra("password", password);
//    context.startActivity(intent);
//}
UNI_EXPORT_METHOD_SYNC(@selector(showConferenceInfo:password:))
- (void)showConferenceInfo:(NSString *)conferenceId password:(NSString *)password {
    WFZConferenceInfoViewController *vc = [[WFZConferenceInfoViewController alloc] init];
    vc.conferenceId = conferenceId;
    vc.password = password;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[WFAVEngineKit sharedEngineKit] presentViewController:vc];
    });
}

//@UniJSMethod(uiThread = true)
//public void showConferencePortal() {
//    Context context = mUniSDKInstance.getContext();
//    Intent intent = new Intent(context, ConferencePortalActivity.class);
//    context.startActivity(intent);
//}
UNI_EXPORT_METHOD_SYNC(@selector(showConferencePortal))
- (void)showConferencePortal {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFZHomeViewController *vc = [[WFZHomeViewController alloc] init];
        vc.isPresent = YES;
        UINavigationController *nv = [[UINavigationController alloc] initWithRootViewController:vc];
        nv.modalPresentationStyle = UIModalPresentationFullScreen;
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nv animated:YES completion:nil];
    });
}

//@UniJSMethod(uiThread = false)
//public boolean isSupportMultiCall() {
//    return AVEngineKit.isSupportMultiCall();
//}
UNI_EXPORT_METHOD_SYNC(@selector(isSupportMultiCall))
- (BOOL)isSupportMultiCall {
    return YES;
}
//@UniJSMethod(uiThread = false)
//public boolean isSupportConference() {
//    return AVEngineKit.isSupportConference();
//}
UNI_EXPORT_METHOD_SYNC(@selector(isSupportConference))
- (BOOL)isSupportConference {
    return [WFAVEngineKit sharedEngineKit].supportConference;
}

- (WFCCConversation *)conversationFromJsonString:(NSString *)strConv {
    NSError *__error = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[strConv dataUsingEncoding:NSUTF8StringEncoding]
                                                               options:kNilOptions
                                                                 error:&__error];
    WFCCConversation *conversation = [[WFCCConversation alloc] init];
    if (!__error) {
        conversation.type = [dictionary[@"type"] intValue];
        conversation.target = dictionary[@"target"];
        conversation.line = [dictionary[@"line"] intValue];
    }
    return conversation;
}

#pragma mark - WFAVEngineDelegate
- (void)didReceiveCall:(WFAVCallSession *)session {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([WFAVEngineKit sharedEngineKit].currentSession.state != kWFAVEngineStateIncomming && [WFAVEngineKit sharedEngineKit].currentSession.state != kWFAVEngineStateConnected && [WFAVEngineKit sharedEngineKit].currentSession.state != kWFAVEngineStateConnecting) {
            return;
        }
        
        UIViewController *videoVC;
        if (session.conversation.type == Group_Type && [WFAVEngineKit sharedEngineKit].supportMultiCall) {
            videoVC = [[WFCUMultiVideoViewController alloc] initWithSession:session];
        } else {
            videoVC = [[WFCUVideoViewController alloc] initWithSession:session];
        }
        
        [[WFAVEngineKit sharedEngineKit] presentViewController:videoVC];
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
            if([[WFCCIMService sharedWFCIMService] isVoipNotificationSilent]) {
                NSLog(@"用户设置禁止voip通知，忽略来电提醒");
                return;
            }
            if(self.localCallNotification) {
                [[UIApplication sharedApplication] scheduleLocalNotification:self.localCallNotification];
            }
            self.localCallNotification = [[UILocalNotification alloc] init];
            
            self.localCallNotification.alertBody = @"来电话了";
            
                WFCCUserInfo *sender = [[WFCCIMService sharedWFCIMService] getUserInfo:session.inviter refresh:NO];
                if (sender.displayName) {
                    if (@available(iOS 8.2, *)) {
                        self.localCallNotification.alertTitle = sender.displayName;
                    } else {
                        // Fallback on earlier versions
                        
                    }
                }
            
            self.localCallNotification.soundName = @"ring.caf";
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] scheduleLocalNotification:self.localCallNotification];
            });
        } else {
            self.localCallNotification = nil;
        }
    });
}

- (void)shouldStartRing:(BOOL)isIncoming {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([WFAVEngineKit sharedEngineKit].currentSession.state == kWFAVEngineStateIncomming || [WFAVEngineKit sharedEngineKit].currentSession.state == kWFAVEngineStateOutgoing) {
            if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
                if([[WFCCIMService sharedWFCIMService] isVoipNotificationSilent]) {
                    NSLog(@"用户设置禁止voip通知，忽略来电震动");
                    return;
                }
                AudioServicesAddSystemSoundCompletion(kSystemSoundID_Vibrate, NULL, NULL, systemAudioCallback, NULL);
                AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
            } else {
                AVAudioSession *audioSession = [AVAudioSession sharedInstance];
                //默认情况按静音或者锁屏键会静音
                [audioSession setCategory:AVAudioSessionCategorySoloAmbient error:nil];
                [audioSession setActive:YES error:nil];
                
                if (self.audioPlayer) {
                    [self shouldStopRing];
                }
                
                NSURL *url = [[NSBundle mainBundle] URLForResource:@"ring" withExtension:@"caf"];
                NSError *error = nil;
                self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
                if (!error) {
                    self.audioPlayer.numberOfLoops = -1;
                    self.audioPlayer.volume = 1.0;
                    [self.audioPlayer prepareToPlay];
                    [self.audioPlayer play];
                }
            }
        }
    });
}

void systemAudioCallback (SystemSoundID soundID, void* clientData) {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
            if ([WFAVEngineKit sharedEngineKit].currentSession.state == kWFAVEngineStateIncomming) {
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            }
        }
    });
}

- (void)shouldStopRing {
    if (self.audioPlayer) {
        [self.audioPlayer stop];
        self.audioPlayer = nil;
    }
}

- (void)didCallEnded:(WFAVCallEndReason) reason duration:(int)callDuration {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
            if(self.localCallNotification) {
                [[UIApplication sharedApplication] cancelLocalNotification:self.localCallNotification];
                self.localCallNotification = nil;
            }
            
            if(reason == kWFAVCallEndReasonTimeout || (reason == kWFAVCallEndReasonRemoteHangup && callDuration == 0)) {
                UILocalNotification *callEndNotification = [[UILocalNotification alloc] init];
                if(reason == kWFAVCallEndReasonTimeout) {
                    callEndNotification.alertBody = @"来电未接听";
                } else {
                    callEndNotification.alertBody = @"来电已取消";
                }
                if (@available(iOS 8.2, *)) {
                    self.localCallNotification.alertTitle = @"网络通话";
                    if([WFAVEngineKit sharedEngineKit].currentSession.inviter) {
                        WFCCUserInfo *sender = [[WFCCIMService sharedWFCIMService] getUserInfo:[WFAVEngineKit sharedEngineKit].currentSession.inviter refresh:NO];
                        if (sender.displayName) {
                            self.localCallNotification.alertTitle = sender.displayName;
                        }
                    }
                }
                
                //应该播放挂断的声音
    //            self.localCallNotification.soundName = @"ring.caf";
                [[UIApplication sharedApplication] scheduleLocalNotification:callEndNotification];
            }
        }
    });
}

- (void)didReceiveIncomingPushWithPayload:(PKPushPayload * _Nonnull)payload forType:(NSString * _Nonnull)type {
    
}


@end
