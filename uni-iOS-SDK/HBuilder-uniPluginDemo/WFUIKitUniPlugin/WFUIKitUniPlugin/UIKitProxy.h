//
//  UIKitProxy.h
//  WFUIKitUniPlugin
//
//  Created by Rain on 2022/7/16.
//

#import <Foundation/Foundation.h>
#import "UniPluginProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface WFUIKitProxy : NSObject <UniPluginProtocol>

@end

NS_ASSUME_NONNULL_END
