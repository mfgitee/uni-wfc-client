//
//  WFPttClientModule.m
//  WFPttClientUniPlugin
//
//  Created by Rain on 2023/10/23.
//

#import "WFPttClientModule.h"
#import <PttClient/WFPttClient.h>
#import <WFChatClient/WFCChatClient.h>

@interface WFPttClientModule () <WFPttDelegate>

@end

@implementation WFPttClientModule

//@UniJSMethod(uiThread = false)
//public void initClient(String options) {
//    try {
//        JSONObject obj = JSONObject.parseObject(options);
//        if (obj.containsKey("enableFullDuplex")) {
//            PTTClient.ENABLE_FULL_DUPLEX = obj.getBoolean("enableFullDuplex");
//        }
//        if (obj.containsKey("enableGlobalPtt")) {
//            PTTClient.ENABLE_GLOBAL_PTT = obj.getBoolean("enableGlobalPtt");
//        }
//        if (obj.containsKey("enablePriorityMode")) {
//            PTTClient.ENABLE_PRIORITY_MODE = obj.getBoolean("enablePriorityMode");
//        }
//        if (obj.containsKey("groupChatMaxSpeakTime")) {
//            PTTClient.GROUP_CHAT_MAX_SPEAK_TIME = obj.getIntValue("groupChatMaxSpeakTime") * 1000;
//        }
//        if (obj.containsKey("groupChatMaxSpeakerCount")) {
//            PTTClient.GROUP_CHAT_MAX_SPEAKER_COUNT = obj.getIntValue("groupChatMaxSpeakerCount");
//        }
//        if (obj.containsKey("groupChatSaveVoiceMessage")) {
//            PTTClient.GROUP_CHAT_SAVE_VOICE_MESSAGE = obj.getBoolean("groupChatSaveVoiceMessage");
//        }
//        if (obj.containsKey("singleChatMaxSpeakTime")) {
//            PTTClient.SINGLE_CHAT_MAX_SPEAK_TIME = obj.getIntValue("singleChatMaxSpeakTime") * 1000;
//        }
//        if (obj.containsKey("singleChatMaxSpeakerCount")) {
//            PTTClient.SINGLE_CHAT_MAX_SPEAKER_COUNT = obj.getIntValue("singleChatMaxSpeakerCount");
//        }
//        if (obj.containsKey("singleChatSaveVoiceMessage")) {
//            PTTClient.SINGLE_CHAT_SAVE_VOICE_MESSAGE = obj.getBoolean("singleChatSaveVoiceMessage");
//        }
//
//        //PTTClient.getInstance().init(); 在 PttClientUniAppHookProxy 里面调用
//
//    } catch (Exception e) {
//        e.printStackTrace();
//    }
//
//    PttClientModule.uniSDKInstance = mUniSDKInstance;
//    // main thread
//    ChatManager.Instance().getMainHandler().post(() -> {
//        PTTClient.getInstance().setPttCallback(this);
//    });
//}

UNI_EXPORT_METHOD_SYNC(@selector(initClient:))
- (void)initClient:(NSDictionary *)dictionary {
    [WFPttClient sharedClient].delegate = self;
    if(dictionary.count) {
        
        //        if (obj.containsKey("enableFullDuplex")) {
        //            PTTClient.ENABLE_FULL_DUPLEX = obj.getBoolean("enableFullDuplex");
        //        }
        if([dictionary valueForKey:@"enableFullDuplex"]) {
            BOOL enableFullDuplex = [dictionary[@"enableFullDuplex"] boolValue];
        }
        //        if (obj.containsKey("enableGlobalPtt")) {
        //            PTTClient.ENABLE_GLOBAL_PTT = obj.getBoolean("enableGlobalPtt");
        //        }
        if([dictionary valueForKey:@"enableGlobalPtt"]) {
            [WFPttClient sharedClient].enablePtt = [dictionary[@"enableGlobalPtt"] boolValue];
        }
        //        if (obj.containsKey("enablePriorityMode")) {
        //            PTTClient.ENABLE_PRIORITY_MODE = obj.getBoolean("enablePriorityMode");
        //        }
        if([dictionary valueForKey:@"enablePriorityMode"]) {
            BOOL enablePriorityMode = [dictionary[@"enablePriorityMode"] boolValue];
        }
        //        if (obj.containsKey("groupChatMaxSpeakTime")) {
        //            PTTClient.GROUP_CHAT_MAX_SPEAK_TIME = obj.getIntValue("groupChatMaxSpeakTime") * 1000;
        //        }
        if([dictionary valueForKey:@"groupChatMaxSpeakTime"]) {
            int groupChatMaxSpeakTime = [dictionary[@"groupChatMaxSpeakTime"] intValue] * 1000;
        }
        //        if (obj.containsKey("groupChatMaxSpeakerCount")) {
        //            PTTClient.GROUP_CHAT_MAX_SPEAKER_COUNT = obj.getIntValue("groupChatMaxSpeakerCount");
        //        }
        if([dictionary valueForKey:@"groupChatMaxSpeakerCount"]) {
            int groupChatMaxSpeakerCount = [dictionary[@"groupChatMaxSpeakerCount"] intValue] ;
        }
        //        if (obj.containsKey("groupChatSaveVoiceMessage")) {
        //            PTTClient.GROUP_CHAT_SAVE_VOICE_MESSAGE = obj.getBoolean("groupChatSaveVoiceMessage");
        //        }
        if([dictionary valueForKey:@"groupChatSaveVoiceMessage"]) {
            BOOL groupChatSaveVoiceMessage = [dictionary[@"groupChatSaveVoiceMessage"] boolValue];
        }

        if([dictionary valueForKey:@"singleChatMaxSpeakTime"]) {
            int singleChatMaxSpeakTime = [dictionary[@"singleChatMaxSpeakTime"] intValue] * 1000;
            [WFPttClient sharedClient].singleChatMaxSpeakerTime = singleChatMaxSpeakTime;
        }

        if([dictionary valueForKey:@"singleChatMaxSpeakerCount"]) {
            int singleChatMaxSpeakerCount = [dictionary[@"singleChatMaxSpeakerCount"] intValue];
            [WFPttClient sharedClient].singleChatMaxSpeakerNumber = singleChatMaxSpeakerCount;
        }

        if([dictionary valueForKey:@"singleChatSaveVoiceMessage"]) {
            BOOL singleChatSaveVoiceMessage = [dictionary[@"singleChatSaveVoiceMessage"] boolValue];
            [WFPttClient sharedClient].singleChatSaveVoiceMessage = singleChatSaveVoiceMessage;
        }
        
    }
}

//
//@UniJSMethod(uiThread = false)
//public int getMaxSpeakCount(String strConv) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    if (conversation == null) {
//        return 0;
//    }
//    return PTTClient.getInstance().getMaxSpeakCount(conversation);
//}
UNI_EXPORT_METHOD_SYNC(@selector(getMaxSpeakCount:))
- (int)getMaxSpeakCount:(NSString *)strConv {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    
    return [[WFPttClient sharedClient] getMaxSpeakerNumber:conversation];
}
//@UniJSMethod(uiThread = false)
//public int getMaxSpeakTime(String strConv) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    if (conversation == null) {
//        return 0;
//    }
//    return PTTClient.getInstance().getMaxSpeakTime(conversation);
//}
UNI_EXPORT_METHOD_SYNC(@selector(getMaxSpeakTime:))
- (int)getMaxSpeakTime:(NSString *)strConv {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    
    return [[WFPttClient sharedClient] getMaxSpeakerTime:conversation];
}
//@UniJSMethod(uiThread = false)
//public int getTalkingMemberCount(String strConv) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    if (conversation == null) {
//        return 0;
//    }
//    return PTTClient.getInstance().getTalkingMemberCount(conversation);
//}
UNI_EXPORT_METHOD_SYNC(@selector(getTalkingMemberCount:))
- (int)getTalkingMemberCount:(NSString *)strConv {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    
    return (int)[[WFPttClient sharedClient] getTalkingMember:conversation].count;
}
//@UniJSMethod(uiThread = false)
//public String getTalkingMembers(String strConv) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    List<String> list = new ArrayList<>();
//    if (conversation != null) {
//        list = PTTClient.getInstance().getTalkingMembers(conversation);
//    }
//    return JSONObject.toJSONString(list, ClientUniAppHookProxy.serializeConfig);
//}
UNI_EXPORT_METHOD_SYNC(@selector(getTalkingMembers:))
- (NSString *)getTalkingMembers:(NSString *)strConv {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    
    NSDictionary<NSString *, NSNumber *> *dict = [[WFPttClient sharedClient] getTalkingMember:conversation];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
}
//@UniJSMethod(uiThread = false)
//public long getTalkingStartTime() {
//    return PTTClient.getInstance().getTalkingStartTime();
//}
UNI_EXPORT_METHOD_SYNC(@selector(getTalkingStartTime))
- (int64_t)getTalkingStartTime {
    return [[WFPttClient sharedClient] getTalkingStartTime];

}
//@UniJSMethod(uiThread = false)
//public boolean isConversationPttSilent(String strConv) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    if (conversation == null) {
//        return true;
//    }
//    return PTTClient.getInstance().isConversationPttSilent(conversation);
//}
UNI_EXPORT_METHOD_SYNC(@selector(isConversationPttSilent:))
- (BOOL)isConversationPttSilent:(NSString *)strConv {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    return [[WFPttClient sharedClient] isConversationPttSilent:conversation];
}
//@UniJSMethod(uiThread = false)
//public boolean isSaveVoiceMessage(String strConv) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    if (conversation == null) {
//        return true;
//    }
//    return PTTClient.getInstance().isSaveVoiceMessage(conversation);
//}
UNI_EXPORT_METHOD_SYNC(@selector(isSaveVoiceMessage:))
- (BOOL)isSaveVoiceMessage:(NSString *)strConv {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    return [[WFPttClient sharedClient] isSaveVoiceMessage:conversation];
}
//@UniJSMethod(uiThread = false)
//public void releaseTalking(String strConv) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    if (conversation == null) {
//        return;
//    }
//    ChatManager.Instance().getMainHandler().post(() -> PTTClient.getInstance().releaseTalking(conversation));
//}
UNI_EXPORT_METHOD_SYNC(@selector(releaseTalking:))
- (void)releaseTalking:(NSString *)strConv {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    [[WFPttClient sharedClient] releaseTalking:conversation];
}
//
//@UniJSMethod(uiThread = true)
//public void requestTalk(String strConv, int talkingPriority, JSCallback onStartTalkingCB, JSCallback onTalkingEndCB, JSCallback onRequestFailCB, JSCallback onAmplitudeUpdateCB) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    if (conversation == null) {
//        if (onRequestFailCB != null) {
//            JSONArray array = new JSONArray();
//            array.add(strConv);
//            array.add(-1);
//            onRequestFailCB.invoke(array);
//        }
//    }
//    PTTClient.getInstance().requestTalk(conversation, new TalkingCallback() {
//        @Override
//        public void onStartTalking(Conversation conversation) {
//            if (onStartTalkingCB != null) {
//                JSONArray array = new JSONArray();
//                array.add(strConv);
//                onStartTalkingCB.invoke(array);
//            }
//        }
//
//        @Override
//        public void onTalkingEnd(Conversation conversation, int reason) {
//            if (onTalkingEndCB != null) {
//                JSONArray array = new JSONArray();
//                array.add(strConv);
//                onTalkingEndCB.invoke(array);
//            }
//        }
//
//        @Override
//        public void onRequestFail(Conversation conversation, int errorCode) {
//            if (onRequestFailCB != null) {
//                JSONArray array = new JSONArray();
//                array.add(strConv);
//                array.add(errorCode);
//                onRequestFailCB.invoke(array);
//            }
//        }
//
//        @Override
//        public int talkingPriority(Conversation conversation) {
//            return talkingPriority;
//        }
//
//        @Override
//        public void onAmplitudeUpdate(int averageAmplitude) {
//            if (onAmplitudeUpdateCB != null) {
//                JSONArray array = new JSONArray();
//                array.add(averageAmplitude);
//                onAmplitudeUpdateCB.invoke(array);
//            }
//        }
//    });
//}
UNI_EXPORT_METHOD_SYNC(@selector(requestTalk:success:talkingEnd:failure:amplitude:))
- (void)requestTalk:(NSString *)strConv success:(UniModuleKeepAliveCallback)successCallback talkingEnd:(UniModuleKeepAliveCallback)endCallback failure:(UniModuleKeepAliveCallback)failureCallback amplitude:(UniModuleKeepAliveCallback)amplitudeCallback {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    [[WFPttClient sharedClient] requestTalk:conversation startTalking:^{
        successCallback(strConv, false);
    } onAmplitude:^(int averageAmplitude) {
        amplitudeCallback(@(averageAmplitude), true);
    } requestFailure:^(int errorCode) {
        failureCallback(@(errorCode), false);
    } talkingEnd:^(PttEndReason reason) {
        endCallback(@(reason), false);
    }];
}
//@UniJSMethod(uiThread = true)
//public void setConversationMaxSpeakerCount(String strConv, int count, JSCallback successCB, JSCallback failCB) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    if (conversation == null) {
//        return;
//    }
//    PTTClient.getInstance().setConversationMaxSpeakerCount(conversation, count, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD_SYNC(@selector(setConversationMaxSpeakerCount:count:success:failure:))
- (void)setConversationMaxSpeakerCount:(NSString *)strConv count:(int)count success:(UniModuleKeepAliveCallback)successCallback failure:(UniModuleKeepAliveCallback)failureCallback {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    [[WFPttClient sharedClient] setConversation:conversation maxSpeakerNumber:count success:^{
        successCallback(nil, false);
    } error:^(int errorCode) {
        failureCallback(@(errorCode), false);
    }];
}
//@UniJSMethod(uiThread = true)
//public void setConversationMaxSpeakTime(String strConv, int duration, JSCallback successCB, JSCallback failCB) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    if (conversation == null) {
//        return;
//    }
//    PTTClient.getInstance().setConversationMaxSpeakerCount(conversation, duration, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD_SYNC(@selector(setConversationMaxSpeakTime:duration:success:failure:))
- (void)setConversationMaxSpeakTime:(NSString *)strConv duration:(int)duration success:(UniModuleKeepAliveCallback)successCallback failure:(UniModuleKeepAliveCallback)failureCallback {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    [[WFPttClient sharedClient] setConversation:conversation maxSpeakerTime:duration success:^{
        successCallback(nil, false);
    } error:^(int errorCode) {
        failureCallback(@(errorCode), false);
    }];
}
//@UniJSMethod(uiThread = true)
//public void setConversationPttSilent(String strConv, boolean silent, JSCallback successCB, JSCallback failCB) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    if (conversation == null) {
//        return;
//    }
//    PTTClient.getInstance().setConversationPttSilent(conversation, silent, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD_SYNC(@selector(setConversationPttSilent:silent:success:failure:))
- (void)setConversationPttSilent:(NSString *)strConv silent:(BOOL)silent success:(UniModuleKeepAliveCallback)successCallback failure:(UniModuleKeepAliveCallback)failureCallback {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    [[WFPttClient sharedClient] setConversation:conversation pttSilent:silent success:^{
        successCallback(nil, false);
    } error:^(int errorCode) {
        failureCallback(@(errorCode), false);
    }];
}

//@UniJSMethod(uiThread = false)
//public void setEnablePtt(String strConv, boolean enable) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    if (conversation == null) {
//        return;
//    }
//    PTTClient.getInstance().setEnablePtt(conversation, enable);
//}
UNI_EXPORT_METHOD_SYNC(@selector(setEnablePtt:enable:))
- (void)setEnablePtt:(NSString*)strConv enable:(BOOL)enable {
    //not implement
}

//@UniJSMethod(uiThread = true)
//public void setSaveVoiceMessage(String strConv, boolean save, JSCallback successCB, JSCallback failCB) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    if (conversation == null) {
//        return;
//    }
//    PTTClient.getInstance().setSaveVoiceMessage(conversation, save, new JSGeneralCallback(successCB, failCB));
//}
UNI_EXPORT_METHOD_SYNC(@selector(setSaveVoiceMessage:save:success:failure:))
- (void)setSaveVoiceMessage:(NSString *)strConv save:(BOOL)save success:(UniModuleKeepAliveCallback)successCallback failure:(UniModuleKeepAliveCallback)failureCallback {
    WFCCConversation *conversation = [self conversationFromJsonString:strConv];
    [[WFPttClient sharedClient] setConversation:conversation saveVoiceMessage:save success:^{
        successCallback(nil, false);
    } error:^(int errorCode) {
        failureCallback(@(errorCode), false);
    }];
}

- (void)onIndication:(NSArray *)args {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    dict[@"args"] = args;
    dict[@"timestamp"] = @([[[NSDate alloc] init] timeIntervalSince1970]);
    [self.uniInstance fireGlobalEvent:@"wfc-ptt-event" params:dict];
}

- (WFCCConversation *)conversationFromJsonString:(NSString *)strConv {
    NSError *__error = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[strConv dataUsingEncoding:NSUTF8StringEncoding]
                                                               options:kNilOptions
                                                                 error:&__error];
    WFCCConversation *conversation = [[WFCCConversation alloc] init];
    if (!__error) {
        conversation.type = [dictionary[@"type"] intValue];
        conversation.target = dictionary[@"target"];
        conversation.line = [dictionary[@"line"] intValue];
    }
    return conversation;
}

#pragma mark - WFPttDelegate
//@Override
//public void didUserStartTalking(Conversation conversation, String userId) {
//    fireToJS("userStartTalking", conversation, userId);
//}
- (void)didConversation:(WFCCConversation *)conversation startTalkingUser:(NSString *)userId {
    [self onIndication:@[@"userStartTalking", [conversation toJsonStr], userId]];
}

//@Override
//public void didUserEndTalking(Conversation conversation, String userId) {
//    fireToJS("userEndTalking", conversation, userId);
//}
- (void)didConversation:(WFCCConversation *)conversation endTalkingUser:(NSString *)userId {
    [self onIndication:@[@"userEndTalking", [conversation toJsonStr], userId]];
}

//@Override
//public void didUserAmplitudeUpdate(Conversation conversation, String userId, int averageAmplitude) {
//    fireToJS("userAmplitudeUpdate", conversation, userId, averageAmplitude);
//}
- (void)didConversation:(WFCCConversation *)conversation amplitudeUpdate:(int)amplitude ofUser:(NSString *)userId {
    [self onIndication:@[@"userAmplitudeUpdate", [conversation toJsonStr], userId, @(amplitude)]];
}
@end
