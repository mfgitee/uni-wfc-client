//
//  WFPttClientProxy.h
//  WFPttClientUniPlugin
//
//  Created by Rain on 2023/10/24.
//

#import <UIKit/UIKit.h>
#import "UniPluginProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface WFPttClientProxy : NSObject <UniPluginProtocol>

@end

NS_ASSUME_NONNULL_END
