#!/bin/sh

set -e

CLIENT_PLUGIN_NAME=WFClientUniPlugin
UIKIT_PLUGIN_NAME=WFUIKitUniPlugin
PTTCLIENT_PLUGIN_NAME=WFPttClientUniPlugin
AV_PLUGIN_NAME=WFAVUniPlugin
BUILD_DIR=build_tmp_path

#编译client plugin
cd HBuilder-uniPluginDemo/$CLIENT_PLUGIN_NAME

rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR

OUTPUT_FOLDER=../../../nativeplugins/wf-uni-wfc-client/ios
rm -rf $OUTPUT_FOLDER
mkdir -p $OUTPUT_FOLDER

#分别编译模拟器和真机的Framework
xcodebuild -target ${CLIENT_PLUGIN_NAME} ONLY_ACTIVE_ARCH=YES -arch arm64 -configuration Release -sdk iphoneos BUILD_DIR="${BUILD_DIR}" clean build
xcodebuild -create-xcframework -framework "${BUILD_DIR}"/Release-iphoneos/"${CLIENT_PLUGIN_NAME}".framework -output "${OUTPUT_FOLDER}"/${CLIENT_PLUGIN_NAME}.xcframework

cp -af ../WF_SDK/Bugly.framework "${OUTPUT_FOLDER}"/
cp -af ../WF_SDK/WFChatClient.xcframework "${OUTPUT_FOLDER}"/
rm -rf $BUILD_DIR

cd ../../


#编译pttclient plugin
cd HBuilder-uniPluginDemo/$PTTCLIENT_PLUGIN_NAME

rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR

OUTPUT_FOLDER=../../../nativeplugins/wf-uni-wfc-pttclient/ios
rm -rf $OUTPUT_FOLDER
mkdir -p $OUTPUT_FOLDER

#分别编译模拟器和真机的Framework
xcodebuild -target ${PTTCLIENT_PLUGIN_NAME} ONLY_ACTIVE_ARCH=YES -arch arm64 -configuration Release -sdk iphoneos BUILD_DIR="${BUILD_DIR}" clean build
xcodebuild -create-xcframework -framework "${BUILD_DIR}"/Release-iphoneos/"${PTTCLIENT_PLUGIN_NAME}".framework -output "${OUTPUT_FOLDER}"/${PTTCLIENT_PLUGIN_NAME}.xcframework
rm -rf $BUILD_DIR
cp -af ../WF_SDK/PttClient.xcframework "${OUTPUT_FOLDER}"/
cd ../../


#编译音视频 plugin
cd HBuilder-uniPluginDemo/$AV_PLUGIN_NAME

rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR

OUTPUT_FOLDER=../../../nativeplugins/wf-uni-wfc-avclient/ios
rm -rf $OUTPUT_FOLDER
mkdir -p $OUTPUT_FOLDER

#分别编译模拟器和真机的Framework
xcodebuild -target ${AV_PLUGIN_NAME} ONLY_ACTIVE_ARCH=YES -arch arm64 -configuration Release -sdk iphoneos BUILD_DIR="${BUILD_DIR}" clean build
xcodebuild -create-xcframework -framework "${BUILD_DIR}"/Release-iphoneos/"${AV_PLUGIN_NAME}".framework -output "${OUTPUT_FOLDER}"/${AV_PLUGIN_NAME}.xcframework
rm -rf $BUILD_DIR
cp -af ../WF_SDK/WebRTC.xcframework "${OUTPUT_FOLDER}"/
cp -af ../WF_SDK/WFAVEngineKit.xcframework "${OUTPUT_FOLDER}"/
cd ../../

open ../nativeplugins
