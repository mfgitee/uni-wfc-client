//
//  WindRewardInfo.h
//  WindSDK
//
//  Created by Codi on 2021/4/7.
//  Copyright © 2021 Codi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WindRewardInfo : NSObject


/**
 The isCompeltedView is Tell you if you've finished watching video.
 */
@property (nonatomic,assign) BOOL isCompeltedView;
@end
